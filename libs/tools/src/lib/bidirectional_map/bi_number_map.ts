import BidirectionalMap from './bidirectional_map';

/**
 * extension of BidirectionalMap with numeric keys with some additional functions
 */
export default class BiNumberMap<TV> extends BidirectionalMap<number, TV> {
  /** remove an entry and insert it with a new key; returns newKey */
  move(oldKey: number, newKey: number): number {
    const val = this._map.get(oldKey);
    this._map.delete(oldKey);
    this._map.set(newKey, val);
    return newKey;
  }

  /** change all keys with a key \>= 'start' by adding 'difference'; returns the keys of the moved entries */
  moveKeys(start: number, difference: number): number[] {
    const keys: number[] = [];
    this._map.forEach((val, key) => {
      if (key >= start) {
        keys.push(key);
      }
    });

    return keys.map(key => this.move(key, key + difference));
  }

  /** delete the entry and move all higher entries; returns the keys of the moved entries */
  deleteMove(key: number): number[] {
    this.delete(key);
    return this.moveKeys(key, -1);
  }

  /** insert a new entry and move all keys \>= key; returns the keys of the moved entries */
  insert(key: number, value: TV): number[] {
    const keys = this.moveKeys(key, 1);
    this.set(key, value);
    return keys;
  }
}
