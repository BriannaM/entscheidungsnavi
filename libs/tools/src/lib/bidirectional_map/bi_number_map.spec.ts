import BiNumberMap from './bi_number_map';

describe('Bidirectional Map', () => {
  let map: BiNumberMap<string>;

  beforeEach(() => {
    map = new BiNumberMap();
    map.set(2, 'foo');
    map.set(7, 'bar');
    map.set(1, 'baz');
  });

  it('test move()', () => {
    map.move(1, 3);
    expect(map.size).toBe(3);
    expect(map.has(3)).toBe(true);
    expect(map.get(3)).toBe('baz');
    expect(map.has(1)).toBe(false);
  });

  it('test moveKeys()', () => {
    map.moveKeys(2, 3);

    expect(map.size).toBe(3);
    expect(map.has(5)).toBe(true);
    expect(map.get(5)).toBe('foo');
    expect(map.has(2)).toBe(false);
    expect(map.has(10)).toBe(true);
    expect(map.get(10)).toBe('bar');

    map.moveKeys(7, -2);

    expect(map.size).toBe(3);
    expect(map.has(5)).toBe(true);
    expect(map.get(5)).toBe('foo');
    expect(map.has(10)).toBe(false);
    expect(map.has(8)).toBe(true);
    expect(map.get(8)).toBe('bar');
  });

  it('test deleteMove()', () => {
    map.deleteMove(1);
    expect(map.get(1)).toBe('foo');
    expect(map.get(6)).toBe('bar');
    expect(map.size).toBe(2);
  });

  it('test insert()', () => {
    map.insert(2, 'test');
    expect(map.get(1)).toBe('baz');
    expect(map.get(2)).toBe('test');
    expect(map.get(3)).toBe('foo');
    expect(map.get(8)).toBe('bar');
    expect(map.size).toBe(4);
  });
});
