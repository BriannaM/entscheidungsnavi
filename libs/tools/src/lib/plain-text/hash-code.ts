/**
 * This is a simple, *NON-SECURE*, fast hash implementation for strings. It is equivalent to String.hashCode() in Java.
 * https://stackoverflow.com/a/8831937/13620444
 *
 * @param input - The string to create a hash from
 */
export function hashCode(input: string): number {
  let hash = 0;
  if (input.length === 0) {
    return hash;
  }
  for (let i = 0; i < input.length; i++) {
    const char = input.charCodeAt(i);
    hash = (hash << 5) - hash + char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}
