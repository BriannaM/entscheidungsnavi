import Delta from 'quill-delta';

export function emptyRichText() {
  return JSON.stringify(new Delta());
}
