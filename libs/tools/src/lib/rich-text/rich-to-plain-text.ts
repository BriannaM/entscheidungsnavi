import Delta from 'quill-delta';

/**
 * Converts the rich text output from Quill.js to plain text. All formatting is removed.
 *
 * @param richText - Input which may be a JSON object from Quill or plain text
 */
export function richToPlainText(richText: string): string {
  try {
    const delta = new Delta(JSON.parse(richText));

    const content = delta.reduce((text, op) => {
      if (!op.insert) throw new TypeError('only `insert` operations can be transformed!');
      if (typeof op.insert !== 'string') return text + ' ';
      return text + op.insert;
    }, '');

    return content;
  } catch {
    // The text is not in JSON format, so we assume it is plaintext
    return richText;
  }
}
