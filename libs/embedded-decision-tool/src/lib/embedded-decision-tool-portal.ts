import { Injectable, OnDestroy } from '@angular/core';
import { plainToInstance } from 'class-transformer';
import { BehaviorSubject, filter, Observable, pairwise, shareReplay, Subscription } from 'rxjs';
import {
  AbstractEmbedderToDecisionToolMessageBodyDto,
  DecisionToolToEmbedderMessageDto,
  EmbeddableProject,
  EmbedderToDecisionToolMessageDto,
  LoadProjectMessageBodyDto,
  ProjectDirtyMessageBodyDto,
  ProjectRequestMessageBodyDto,
  ProjectResponseMessageBodyDto,
} from './protocol.types';

const STATE = {
  NO_PROJECT_LOADED: 'open-but-no-project-loaded',
  PROJECT_LOADED: 'open-and-project-loaded',
  CLOSED: 'closed',
} as const;

type State = (typeof STATE)[keyof typeof STATE];

/**
 * This service assists `EmbeddedDecisionToolPortalOutletComponent` in
 * - _loading_ a project into DT,
 * - _fetching_ a project from DT,
 * - and _notifying_ it with project updates from DT
 */
@Injectable({
  providedIn: 'root',
})
export class EmbeddedDecisionToolPortal implements OnDestroy {
  private state$ = new BehaviorSubject<State>(STATE.CLOSED);

  private isProjectLoaded$ = this.state$.pipe(
    filter(state => state === STATE.PROJECT_LOADED),
    shareReplay(1)
  );

  private isProjectUnloaded$ = this.state$.pipe(
    pairwise(),
    filter(([previousState, _]) => previousState === STATE.PROJECT_LOADED)
  );

  private embedderOrigin: string;

  private loadProject: (project: EmbeddableProject) => void | null = null;
  private fetchProject: () => EmbeddableProject | null = null;

  private projectUpdate$: Observable<void> | null = null;
  private projectUpdatedSubscription: Subscription | null = null;

  messageListener: (message: MessageEvent<unknown>) => void;

  open(
    embedderOrigin: string,
    loadProject: (project: EmbeddableProject) => void,
    fetchProject: () => EmbeddableProject,
    projectUpdate$: Observable<void>
  ) {
    if (this.state$.value !== STATE.CLOSED) throw new Error('Portal is already open.');

    this.embedderOrigin = embedderOrigin;
    this.loadProject = loadProject;
    this.fetchProject = fetchProject;
    this.projectUpdate$ = projectUpdate$;

    this.messageListener = unsafeMessage => this.receiveUnsafeMessage(unsafeMessage);
    window.addEventListener('message', this.messageListener);

    this.state$.next(STATE.NO_PROJECT_LOADED);
  }

  markProjectAsDirty() {
    if (this.state$.value === STATE.CLOSED) return;

    this.sendMessage(
      plainToInstance(ProjectDirtyMessageBodyDto, {
        messageType: 'dt-embedded-project-dirty',
      } satisfies ProjectDirtyMessageBodyDto)
    );
  }

  isProjectLoaded() {
    return this.isProjectLoaded$;
  }

  private receiveUnsafeMessage(unsafeMessage: MessageEvent<unknown>) {
    if (unsafeMessage.origin !== this.embedderOrigin) return;
    if (unsafeMessage.source !== window.parent) return;

    const embedderToDecisionToolMessage = plainToInstance(EmbedderToDecisionToolMessageDto, unsafeMessage.data);
    this.receiveMessage(embedderToDecisionToolMessage.body);
  }

  private receiveMessage(messageBody: AbstractEmbedderToDecisionToolMessageBodyDto) {
    if (messageBody instanceof LoadProjectMessageBodyDto && this.state$.value === STATE.NO_PROJECT_LOADED) {
      this.loadProject(messageBody.project);
      this.projectUpdatedSubscription = this.projectUpdate$.subscribe(() => this.markProjectAsDirty());
      this.state$.next(STATE.PROJECT_LOADED);
    } else if (messageBody instanceof ProjectRequestMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      this.sendMessage(
        plainToInstance(ProjectResponseMessageBodyDto, {
          messageType: 'dt-embedded-project-response',
          project: this.fetchProject(),
        } satisfies ProjectResponseMessageBodyDto)
      );
    } else {
      throw new Error(`Cannot receive message of type '${messageBody.messageType}' in state '${this.state$.value}'.`);
    }
  }

  private sendMessage(message: ProjectResponseMessageBodyDto | ProjectDirtyMessageBodyDto) {
    const send = () => window.parent.postMessage({ body: message } satisfies DecisionToolToEmbedderMessageDto, this.embedderOrigin);

    if (message instanceof ProjectResponseMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      send();
    } else if (message instanceof ProjectDirtyMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      send();
    } else {
      throw new Error(`Cannot send message of type '${message.messageType}' in state '${this.state$.value}'.`);
    }
  }

  close() {
    window.removeEventListener('message', this.messageListener);
    this.loadProject = null;
    this.fetchProject = null;
    this.projectUpdatedSubscription?.unsubscribe();
    this.projectUpdate$ = null;
    this.state$.next(STATE.CLOSED);
  }

  ngOnDestroy() {
    this.close();
  }
}
