import { Exclude, Expose, Type } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsString, Matches, ValidateNested } from 'class-validator';

export interface EmbeddableProject {
  name: string;
  data: any;
}

@Exclude()
export class EmbeddableProjectDto implements EmbeddableProject {
  @Expose()
  @IsString()
  name: string;

  @Expose()
  @IsString()
  @IsNotEmpty()
  data: string;
}

@Exclude()
abstract class AbstractMessageBodyDto {
  @Expose()
  @IsString()
  @Matches(/dt-embedded-[a-zA-Z]+/, { message: "A valid message must begin with 'dt-embedded'" })
  abstract messageType: `dt-embedded-${string}`;
}

// Messages from decision-tool to embedder (e.g. cockpit)

export const DECISION_TOOL_TO_EMBEDDER_MESSAGE_TYPES = ['dt-embedded-project-response', 'dt-embedded-project-dirty'] as const;
export type DecisionToolToEmbedderMessageType = (typeof DECISION_TOOL_TO_EMBEDDER_MESSAGE_TYPES)[number];

@Exclude()
export abstract class AbstractDecisionToolToEmbedderMessageBodyDto extends AbstractMessageBodyDto {
  @Expose()
  @IsEnum(DECISION_TOOL_TO_EMBEDDER_MESSAGE_TYPES, { message: 'Unsupported message from decision-tool to embedder.' })
  abstract override messageType: DecisionToolToEmbedderMessageType;
}

@Exclude()
export class ProjectResponseMessageBodyDto extends AbstractDecisionToolToEmbedderMessageBodyDto {
  override messageType: 'dt-embedded-project-response';

  @Expose()
  project: EmbeddableProject;
}

@Exclude()
export class ProjectDirtyMessageBodyDto extends AbstractDecisionToolToEmbedderMessageBodyDto {
  override messageType: 'dt-embedded-project-dirty';
}

export class DecisionToolToEmbedderMessageDto {
  @Expose()
  @ValidateNested({ each: true })
  @Type(() => AbstractDecisionToolToEmbedderMessageBodyDto, {
    keepDiscriminatorProperty: true,
    discriminator: {
      property: 'messageType',
      subTypes: [
        { value: ProjectResponseMessageBodyDto, name: 'dt-embedded-project-response' },
        { value: ProjectDirtyMessageBodyDto, name: 'dt-embedded-project-dirty' },
      ],
    },
  })
  body: ProjectResponseMessageBodyDto | ProjectDirtyMessageBodyDto;
}

// Messages from embedder (e.g. cockpit) to decision-tool
const EMBEDDER_TO_DECISION_TOOL_MESSAGE_TYPES = ['dt-embedded-project-request', 'dt-embedded-project-load'] as const;
type EmbedderToDecisionToolMessageType = (typeof EMBEDDER_TO_DECISION_TOOL_MESSAGE_TYPES)[number];

export abstract class AbstractEmbedderToDecisionToolMessageBodyDto extends AbstractMessageBodyDto {
  @IsEnum(EMBEDDER_TO_DECISION_TOOL_MESSAGE_TYPES, { message: 'Unsupported message from embedder to decision-tool.' })
  abstract override messageType: EmbedderToDecisionToolMessageType;
}

@Exclude()
export class ProjectRequestMessageBodyDto extends AbstractEmbedderToDecisionToolMessageBodyDto {
  override messageType: 'dt-embedded-project-request';
}

@Exclude()
export class LoadProjectMessageBodyDto extends AbstractEmbedderToDecisionToolMessageBodyDto {
  override messageType: 'dt-embedded-project-load';

  @Expose()
  @ValidateNested()
  @Type(() => EmbeddableProjectDto)
  project: EmbeddableProjectDto;
}

export class EmbedderToDecisionToolMessageDto {
  @Expose()
  @ValidateNested({ each: true })
  @Type(() => AbstractEmbedderToDecisionToolMessageBodyDto, {
    keepDiscriminatorProperty: true,
    discriminator: {
      property: 'messageType',
      subTypes: [
        { value: ProjectRequestMessageBodyDto, name: 'dt-embedded-project-request' },
        { value: LoadProjectMessageBodyDto, name: 'dt-embedded-project-load' },
      ],
    },
  })
  body: ProjectRequestMessageBodyDto | LoadProjectMessageBodyDto;
}
