import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-toggle-button',
  templateUrl: './toggle-button.component.html',
})
export class ToggleButtonComponent {
  @Input() value: boolean;
  @Input() icon: string;
  @Input() tooltipTrue: string;
  @Input() tooltipFalse: string;

  @Output() valueChange = new EventEmitter<boolean>();
}
