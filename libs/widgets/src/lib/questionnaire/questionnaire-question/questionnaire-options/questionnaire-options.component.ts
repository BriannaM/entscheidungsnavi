import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OptionsQuestionEntry } from '@entscheidungsnavi/api-types';
import { FORWARD_CONTROL_CONTAINER } from '../../../form-elements';

@Component({
  selector: 'dt-questionnaire-question-options',
  templateUrl: './questionnaire-options.component.html',
  styleUrls: ['./questionnaire-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class QuestionnaireQuestionOptionsComponent {
  @Input()
  entry: OptionsQuestionEntry;

  @Input()
  formIndex: number;
}
