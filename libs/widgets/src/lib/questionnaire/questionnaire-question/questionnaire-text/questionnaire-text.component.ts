import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TextQuestionEntry } from '@entscheidungsnavi/api-types';
import { FORWARD_CONTROL_CONTAINER } from '../../../form-elements';

@Component({
  selector: 'dt-questionnaire-question-text',
  templateUrl: './questionnaire-text.component.html',
  styleUrls: ['./questionnaire-text.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class QuestionnaireQuestionTextComponent {
  @Input()
  entry: TextQuestionEntry;

  @Input()
  formIndex: number;
}
