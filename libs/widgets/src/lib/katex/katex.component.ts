import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import katex from 'katex';

@Component({
  selector: 'dt-katex',
  templateUrl: './katex.component.html',
  styleUrls: ['./katex.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KatexComponent {
  @Input()
  latex: string;

  constructor(private sanitizer: DomSanitizer) {}

  get html() {
    return this.sanitizer.bypassSecurityTrustHtml(katex.renderToString(this.latex));
  }
}
