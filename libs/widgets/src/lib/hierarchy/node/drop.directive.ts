import { Directive, ElementRef, HostListener, Renderer2, inject } from '@angular/core';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { HierarchyInterfaceDirective } from '../interface.directive';
import { HierarchyNodeStateDirective } from '../node-state.directive';
import { HierarchyNodeComponent } from './hierarchy-node.component';

/**
 * This directive handles dropping onto a hierarchy node.
 */
@Directive({ selector: '[dtHierarchyNodeDrop]', standalone: true })
export class HierarchyNodeDropDirective<T> {
  private nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);
  private node = inject<HierarchyNodeComponent<T>>(HierarchyNodeComponent);
  private elementRef = inject<ElementRef<HTMLElement>>(ElementRef);

  private renderer = inject(Renderer2);

  private dropOverlay: HTMLDivElement;

  @HostListener('dragover', ['$event'])
  dragOver(event: DragEvent) {
    event.preventDefault();

    // If we ourselves are being dragged, or we are a child of a dragged element, we cannot drop here
    for (const node of this.nodeState.focused) {
      if (node.equals(this.node.locationSequence) || this.node.locationSequence.isDescendantOf(node)) {
        event.dataTransfer.dropEffect = 'none';
        return;
      }
    }

    event.dataTransfer.dropEffect = 'move';

    if (!this.dropOverlay) {
      this.dropOverlay = this.renderer.createElement('div');
      this.renderer.setStyle(this.dropOverlay, 'position', 'absolute');
      this.renderer.setStyle(this.dropOverlay, 'left', '0');
      this.renderer.setStyle(this.dropOverlay, 'right', '0');
      this.renderer.setStyle(this.dropOverlay, 'top', '0');
      this.renderer.setStyle(this.dropOverlay, 'bottom', '0');
      this.renderer.setStyle(this.dropOverlay, 'pointerEvents', 'none');
      this.renderer.appendChild(this.elementRef.nativeElement, this.dropOverlay);
    }

    const dropDirection = this.getDropDirection(event);
    switch (dropDirection) {
      case 'below':
        this.renderer.setStyle(this.dropOverlay, 'boxShadow', 'inset 0px -5px 0px -2px orange');
        break;
      case 'above':
        this.renderer.setStyle(this.dropOverlay, 'boxShadow', 'inset 0px 5px 0px -2px orange');
        break;
      case 'right':
        this.renderer.setStyle(this.dropOverlay, 'boxShadow', 'inset -5px 0px 0px -2px orange');
        break;
      default:
        assertUnreachable(dropDirection);
    }
  }

  @HostListener('drop', ['$event'])
  drop(event: DragEvent) {
    event.preventDefault();

    const direction = this.getDropDirection(event);
    this.clearDrop();

    switch (direction) {
      case 'above':
        this.interface.move(this.nodeState.focused, this.node.locationSequence);
        break;
      case 'below':
        this.interface.move(
          this.nodeState.focused,
          new LocationSequence([...this.node.locationSequence.value.slice(0, -1), this.node.locationSequence.value.at(-1) + 1])
        );
        break;
      case 'right':
        this.interface.move(this.nodeState.focused, new LocationSequence([...this.node.locationSequence.value, 0]));
        break;
      default:
        assertUnreachable(direction);
    }
  }

  @HostListener('dragleave')
  @HostListener('dragend')
  clearDrop() {
    if (this.dropOverlay) {
      this.renderer.removeChild(this.elementRef, this.dropOverlay);
      this.dropOverlay = null;
    }
  }

  private getDropDirection(event: DragEvent): 'above' | 'below' | 'right' {
    const target = event.target as HTMLElement;

    const x = event.offsetX;
    const y = event.offsetY;
    const height = target.offsetHeight;
    const width = target.offsetWidth;

    if (x / width > 0.7) {
      return 'right';
    } else {
      return y > height / 2 ? 'below' : 'above';
    }
  }
}
