import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { ColorPickerService } from './color-picker.service';

@Directive({
  selector: '[dtColorPicker]',
})
export class ColorPickerDirective {
  pickerOpen = false;

  @Input('dtColorPicker')
  initialColor: string;

  @Input()
  dtColorPickerPreset: string[] | (() => string[]);

  @Output('dtColorPicker')
  colorChange = new EventEmitter<string>();

  @Output()
  dtColorPickerOpen = new EventEmitter();

  @Output()
  dtColorPickerClose = new EventEmitter();

  constructor(private colorPickerService: ColorPickerService, private elRef: ElementRef<HTMLElement>) {}

  @HostListener('click')
  hostClick() {
    if (!this.pickerOpen && this.elRef.nativeElement.getAttribute('disabled') === 'false') {
      this.openPicker();
    }
  }

  openPicker() {
    let finalPreset;

    if (this.dtColorPickerPreset) {
      if (Array.isArray(this.dtColorPickerPreset)) {
        finalPreset = this.dtColorPickerPreset.slice();
      } else {
        finalPreset = this.dtColorPickerPreset();
      }
    }

    this.dtColorPickerOpen.emit();

    this.colorPickerService.openColorPicker(this.elRef.nativeElement, this.initialColor, finalPreset).subscribe({
      next: (newColor: string) => {
        this.colorChange.emit(newColor);
      },
      complete: () => {
        this.pickerOpen = false;
        this.dtColorPickerClose.emit();
      },
    });

    this.pickerOpen = true;
  }
}
