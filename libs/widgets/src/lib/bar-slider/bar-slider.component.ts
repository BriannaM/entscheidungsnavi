import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-bar-slider',
  styleUrls: ['bar-slider.component.scss'],
  templateUrl: 'bar-slider.component.html',
})
export class BarSliderComponent {
  @Output() changeEvent: EventEmitter<number> = new EventEmitter();
  @Output() valueChange: EventEmitter<number> = new EventEmitter();
  @Input() value: number;
  @Input() disabled = false;
  @Input() disabledLook: boolean;
  @Input() readonly = false;
  @Input() min = 0;
  @Input() max = 100;
  @Input() precision = 0; // absolute deviation from value for the interval
  @Input() interval = false;
  @Input() vertical = true;
  @Input() length = 250; // in px
  @Input() width = 40; // in px
  @Input() fillContainer = false;
  @Input() topLabels: string[];
  @Input() showBottomLabels = false;
  @Input() step = 1;

  circleHeight = 8;

  readonly numberOfIntervals = 11;

  get intervals(): number[] {
    const intervals = [];
    for (let i = 0; i < this.numberOfIntervals; i++) {
      intervals.push((i * (this.max - this.min)) / (this.numberOfIntervals - 1));
    }
    return intervals;
  }

  // get the value restricted to [min,max]
  get sliderValue(): number {
    return Math.min(this.max, Math.max(this.min, Math.round(this.value * (1 / this.step)) / (1 / this.step)));
  }

  set sliderValue(value: number) {
    if (!this.disabled && !this.readonly && value !== this.value) {
      this.emitNewValue(+value);
    }
  }

  // get the value as percentage [0, 100]
  get valueInPercent(): number {
    return ((this.sliderValue - this.min) * 100) / (this.max - this.min);
  }

  get topPerc(): number {
    return Math.min(100 - this.valueInPercent, (this.precision * 100) / (this.max - this.min));
  }

  get botPerc(): number {
    return Math.min(this.valueInPercent, (this.precision * 100) / (this.max - this.min));
  }

  get intervalHeightInPercent(): number {
    return this.topPerc + this.botPerc;
  }

  emitChangeEvent(val: number) {
    this.changeEvent.emit(val);
  }

  emitNewValue(val: number) {
    this.valueChange.emit(val != null ? val : this.value);
  }

  hasDisabledStyle(): boolean {
    return (this.disabled && this.disabledLook !== false) || this.disabledLook;
  }
}
