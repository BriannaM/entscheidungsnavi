import {
  AfterViewInit,
  Component,
  ContentChild,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Sort, SortDirection } from '@angular/material/sort';

// eslint-disable-next-line @angular-eslint/directive-selector
@Directive({ selector: 'dt-flex-column' })
export class FlexColumnDirective {
  @Input() id!: string;
  @Input() name: string;
  @Input() flex: boolean;
  @Input() sortable = true;

  @ContentChild(TemplateRef) cellTemplate: TemplateRef<any>;
}

/**
 * Example Usage in LoadProjectComponent
 */

@Component({
  selector: 'dt-flex-table',
  templateUrl: './flex-table.component.html',
  styleUrls: ['./flex-table.component.scss'],
})
export class FlexTableComponent implements AfterViewInit {
  @Input()
  data: readonly unknown[];

  @Input()
  clickableRows = true;

  @Input()
  defaultSortDirection: SortDirection = 'asc';

  @Input()
  defaultSortColumn: string;

  @Input()
  rowHeight = 50;

  @Input()
  sortChangeable = true;

  @Output()
  clickRow: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  sort: EventEmitter<Sort> = new EventEmitter<Sort>();

  @Input()
  selectedIndex: number;

  @ContentChildren(FlexColumnDirective)
  columns: QueryList<FlexColumnDirective>;

  @ViewChild('tablecontainer', { static: true })
  tableContainer: ElementRef<HTMLDivElement>;

  hoverRow: number;

  ngAfterViewInit() {
    const tableContainer = this.tableContainer.nativeElement;

    const observer = new MutationObserver(_changes => {
      this.updateColumns();
    });
    observer.observe(tableContainer.querySelector('.scroll-container'), {
      attributes: false,
      childList: true,
      characterData: true,
      subtree: true,
    });
    this.updateColumns();

    requestAnimationFrame(() => this.updateColumns());
  }

  @HostListener('window:resize')
  updateColumns() {
    if (this.data.length === 0) {
      return;
    }

    const tableContainer = this.tableContainer.nativeElement;
    const headerContainer = tableContainer.querySelector('.header-container') as HTMLDivElement;
    const availableWidth = headerContainer.offsetWidth;

    this.columns.forEach((column, index) => {
      const columnElement = tableContainer.querySelector('.body-container .column:nth-child(' + (index + 1) + ')') as HTMLDivElement;
      const headerElement = tableContainer.querySelector('.header-container .header:nth-child(' + (index + 1) + ')') as HTMLDivElement;

      const width = columnElement.offsetWidth;

      if (width !== 0) {
        headerElement.style.width = headerElement.style.minWidth = (width / availableWidth) * 100 + '%';
      }
    });
  }

  clickEvent(event: MouseEvent, index: number) {
    if (
      this.clickableRows &&
      event.target instanceof HTMLElement &&
      (event.target.classList.contains('cell') || event.target.parentElement.classList.contains('cell'))
    ) {
      this.clickRow.emit(index);
    }
  }

  sortEvent(event: Sort) {
    this.sort.emit(event);
  }
}
