import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatLegacyCheckboxModule as MatCheckboxModule } from '@angular/material/legacy-checkbox';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MAT_ICON_DEFAULT_OPTIONS, MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RouterModule } from '@angular/router';
import { ColorSketchModule } from 'ngx-color/sketch';
import { QuillModule } from 'ngx-quill';
import { PortalModule } from '@angular/cdk/portal';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { Platform } from '@angular/cdk/platform';
import { ClipboardModule } from 'ngx-clipboard';
import { NoteBtnComponent } from './note-btn';
import { BarSliderComponent } from './bar-slider';
import { CollapsibleComponent } from './collapsible';
import { ColorPickerDirective } from './color-picker';
import { DragDropListComponent } from './drag-drop-list';
import { FlexColumnDirective, FlexTableComponent } from './flex-table';
import { InputboxComponent, InputboxGroupedComponent, InputboxSimpleComponent } from './inputbox';
import { RangeInputComponent } from './range-input';
import { TabComponent, TabsComponent } from './tabs';
import { YoutubeContainerComponent, YoutubeModalComponent } from './youtube';
import { TriangleComponent } from './triangle';
import {
  AutoFocusDirective,
  DirectEventDirective,
  DynamicChangeDirectiveModule,
  ElementRefDirective,
  InlineSVGDirective,
  OverflowDirective,
  RegexBlockDirective,
  ResizeObserverDirective,
  ScrollbarDirective,
  ScrollIntoViewDirective,
  ValidateNumberInputDirective,
  ViewIntersectionDirective,
  VisibleDirective,
  WidthTriggerDirective,
} from './directives';
import { OverlayProgressBarDirective } from './overlay-progress-bar';
import { VerticalSliderComponent } from './vertical-slider';
import { CopyWhistleDirective, PopOverComponent, WhistleComponent } from './popover';
import { NumberRoundingPipe } from './pipes/number-rounding.pipe';
import { NumberRoundingAsPower } from './pipes/number-rounding-as-power.pipe';
import { AspectBoxComponent } from './aspect-box';
import { PasswordStrengthComponent } from './password-strength/password-strength.component';
import { PasswordFormComponent } from './form-elements/password-form';
import { ModalModule } from './modal/modal.module';
import { ButtonModule } from './buttons/button.module';
import { LoadingSnackbarComponent } from './loading-snackbar';
import { SelectComponent, SelectGroupDirective, SelectLabelDirective, SelectLineComponent, SelectOptionDirective } from './select';
import {
  InfluenceFactorNamePipe,
  IsValidEmailPipe,
  LocalizedStringPipe,
  NumberCastPipe,
  SafeArrayIndexPipe,
  SafeNumberPipe,
  SafeUrlPipe,
  SortByPipe,
  StateNamePipe,
  ToFixedPipe,
} from './pipes';
import { TagInputComponent } from './tag-input/tag-input.component';
import { ObjectBoxComponent } from './object-box/object-box.component';
import { InputSpinnerComponent } from './input-spinner/input-spinner.component';
import { CollapseAllComponent } from './collapse-all/collapse-all.component';
import { EmailConfirmationModalComponent } from './email-confirmation-modal';
import { SnackbarComponent } from './snackbar';
import {
  AccountModalComponent,
  ChangeEmailComponent,
  ChangePasswordComponent,
  ChangeUsernameComponent,
  DeleteAccountModalComponent,
} from './account-modal';
import { RequestPasswordResetModalComponent } from './request-password-reset-modal';
import { RangeInputLogComponent } from './range-input-log/range-input-log.component';
import {
  EvaluationCheckboxesComponent,
  QuestionnaireComponent,
  QuestionnaireQuestionComponent,
  QuestionnaireQuestionNumberComponent,
  QuestionnaireQuestionOptionsComponent,
  QuestionnaireQuestionTableComponent,
  QuestionnaireQuestionTextBlockComponent,
  QuestionnaireQuestionTextComponent,
} from './questionnaire';
import { PaginatorIntlService } from './paginator/paginator-intl.service';
import { ConfirmPopOverComponent } from './popover/confirm/confirm-popover.component';
import { QuillConfigurationService } from './rich-text-editor';

const DECLARE_AND_EXPORT = [
  AutoFocusDirective,
  BarSliderComponent,
  CollapseAllComponent,
  CollapsibleComponent,
  VerticalSliderComponent,
  DragDropListComponent,
  InputboxComponent,
  InputboxGroupedComponent,
  InputboxSimpleComponent,
  RegexBlockDirective,
  VisibleDirective,
  DirectEventDirective,
  RangeInputComponent,
  TabComponent,
  TabsComponent,
  PopOverComponent,
  WhistleComponent,
  CopyWhistleDirective,
  TriangleComponent,
  ViewIntersectionDirective,
  ScrollbarDirective,
  YoutubeContainerComponent,
  YoutubeModalComponent,
  FlexTableComponent,
  FlexColumnDirective,
  ColorPickerDirective,
  SafeNumberPipe,
  SafeArrayIndexPipe,
  SortByPipe,
  NumberCastPipe,
  StateNamePipe,
  InfluenceFactorNamePipe,
  ValidateNumberInputDirective,
  NumberRoundingPipe,
  NumberRoundingAsPower,
  AspectBoxComponent,
  ToFixedPipe,
  PasswordStrengthComponent,
  PasswordFormComponent,
  ElementRefDirective,
  ScrollIntoViewDirective,
  EvaluationCheckboxesComponent,
  LoadingSnackbarComponent,
  InlineSVGDirective,
  LocalizedStringPipe,
  TagInputComponent,
  ObjectBoxComponent,
  SelectComponent,
  SelectLabelDirective,
  SelectOptionDirective,
  SelectGroupDirective,
  SelectLineComponent,
  InputSpinnerComponent,
  ResizeObserverDirective,
  RangeInputLogComponent,
  EmailConfirmationModalComponent,
  AccountModalComponent,
  RequestPasswordResetModalComponent,
  QuestionnaireComponent,
  QuestionnaireQuestionComponent,
  QuestionnaireQuestionTextBlockComponent,
  QuestionnaireQuestionNumberComponent,
  QuestionnaireQuestionOptionsComponent,
  QuestionnaireQuestionTableComponent,
  QuestionnaireQuestionTextComponent,
  IsValidEmailPipe,
];

const COMMON_RE_EXPORTS = [
  MatMenuModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatIconModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatButtonModule,
  MatSortModule,
  MatRadioModule,
  MatTooltipModule,
  MatDividerModule,
  MatSidenavModule,
  MatToolbarModule,
  MatSelectModule,
  MatRippleModule,
  MatListModule,
  PortalModule,
  MatCardModule,
  MatTabsModule,
  MatStepperModule,
  MatExpansionModule,
  FormsModule,
  DragDropModule,
  ReactiveFormsModule,
  MatSlideToggleModule,
  MatDialogModule,
  ModalModule,
  ButtonModule,
  MatPaginatorModule,
  MatTableModule,
  MatChipsModule,
  MatAutocompleteModule,
  DynamicChangeDirectiveModule,
  ClipboardModule,
  SafeUrlPipe,
];

@NgModule({
  imports: [
    ...COMMON_RE_EXPORTS,
    OverlayProgressBarDirective,
    WidthTriggerDirective,
    ColorSketchModule,
    CommonModule,
    NoteBtnComponent,
    OverflowDirective,
    SnackbarComponent,
    OverlayModule,
    ConfirmPopOverComponent,
    RouterModule,
    QuillModule.forRoot(),
  ],
  declarations: [
    ...DECLARE_AND_EXPORT,
    ChangeEmailComponent,
    ChangePasswordComponent,
    ChangeUsernameComponent,
    DeleteAccountModalComponent,
  ],
  exports: [
    ...DECLARE_AND_EXPORT,
    ...COMMON_RE_EXPORTS,
    OverlayProgressBarDirective,
    WidthTriggerDirective,
    ConfirmPopOverComponent,
    QuillModule,
  ],
  providers: [
    DecimalPipe,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        panelClass: 'dt-mat-dialog-panel',
        hasBackdrop: true,
        backdropClass: 'dt-mat-dialog-backdrop',
        position: {
          top: '10vh',
        },
      },
    },
    {
      provide: MAT_ICON_DEFAULT_OPTIONS,
      useValue: {
        fontSet: 'material-symbols-outlined',
      },
    },
    {
      provide: MatPaginatorIntl,
      useClass: PaginatorIntlService,
    },
  ],
})
export class WidgetsModule {
  constructor(platform: Platform, quillConfigurationService: QuillConfigurationService) {
    quillConfigurationService.configureQuill();

    if (platform.WEBKIT) {
      document.body.classList.add('dt-webkit');
    }
  }
}
