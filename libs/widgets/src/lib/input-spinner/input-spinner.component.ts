import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'dt-input-spinner',
  templateUrl: './input-spinner.component.html',
  styleUrls: ['./input-spinner.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: InputSpinnerComponent,
    },
  ],
})
export class InputSpinnerComponent implements ControlValueAccessor {
  @Input() step = 1;

  @Input() disabled = false;
  value: number;
  touched = false;

  @Input() plusDisabled = false;
  @Input() minusDisabled = false;

  @Output() increase = new EventEmitter<void>();
  @Output() decrease = new EventEmitter<void>();

  onChange: (value: number) => void;
  onTouched: () => void;

  plusButton() {
    this.increase.emit();

    if (this.value != null) {
      this.value += this.step;
      this.notifyChange();
    }
  }

  minusButton() {
    this.decrease.emit();

    if (this.value != null) {
      this.value -= this.step;
      this.notifyChange();
    }
  }

  /*******************************************
   * ControlValueAccessor interface
   *******************************************/

  writeValue(obj: number): void {
    this.value = obj;
  }

  registerOnChange(fn: (value: number) => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  private notifyChange() {
    this.onChange?.(this.value);

    if (!this.touched) {
      this.touched = true;
      this.onTouched?.();
    }
  }
}
