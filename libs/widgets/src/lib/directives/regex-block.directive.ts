import { Directive, ElementRef, Input } from '@angular/core';

/*
  This directive wraps certain text defined by a regex into a <span style = "display: inline-block;"></span>
  block which prevents the content from being split up on a line break.
*/
@Directive({
  selector: '[dtRegexBlock]',
})
export class RegexBlockDirective {
  private _regexObj: RegExp;
  private _text: string;

  @Input() set text(text: string) {
    this._text = text;
    this.updateText();
  }
  @Input() set regex(regex: string | RegExp) {
    if (typeof regex === 'string') {
      this._regexObj = new RegExp(regex, 'gm');
    } else {
      this._regexObj = regex;
    }
    this.updateText();
  }

  constructor(private elementRef: ElementRef) {}

  updateText() {
    if (this._text) {
      this.elementRef.nativeElement.innerHTML = this._text.replace(this._regexObj, '<span style="display: inline-block;">$1</span>');
    }
  }
}
