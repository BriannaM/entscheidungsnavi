import { Directive, ElementRef, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[dtVisible]',
})
export class VisibleDirective {
  @Input('dtVisible')
  visible: boolean;

  constructor(private el: ElementRef) {}

  @HostBinding('style.visibility')
  get visibilityValue(): string {
    return this.visible ? 'visible' : 'hidden';
  }
}
