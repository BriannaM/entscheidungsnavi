import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MAT_SNACK_BAR_DATA, MatSnackBarModule, MatSnackBarRef } from '@angular/material/snack-bar';

export interface SnackbarData {
  message: string;
  icon?: string;
  dismissButton?: boolean;
  loadingSpinner?: boolean;
}

@Component({
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule, MatIconModule],
})
export class SnackbarComponent {
  constructor(public snackBarRef: MatSnackBarRef<SnackbarComponent>, @Inject(MAT_SNACK_BAR_DATA) public data: SnackbarData) {}

  dismissClick() {
    this.snackBarRef.dismiss();
  }
}
