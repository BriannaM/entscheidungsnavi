import { Component, Input } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  templateUrl: './whistle.component.html',
  styleUrls: ['./whistle.component.scss'],
})
export class WhistleComponent {
  @Input()
  text: string;

  @Input()
  icon: string;

  @Input()
  duration: number;

  @Input()
  iconColor: ThemePalette;

  get animationDelay() {
    return `0ms, ${this.duration}ms`;
  }
}
