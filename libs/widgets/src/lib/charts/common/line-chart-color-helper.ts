import { ColorHelper, Gradient, ScaleType } from '@swimlane/ngx-charts';
import { LineSeries } from '../common/chart-data-types';

export class LineChartColorHelper implements ColorHelper {
  // we only need the getColor method as this is only used for tooltips.
  // those properties are needed to implement ColorHelper.
  scale: any;
  scaleType: ScaleType;
  colorDomain: string[];
  domain: string[] | number[];
  customColors: any;

  constructor(private lineChartSeries: LineSeries[]) {}

  getColor(groupName: string): string {
    const result = this.lineChartSeries.find(value => value.name === groupName);
    if (result) {
      return result.color;
    } else {
      return '';
    }
  }

  generateColorScheme(_scheme: any, _type: ScaleType, _domain: string[] | number[]) {
    throw new Error('Method not implemented.');
  }
  getLinearGradientStops(_value: string | number, _start?: string | number): Gradient[] {
    throw new Error('Method not implemented.');
  }
}
