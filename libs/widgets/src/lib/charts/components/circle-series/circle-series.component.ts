import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef } from '@angular/core';
import { id, StyleTypes, ViewDimensions } from '@swimlane/ngx-charts';
import { ScaleLinear, ScalePoint, ScaleTime } from 'd3-scale';
import { defaults } from 'lodash';
import { CircleProperties, LineSeries, Point, ScaleType, TextCircleSeries, XScale, YScale } from '../../common';

class Circle implements CircleProperties {
  name: any; // x-value
  value: number; // y-value

  id: number; // A unique id by which this circle is identified (useful for animations)
  color: string;
  showText?: boolean;
  text?: string;
  textColor?: string;
  textPosition?: 'left' | 'right';

  tag?: any; // Taken from CircleProperties

  cx: number; // coordinates
  cy: number;
}

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-circle-series]',
  templateUrl: './circle-series.component.html',
  styleUrls: ['./circle-series.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CircleSeriesComponent implements OnChanges, OnInit {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  StyleTypes = StyleTypes;
  // We can display points with and without text
  @Input() series: LineSeries | TextCircleSeries;
  // Only the points whose x-values are in this array are shown, the others are hidden.
  // If this is null, all are shown.
  @Input() visibleXValues?: any[];
  // The points whose x-values are in this array have a shadow/are highlighted
  @Input() highlightedXValues: any[] = [];
  @Input() scaleType: ScaleType;
  @Input() xScale: XScale;
  @Input() yScale: YScale;
  @Input() dimensions: ViewDimensions;
  @Input() radius = 5;
  @Input() animations = true;
  @Input() tooltipDisabled = false;
  @Input() tooltipTemplate: TemplateRef<any>;

  hoveredCircle?: Circle;
  circles: Circle[];
  gradientId: string;
  gradientUrl: string;

  ngOnInit() {
    this.gradientId = `grad${id().toString()}`;
    this.gradientUrl = `url(#${this.gradientId})`;
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('series' in changes || 'xScale' in changes || 'yScale' in changes || 'scaleType' in changes) {
      this.update();
    }
  }

  update() {
    this.circles = (this.series.series as (Point<number> | Point<[number, CircleProperties]>)[]).map(p => this.mapDataPointToCircle(p));
  }

  circleTrackBy(_index: number, item: Circle) {
    return item.id;
  }

  private mapDataPointToCircle(d: Point<number> | Point<[number, CircleProperties]>): Circle {
    const color = 'circleColor' in this.series ? this.series.circleColor : this.series.color;
    let properties = {
      color,
      textColor: color,
      id: d.name, // default id is the x-value
    };

    let value: number;
    if (typeof d.value === 'number') {
      value = d.value;
    } else {
      value = d.value[0];
      properties = defaults(d.value[1], properties);
    }
    const label = d.name;

    let cx;
    if (this.scaleType === 'time') {
      cx = (this.xScale as ScaleTime<number, number>)(label);
    } else if (this.scaleType === 'linear') {
      cx = (this.xScale as ScaleLinear<number, number>)(Number(label));
    } else {
      cx = (this.xScale as ScalePoint<string>)(label);
    }
    const cy = this.yScale(value);

    return {
      name: label,
      value,
      ...properties,
      textPosition: cx > this.dimensions.width / 2 ? 'left' : 'right',
      cx,
      cy,
    };
  }

  mouseOver(circle: Circle) {
    this.hoveredCircle = circle;
  }
  mouseLeave() {
    this.hoveredCircle = null;
  }
}
