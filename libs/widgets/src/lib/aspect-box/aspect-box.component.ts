import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-aspect-box',
  templateUrl: './aspect-box.component.html',
  styleUrls: ['./aspect-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspectBoxComponent {
  @Input() name: string;
  @Output() nameChange = new EventEmitter<string>();
  @Input() readonly = false;
  @Input() grayedOut = false;
  @Output() deleteClick = new EventEmitter<void>();
  @Input() showDeleteButton = true;
  @Input() currentlyDraggingBox = false;
  @Input() currentlyDragging = true;
  isEditing = false;

  onNameChange(newName: string): void {
    this.nameChange.emit(newName);
  }

  constructor(private cdRef: ChangeDetectorRef) {}

  edit() {
    this.isEditing = true;
    this.cdRef.markForCheck();
  }
}
