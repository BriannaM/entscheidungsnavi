import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PlatformDetectService {
  macOS = !!navigator?.userAgent?.includes('Macintosh');
  iPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
}
