import { Pipe, PipeTransform } from '@angular/core';
import '@entscheidungsnavi/tools/number_rounding';
import { round } from 'lodash';

@Pipe({
  name: 'roundAsPower',
})
export class NumberRoundingAsPower implements PipeTransform {
  transform(value: number, sigDigits = 0): string {
    if (value < 999999 && value > -999999) {
      return String(value.round(sigDigits));
    }

    let power = 0;
    while (value >= 1000 || value <= -1000) {
      value /= 1000;
      power += 3;
    }

    const sup = {
      '0': '⁰',
      '1': '¹',
      '2': '²',
      '3': '³',
      '4': '⁴',
      '5': '⁵',
      '6': '⁶',
      '7': '⁷',
      '8': '⁸',
      '9': '⁹',
    };
    const regex = RegExp(`[${Object.keys(sup).join('')}]`, 'g');

    return round(value, 2) + ' × 10' + String(power).replace(regex, c => sup[c]);
  }
}
