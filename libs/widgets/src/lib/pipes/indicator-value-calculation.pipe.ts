import { Pipe, PipeTransform } from '@angular/core';
import { calculateIndicatorValue } from '@entscheidungsnavi/decision-data/calculation';
import { Indicator } from '@entscheidungsnavi/decision-data/classes';

/**
 * Example Usage: {{ value | indicatorValue: indicator }}
 *
 * Convert indicator vector to indicator value.
 */
@Pipe({ name: 'indicatorValue', pure: false, standalone: true })
export class IndicatorValueCalculationPipe implements PipeTransform {
  transform(values: number[], indicator: Indicator): number {
    return calculateIndicatorValue(values, indicator);
  }
}
