import { Pipe, PipeTransform } from '@angular/core';

/**
 * Example Usage: {{ value | numberCast }}
 *
 * Casts the given value to a number.
 */

@Pipe({ name: 'numberCast' })
export class NumberCastPipe implements PipeTransform {
  transform(value: any): number {
    return value as number;
  }
}
