import { Observable, Observer } from 'rxjs';

export class FileExport {
  static download(filename: string, text: string) {
    const getBlob = (content: string) => new Blob([content], { type: 'application/json' });

    try {
      const blob = getBlob(text);
      const element = document.createElement('a');
      element.setAttribute('href', URL.createObjectURL(blob));
      element.setAttribute('download', filename);

      element.style.display = 'none';
      document.body.appendChild(element);

      element.click();

      document.body.removeChild(element);
    } catch (e) {
      console.error(e);
    }
  }

  static readFile(file: File): Observable<string> {
    return new Observable((observer: Observer<string>) => {
      const fileReader = new FileReader();
      fileReader.onload = () => {
        observer.next(fileReader.result as string);
        observer.complete();
      };
      fileReader.onerror = error => observer.error(error);
      fileReader.readAsText(file);
    });
  }
}
