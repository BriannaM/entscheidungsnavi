import { ContentChild, ContentChildren, Directive, QueryList } from '@angular/core';
import { AbstractSelectItemDirective, SelectLabelDirective, SelectOptionDirective } from '.';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'dt-select-group',
  providers: [{ provide: AbstractSelectItemDirective, useExisting: SelectGroupDirective }],
})
export class SelectGroupDirective extends AbstractSelectItemDirective {
  @ContentChild(SelectLabelDirective) label: SelectLabelDirective;
  @ContentChildren(SelectOptionDirective) groupedOptions: QueryList<SelectOptionDirective>;
}
