export interface LocalizedString {
  en: string;
  de: string;
}
