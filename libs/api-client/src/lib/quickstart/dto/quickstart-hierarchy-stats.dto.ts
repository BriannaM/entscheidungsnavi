import { Expose, Type } from 'class-transformer';
import { IsDefined, IsNumber, ValidateNested } from 'class-validator';
import { QuickstartHierarchyListWithStats, QuickstartHierarchyWithStats } from '@entscheidungsnavi/api-types';
import {
  QuickstartHierarchyTreeValueDto,
  QuickstartHierarchyTreeDto,
  QuickstartHierarchyDto,
  QuickstartHierarchyListDto,
} from './quickstart-hierarchy.dto';

export class QuickstartHierarchyTreeValueWithStatsDto extends QuickstartHierarchyTreeValueDto {
  @Expose()
  accumulatedScore: number;
}

export class QuickstartHierarchyTreeWithStatsDto extends QuickstartHierarchyTreeDto {
  @IsDefined()
  @ValidateNested()
  @Type(() => QuickstartHierarchyTreeValueWithStatsDto)
  override value: QuickstartHierarchyTreeValueWithStatsDto;

  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartHierarchyTreeWithStatsDto)
  override children: QuickstartHierarchyTreeWithStatsDto[];
}

export class QuickstartHierarchyWithStatsDto extends QuickstartHierarchyDto implements QuickstartHierarchyWithStats {
  @IsDefined()
  @ValidateNested()
  @Type(() => QuickstartHierarchyTreeWithStatsDto)
  override tree: QuickstartHierarchyTreeWithStatsDto;

  @IsNumber()
  totalAccumulatedScore: number;
}

export class QuickstartHierarchyListWithStatsDto extends QuickstartHierarchyListDto implements QuickstartHierarchyListWithStats {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartHierarchyWithStatsDto)
  override items: QuickstartHierarchyWithStatsDto[];
}
