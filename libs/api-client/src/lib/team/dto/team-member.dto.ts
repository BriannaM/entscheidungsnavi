import { Type } from 'class-transformer';
import { IsMongoId, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { TeamMember as ApiTeamMember } from '@entscheidungsnavi/api-types';
import { TeamCommentDto } from './team-comment.dto';
import { PartialUserInfoDto } from './partial-user-info.dto';

export class TeamMemberDto implements ApiTeamMember {
  @IsString()
  id: string;

  @IsNotEmpty()
  @Type(() => PartialUserInfoDto)
  @ValidateNested()
  user: PartialUserInfoDto;

  @IsMongoId()
  project: string;

  @Type(() => TeamCommentDto)
  @ValidateNested({ each: true })
  comments: TeamCommentDto[];

  @Type(() => String)
  unreadComments: string[];
}
