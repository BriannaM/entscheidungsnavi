import { TeamPartialUserInfo as APITeamPartialUserInfo } from '@entscheidungsnavi/api-types';
import { IsMongoId, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class PartialUserInfoDto implements APITeamPartialUserInfo {
  @IsMongoId()
  id: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  email: string;
}
