import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { catchError, of } from 'rxjs';
import { ProjectUpdate } from '@entscheidungsnavi/api-types';
import { ProjectDto, ProjectWithDataDto } from '../projects/dto/project.dto';
import { TeamDto } from './dto/team.dto';
import { TeamCommentDto } from './dto/team-comment.dto';
import { TeamInviteDto } from './dto/team-invite.dto';
import { TeamInviteInfoDto } from './dto/team-invite-info.dto';

@Injectable({
  providedIn: 'root',
})
export class TeamsService {
  constructor(private http: HttpClient) {}

  createTeam(projectId: string, teamName: string) {
    return this.http.post<unknown>(`/api/teams`, { projectId, teamName }).pipe(transformAndValidate(TeamDto), logError());
  }

  getTeamComments(teamId: string, memberId: string) {
    return this.http.get<unknown[]>(`/api/teams/${teamId}/${memberId}/comments`).pipe(transformAndValidate(TeamCommentDto), logError());
  }

  getTeamCommentsForObject(teamId: string, memberId: string, objectId: string) {
    return this.http
      .get<unknown[]>(`/api/teams/${teamId}/${memberId}/comments/object/${objectId}`)
      .pipe(transformAndValidate(TeamCommentDto), logError());
  }

  getTeamProject(teamId: string, memberId: string) {
    return this.http.get<unknown>(`/api/teams/${teamId}/${memberId}/project`).pipe(transformAndValidate(ProjectWithDataDto), logError());
  }

  invite(teamId: string, email: string) {
    return this.http.post<unknown[]>(`/api/teams/${teamId}/invite`, { email }).pipe(transformAndValidate(TeamInviteDto), logError());
  }

  join(token: string) {
    return this.http.post<unknown>(`/api/teams/invites/${token}`, {}).pipe(transformAndValidate(ProjectDto), logError());
  }

  addComment(teamId: string, memberId: string, objectId: string, content: string) {
    return this.http
      .post<unknown[]>(`/api/teams/${teamId}/${memberId}/comments`, {
        objectId,
        content,
      })
      .pipe(transformAndValidate(TeamCommentDto), logError());
  }

  deleteComment(teamId: string, memberId: string, commentId: string) {
    return this.http.delete<void>(`/api/teams/${teamId}/${memberId}/comments/${commentId}`).pipe(logError());
  }

  getTeam(teamId: string) {
    return this.http.get<unknown>(`/api/teams/${teamId}`).pipe(transformAndValidate(TeamDto), logError());
  }

  transferObject(teamId: string, sourceMemberId: string, objectId: string) {
    return this.http.post<void>(`/api/teams/${teamId}/${sourceMemberId}/transfer`, { objectId });
  }

  getInviteInfo(token: string) {
    return this.http.get<unknown>(`/api/teams/invites/${token}`).pipe(transformAndValidate(TeamInviteInfoDto));
  }

  updateTeam(teamId: string, update: { name?: string; editor?: string; whiteboard?: string }) {
    return this.http.patch<unknown>(`/api/teams/${teamId}`, update).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status === 409) {
          return of(error.error as unknown);
        }

        throw error;
      }),
      transformAndValidate(TeamDto)
    );
  }

  updateMainProject(teamId: string, updatedData: ProjectUpdate) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/teams/${teamId}/mainproject`, updatedData, { headers })
      .pipe(transformAndValidate(ProjectDto), logError());
  }
}
