import { KlugProjectList } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDefined, IsNumber, ValidateNested } from 'class-validator';
import { KlugProjectDto } from './klug-project.dto';

export class KlugProjectListDto implements KlugProjectList {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => KlugProjectDto)
  items: KlugProjectDto[];

  @IsNumber()
  count: number;
}
