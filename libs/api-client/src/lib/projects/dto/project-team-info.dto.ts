import { ProjectTeamInfo } from '@entscheidungsnavi/api-types';
import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class ProjectTeamInfoDto implements ProjectTeamInfo {
  @IsMongoId()
  id: string;

  @IsString()
  @IsNotEmpty()
  name: string;
}
