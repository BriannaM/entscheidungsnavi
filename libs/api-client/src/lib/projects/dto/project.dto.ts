import { Project as ApiProject, ProjectWithData } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsMongoId, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator';
import { ProjectTeamInfoDto } from './project-team-info.dto';

export class ProjectDto implements ApiProject {
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  name: string;

  @IsMongoId()
  userId: string;

  @IsOptional()
  @IsString()
  shareToken?: string;

  @Type(() => ProjectTeamInfoDto)
  @IsOptional()
  @ValidateNested()
  team?: ProjectTeamInfoDto;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class ProjectWithDataDto extends ProjectDto implements ProjectWithData {
  @IsString()
  data: string;
}
