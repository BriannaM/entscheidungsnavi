import { normalizeDiscrete, normalizeDiscretePrioritized } from './normalize';

describe('normalize', () => {
  it('values array check', () => {
    expect(() => normalizeDiscrete([])).toThrow();
    expect(() => normalizeDiscrete([1, 1])).not.toThrow();
  });

  it('all equal', () => {
    expect(normalizeDiscrete([1, 1, 1, 1])).toEqual([25, 25, 25, 25]);
  });

  it('all 0', () => {
    expect(normalizeDiscrete([0, 0, 0, 0])).toEqual([25, 25, 25, 25]);
  });

  it('all equal, sumValues < targetSum', () => {
    expect(normalizeDiscrete([1, 1, 1])).toEqual([34, 33, 33]);
  });

  it('custom step size', () => {
    expect(normalizeDiscrete([1, 1, 1], 1, 0.01)).toEqual([0.34, 0.33, 0.33]);
  });

  it('normalize([60, 1], 1, 0.01)', () => {
    expect(normalizeDiscrete([60, 1], 1, 0.01)).toEqual([0.98, 0.02]);
  });

  it('round to 0', () => {
    expect(normalizeDiscrete([60, 1], 1, 0.1)).toEqual([1, 0]);
  });

  it('normalize([30, 38, 1])', () => {
    expect(normalizeDiscrete([30, 38, 1])).toEqual([44, 55, 1]);
  });

  it('sumValues > targetSum', () => {
    expect(normalizeDiscrete([1, 1], 99)).toEqual([50, 49]);
  });
});

describe('normalizePrioritized', () => {
  it('prioritized check', () => {
    expect(() => normalizeDiscretePrioritized([1, 1], [])).toThrow();
    expect(() => normalizeDiscretePrioritized([1, 1], [1])).toThrow();
    expect(() => normalizeDiscretePrioritized([1, 1], [1, 0])).not.toThrow();
    expect(() => normalizeDiscretePrioritized([1, 1], [1, 0, 1])).toThrow();
  });

  it('all not prioritized', () => {
    expect(normalizeDiscretePrioritized([1, 1, 1, 1], [0, 0, 0, 0])).toEqual([25, 25, 25, 25]);
  });

  it('partially prioritized, equal', () => {
    expect(normalizeDiscretePrioritized([1, 1, 1, 1], [0, 1, 0, 1])).toEqual([49, 1, 49, 1]);
  });

  it('partially prioritized, all 0', () => {
    expect(normalizeDiscretePrioritized([0, 0, 0, 0], [0, 1, 0, 1])).toEqual([50, 0, 50, 0]);
  });

  it('sumValues > targetSum', () => {
    expect(normalizeDiscretePrioritized([1, 1, 1], [1, 0, 0])).toEqual([1, 50, 49]);
  });

  it('custom step size', () => {
    expect(normalizeDiscretePrioritized([0.01, 1, 1], [1, 0, 0], 1, 0.01)).toEqual([0.01, 0.5, 0.49]);
  });

  it('all prioritized', () => {
    expect(normalizeDiscretePrioritized([60, 1], [1, 1], 1, 0.01)).toEqual([0.98, 0.02]);
  });

  it('all prioritized, all 0', () => {
    expect(normalizeDiscretePrioritized([0, 0], [1, 1], 1, 0.01)).toEqual([0.5, 0.5]);
  });

  it('all prioritized, partially 0', () => {
    expect(normalizeDiscretePrioritized([3, 2, 0], [1, 1, 1])).toEqual([60, 40, 0]);
  });

  it('highPrioSum === targetSum', () => {
    expect(normalizeDiscretePrioritized([60, 1], [0, 1], 1, 0.1)).toEqual([0, 1]);
  });

  it('normalizePrioritized([60, 35, 30], [1, 0, 1])', () => {
    expect(normalizeDiscretePrioritized([60, 35, 30], [1, 0, 1])).toEqual([60, 10, 30]);
  });

  it('highPrioSum > targetSum', () => {
    expect(normalizeDiscretePrioritized([60, 55, 30], [1, 1, 0])).toEqual([52, 48, 0]);
  });

  it('prioritized rounding', () => {
    expect(normalizeDiscretePrioritized([6, 55, 30], [1, 0, 0], 100, 5)).toEqual([5, 60, 35]);
  });

  it('multiple priorities', () => {
    expect(normalizeDiscretePrioritized([3, 5, 2, 69, 49, 10], [0, 3, 2, 3, 2, 1])).toEqual([0, 5, 1, 69, 25, 0]);
  });
});
