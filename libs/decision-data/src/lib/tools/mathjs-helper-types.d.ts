declare module 'mathjs/number' {
  export {
    create,
    parseDependencies,
    compileDependencies,
    addDependencies,
    subtractDependencies,
    divideDependencies,
    multiplyDependencies,
    expDependencies,
    logDependencies,
    sqrtDependencies,
    cosDependencies,
    sinDependencies,
    tanDependencies,
    unaryMinusDependencies,
  } from 'mathjs';
}
