// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="./mathjs-helper-types.d.ts" />
import {
  create,
  parseDependencies,
  compileDependencies,
  addDependencies,
  subtractDependencies,
  divideDependencies,
  multiplyDependencies,
  expDependencies,
  logDependencies,
  sqrtDependencies,
  cosDependencies,
  sinDependencies,
  tanDependencies,
  unaryMinusDependencies,
} from 'mathjs/number';

// Disable certain mathjs functions for security reasons. Note, that we do not disable the parse-function
// as it is required for calling math.compile.
// https://mathjs.org/docs/expressions/security.html
const math = create(
  {
    parseDependencies,
    compileDependencies,
    addDependencies,
    subtractDependencies,
    divideDependencies,
    multiplyDependencies,
    expDependencies,
    logDependencies,
    sqrtDependencies,
    cosDependencies,
    sinDependencies,
    tanDependencies,
    unaryMinusDependencies,
  },
  {}
);

math.import(
  {
    import: function () {
      throw new Error('Function import is disabled');
    },
    createUnit: function () {
      throw new Error('Function createUnit is disabled');
    },
    evaluate: function () {
      throw new Error('Function evaluate is disabled');
    },
    simplify: function () {
      throw new Error('Function simplify is disabled');
    },
    derivative: function () {
      throw new Error('Function derivative is disabled');
    },
  },
  { override: true }
);

export function parseFormula(formula: string) {
  return math.parse(formula);
}

export function compileFormula(formula: string) {
  return parseFormula(formula).compile();
}

export function replaceSymbols(formula: string, replacements: { [key: string]: string }) {
  const calc = parseFormula(formula);
  calc.traverse(node => {
    if (isSymbolNode(node) && node.name in replacements) {
      node.name = replacements[node.name];
    }
  });
  return calc.toString();
}

function isSymbolNode(node: math.MathNode): node is math.SymbolNode {
  return node.type === 'SymbolNode';
}
