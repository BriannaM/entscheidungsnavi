import { appendRichText, ensureIsRichText, isRichTextEmpty } from '@entscheidungsnavi/tools';

/**
 * Up until version 7.5, numerical and verbal objectives had separate explanations and comments.
 * These two fields were consolidated into one.
 *
 * @param data - The imported DecisionData object to be migrated
 */
export function migrateObjectiveExplanationsV75(data: any) {
  data.objectives.forEach((objective: any) => {
    if (objective.numericalData?.explanationFrom) {
      objective.numericalData.commentFrom = isRichTextEmpty(objective.numericalData.commentFrom)
        ? ensureIsRichText(objective.numericalData.explanationFrom)
        : appendRichText(objective.numericalData.commentFrom, '\n' + objective.numericalData.explanationFrom);
    }
    if (objective.numericalData?.explanationTo) {
      objective.numericalData.commentTo = isRichTextEmpty(objective.numericalData.commentTo)
        ? ensureIsRichText(objective.numericalData.explanationTo)
        : appendRichText(objective.numericalData.commentTo, '\n' + objective.numericalData.explanationTo);
    }

    if (objective.verbalData?.explanations && objective.verbalData?.comments) {
      objective.verbalData.explanations.forEach((explanation: string, index: number) => {
        if (explanation.length === 0) return;

        objective.verbalData.comments[index] = isRichTextEmpty(objective.verbalData.comments[index])
          ? ensureIsRichText(explanation)
          : appendRichText(objective.verbalData.comments[index], '\n' + explanation);
      });
    }
  });
}
