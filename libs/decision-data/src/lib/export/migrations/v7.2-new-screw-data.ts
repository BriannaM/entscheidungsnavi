// Screw.positions was renamed to Screw.states
export function migrateScrewPositionNameChangeV72(data: any) {
  if (data.hintAlternatives.screws) {
    data.hintAlternatives.screws.forEach((screw: any) => {
      if (screw.positions) {
        screw.states = screw.positions;
      }
    });

    data.alternatives.forEach((alternative: any) => {
      if (!alternative.screwConfiguration) {
        alternative.screwConfiguration = [];
      }

      if (alternative.screwConfiguration.length !== data.hintAlternatives.screws.length) {
        alternative.screwConfiguration = new Array(data.hintAlternatives.screws.length).fill(null);
      }
    });
  }
}
