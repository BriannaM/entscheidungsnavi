export function migrateLegacyPropertiesV55(data: any) {
  migrateGermanPropertyNames(data);
  migrateSplitHintAlternatives(data);
  migrateSplitWeights(data);
  migrateProjectNotes(data);
  migrateExportVersion(data);
}

/**
 * Up until version 5.5, the four top-level properties had german names.
 * @param data - DecisionData object to be migrated
 */
function migrateGermanPropertyNames(data: any) {
  if (data.entscheidungsproblem) {
    data.decisionProblem = data.entscheidungsproblem;
  }
  if (data.alternativen) {
    data.alternatives = data.alternativen;
  }
  if (data.unsicherheitsfaktoren) {
    data.influenceFactors = data.unsicherheitsfaktoren;
  }
  if (data.auspraegungen) {
    data.outcomes = data.auspraegungen;
  }
  if (data.ziele) {
    data.objectives = data.ziele;
  }
}

/**
 * Up until version 5.5, hintAlternatives.\{alternatives, ideas, screws\} were saved as top-level
 * fields hint_alternatives, hint_alternative_ideas, hint_alternative_screws.
 * @param data - DecisionData object to be migrated
 */
function migrateSplitHintAlternatives(data: any) {
  if (!data.hintAlternatives) {
    data.hintAlternatives = {};
  }

  if (data.hint_alternatives) {
    data.hintAlternatives.alternatives = data.hint_alternatives;
  }
  if (data.hint_alternative_ideas) {
    data.hintAlternatives.ideas = data.hint_alternative_ideas;
  }
  if (data.hint_alternative_screws) {
    data.hintAlternatives.screws = data.hint_alternative_screws;
  }
}

/**
 * Up until version 5.5, the properties of weights were saved as top-level fields.
 * @param data - DecisionData object to be migrated
 */
function migrateSplitWeights(data: any) {
  if (!data.weights) {
    data.weights = {};
  }

  if (data.ziel1_idx != null) {
    data.weights.tradeoffObjectiveIdx = data.ziel1_idx;
  }
  if (data.gewichtung) {
    data.weights.preliminaryWeights = data.gewichtung;
  }
  if (data.gewichtung_post) {
    data.weights.tradeoffWeights = data.gewichtung_post;
  }
  if (data.unverified_gewichtung_post) {
    data.weights.unverifiedWeights = data.unverified_gewichtung_post;
  }
  if (data.manual_tradeoffs) {
    data.weights.manualTradeoffs = data.manual_tradeoffs;
  }
  if (data.tradeoff_explanations) {
    data.weights.explanations = data.tradeoff_explanations;
  }
}

/**
 * Up until version 5.5, the projectNotes were saved in a different field.
 * @param data - DecisionData object to be migrated
 */
function migrateProjectNotes(data: any) {
  if (data.notiz_text) {
    data.projectNotes = data.notiz_text;
  }
}

/**
 * Up until version 5.5, the the exportVersion field had a different name.
 * @param data - DecisionData object to be migrated
 */
function migrateExportVersion(data: any) {
  if (data.export_version) {
    data.exportVersion = data.export_version;
  }
}
