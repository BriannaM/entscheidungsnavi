import { pick } from 'lodash';
import { DecisionData } from '../decision-data';
import { PROPERTY_NAMES } from './properties';

/**
 * Exports the given decision data object into a string.
 *
 * @param data - The object to be exported
 * @param exportVersion - The software version used for this export
 */
export function dataToText(data: DecisionData, exportVersion: string): string {
  const exportData = { exportVersion, ...pick(data, PROPERTY_NAMES) };
  return JSON.stringify(exportData);
}
