import { PredefinedInfluenceFactor, PredefinedScenario } from './predefined-influence-factor';
import { UserdefinedInfluenceFactor, UserdefinedScenario } from './userdefined-influence-factor';

export type InfluenceFactor = UserdefinedInfluenceFactor | PredefinedInfluenceFactor;

export type InfluenceFactorScenario = UserdefinedScenario | PredefinedScenario;
