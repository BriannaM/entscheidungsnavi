export type IssueCategory = (typeof ISSUE_CATEGORIES)[number];
export const ISSUE_CATEGORIES = ['notAssigned', 'decisionStatement', 'objectives', 'alternatives', 'influenceFactors'] as const;
