import { ObjectiveInput } from './objective';

export const PREDEFINED_INFLUENCE_FACTOR_IDS = ['3-Scenarios'] as const;
export type PredefinedInfluenceFactorId = (typeof PREDEFINED_INFLUENCE_FACTOR_IDS)[number];

export class PredefinedState {
  /**
   * @param probability - Is in [0, 100]
   */
  constructor(public probability: number) {}
}

export interface PredefinedScenario {
  value: ObjectiveInput;
  stateIndices: number[];
  probability: number;
}

export class PredefinedInfluenceFactor {
  constructor(public id: PredefinedInfluenceFactorId, public states: PredefinedState[] = []) {}

  generateScenarios(outcomeValues: ObjectiveInput[]): PredefinedScenario[] {
    return generatePredefinedScenarios(
      outcomeValues,
      this.states.map(state => state.probability / 100)
    );
  }
}

/*
  Hardcoded predefined influence factors.
*/
export const PREDEFINED_INFLUENCE_FACTORS: Record<PredefinedInfluenceFactorId, PredefinedInfluenceFactor> = {
  '3-Scenarios': new PredefinedInfluenceFactor('3-Scenarios', [new PredefinedState(25), new PredefinedState(50), new PredefinedState(25)]),
};

/**
 * Compute all possible scenarios for a predefined influence factor in an indicator objective.
 * Since indicators are independent, this is a much longer list than the state list.
 *
 * For example from outcome.values = [[1, 3, 5], [0.1, 0.3, 0.5], [10, 30, 50]] the following combinations are generated:
 * [
 *  (0)  \{ combinations: [1, 3, 5],     indexes: [0, 0, 0] \}
 *  (1)  \{ combinations: [1, 3, 0.5],   indexes: [0, 0, 1] \}
 *  (2)  \{ combinations: [1, 3, 50],    indexes: [0, 0, 2] \}
 *  (3)  \{ combinations: [1, 0.3, 5],   indexes: [0, 1, 0] \}
 *  (4)  \{ combinations: [1, 0.5, 0.5], indexes: [0, 1, 1] \}
 *  (5)  \{ combinations: [1, 0.3, 50],  indexes: [0, 1, 2] \}
 *  ...
 *  (26) \{ combinations: [10, 30, 50],  indexes: [2, 2, 2] \}
 * ]
 *
 * @param indicatorOutcomeValues - The outcome values for every indicator in every state
 * @param stateProbabilities - Probability for every state in [0, 1]
 */
export function generatePredefinedScenarios(indicatorOutcomeValues: ObjectiveInput[], stateProbabilities: number[]): PredefinedScenario[] {
  // stateCount must always be > 0
  const stateCount = indicatorOutcomeValues.length;
  const indicatorCount = indicatorOutcomeValues[0].length;

  const combinations = new Array<PredefinedScenario>(stateCount ** indicatorCount);
  let currentCombinationIndex = 0;

  function findCombinations(combination: ObjectiveInput, stateIndices: number[], indicatorIndex: number) {
    for (let stateIndex = 0; stateIndex < stateCount; stateIndex++) {
      const combinationsCopy = combination.slice(0);
      const indexesCopy = stateIndices.slice(0);

      combinationsCopy.push(indicatorOutcomeValues[stateIndex][indicatorIndex]); // save element
      indexesCopy.push(stateIndex); // save element index

      if (indicatorIndex == indicatorCount - 1) {
        combinations[currentCombinationIndex++] = {
          value: combinationsCopy,
          stateIndices: indexesCopy,
          probability: indexesCopy.map(idx => stateProbabilities[idx]).reduce((a, b) => a * b),
        };
      } else {
        findCombinations(combinationsCopy, indexesCopy, indicatorIndex + 1);
      }
    }
  }
  findCombinations([], [], 0);

  return combinations;
}
