export class UtilityFunction {
  constructor(public c = 0, public precision = 0, public width?: number, public level?: number, public explanation = '') {}
}
