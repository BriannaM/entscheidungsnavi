import { ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import MongoMemoryServer from 'mongodb-memory-server-core';
import mongoose from 'mongoose';
import request from 'supertest';
import { NestExpressApplication } from '@nestjs/platform-express';
import session from 'express-session';
import passport from 'passport';
import { noop } from 'lodash';
import { AppModule } from '../src/app/app.module';
import { USERS, loginUser } from './test-helper';

jest.setTimeout(30000);

describe('Events (e2e)', () => {
  let app: NestExpressApplication;
  let mongod: MongoMemoryServer;

  let eventManagerCookie: string[];
  let basicUserCookie: string[];

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = await mongod.getUri('navi');

    await mongoose.connect(uri);

    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue({
        get: (key: string) => {
          switch (key) {
            case 'NODE_ENV':
              return 'test';
            case 'BASE_URL':
              return 'http://dev.entscheidungsnavi.de';
            case 'MONGODB_URI':
              return uri;
            default:
              return null;
          }
        },
      })
      .compile();

    app = module
      .createNestApplication<NestExpressApplication>()
      .setGlobalPrefix('/api')
      .use(session({ secret: 'testingsecret', resave: false, saveUninitialized: false }))
      .use(passport.initialize())
      .use(passport.session());

    // Override default session middlewares
    app.get(AppModule).configure = noop;

    await app.init();

    // Setup some users
    const { db } = mongoose.connection;
    await db.collection('users').insertMany([USERS.user, USERS.eventManager]);

    // Log the users in
    basicUserCookie = await loginUser(app, 'user');
    eventManagerCookie = await loginUser(app, 'eventManager');
  });

  afterAll(async () => {
    await app.close();
    await mongoose.disconnect();
    await mongod.stop();
  });

  beforeEach(async () => {
    // Clear the events table
    await mongoose.connection.db.collection('events').deleteMany({});
  });

  it('requires the event-manager role for event creation', async () => {
    await request(app.getHttpServer()).post('/api/events').send({ name: 'new event' }).set('Cookie', basicUserCookie).expect(403);
  });

  it('retrieves events for non event-manager editors', async () => {
    await request(app.getHttpServer())
      .post('/api/events')
      .send({ name: 'new event', editors: [{ email: 'user@entscheidungsnavi.de' }] })
      .set('Cookie', eventManagerCookie)
      .expect(201);

    const result = (await request(app.getHttpServer()).get('/api/events').set('Cookie', basicUserCookie).expect(200)).body;
    expect(result).toHaveLength(1);
  });

  it('create event, get events, update event, get event', async () => {
    await request(app.getHttpServer()).post('/api/events').send({ name: 'new event' }).set('Cookie', eventManagerCookie).expect(201);

    const events = (await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body;
    expect(events.length).toBe(1);
    expect(events[0].name).toBe('new event');

    await request(app.getHttpServer())
      .patch(`/api/events/${events[0].id}`)
      .send({ name: 'updated' })
      .set('Cookie', eventManagerCookie)
      .expect(200);

    const event = (await request(app.getHttpServer()).get(`/api/events/${events[0].id}`).set('Cookie', eventManagerCookie).expect(200))
      .body;
    expect(event.name).toBe('updated');
  });

  it('creates an event and changes its permission', async () => {
    await request(app.getHttpServer()).post('/api/events').send({ name: 'new event' }).set('Cookie', eventManagerCookie).expect(201);

    const events = (await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body;
    expect(events.length).toBe(1);

    await request(app.getHttpServer())
      .patch(`/api/events/${events[0].id}`)
      .send({ owner: { email: 'non-existent@entscheidungsnavi.de' } })
      .set('Cookie', eventManagerCookie)
      .expect(404);

    await request(app.getHttpServer())
      .patch(`/api/events/${events[0].id}`)
      .send({ owner: { email: 'user@entscheidungsnavi.de' } })
      .set('Cookie', eventManagerCookie)
      .expect(200);

    // The manager has lost access
    expect((await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body.length).toBe(0);
  });
});
