import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { TeamComment, TeamCommentSchema } from './team-comment.schema';

export type TeamMemberDocument = TeamMember & Document;

@Schema()
export class TeamMember {
  @Prop({ required: true, type: Types.ObjectId, ref: 'User' })
  user: Types.ObjectId;

  @Prop({ required: true, type: Types.ObjectId, ref: 'Project' })
  project: Types.ObjectId;

  // These are the comments on this members project, NOT the comments this member made.
  @Prop({ required: true, type: [TeamCommentSchema], default: [] })
  comments: Types.DocumentArray<TeamComment>;

  @Prop({ type: [Types.ObjectId], ref: 'TeamComment' })
  unreadComments: Types.Array<Types.ObjectId>;
}

export const TeamMemberSchema = SchemaFactory.createForClass(TeamMember);
