import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ProjectHistoryEntryDocument = ProjectHistoryEntry & Document;

@Schema({ timestamps: false })
export class ProjectHistoryEntry {
  readonly id: string;

  @Prop({ required: true })
  compressedPatch: Buffer;

  // Whether or not this history entry was the result of a version conflict
  @Prop({ default: false })
  resolvesConflict: boolean;

  // The timestamp of the version we reach when applying this patch
  @Prop({ required: true })
  versionTimestamp: Date;
}

export const ProjectHistoryEntrySchema = SchemaFactory.createForClass(ProjectHistoryEntry);
