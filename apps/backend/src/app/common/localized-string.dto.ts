import { LocalizedString } from '@entscheidungsnavi/api-types';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
export class LocalizedStringDto implements LocalizedString {
  @Expose()
  @IsString()
  @IsNotEmpty()
  en: string;

  @Expose()
  @IsString()
  @IsNotEmpty()
  de: string;
}
