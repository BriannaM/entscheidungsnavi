import { Language as LanguageType, LANGUAGES } from '@entscheidungsnavi/api-types';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const Lang = createParamDecorator((_data: unknown, ctx: ExecutionContext): LanguageType => {
  const req = ctx.switchToHttp().getRequest<Request>();
  return (req.acceptsLanguages(...LANGUAGES) as LanguageType) || 'de';
});
