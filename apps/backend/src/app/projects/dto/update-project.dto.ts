import { PartialType } from '@nestjs/mapped-types';
import { IsBoolean, IsNumber, IsOptional, IsString, ValidateIf } from 'class-validator';
import { CreateProjectDto } from './create-project.dto';

export class UpdateProjectDto extends PartialType(CreateProjectDto) {
  @IsOptional()
  @IsString()
  dataPatch?: string;

  // We need the hash when we are patching
  @ValidateIf(o => o.dataPatch != null)
  @IsNumber()
  oldDataHash?: number;

  @IsOptional()
  @IsBoolean()
  resolvesConflict?: boolean;
}
