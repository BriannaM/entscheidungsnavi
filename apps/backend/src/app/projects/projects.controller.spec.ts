import { ClassSerializerInterceptor, ExecutionContext, INestApplication, ValidationPipe } from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { Types } from 'mongoose';
import request from 'supertest';
import { LoginGuard } from '../auth/login.guard';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';

const userId = new Types.ObjectId();

describe('ProjectsController', () => {
  let app: INestApplication;
  let createProject: jest.Mock;
  let updateProject: jest.Mock;

  beforeAll(async () => {
    createProject = jest.fn();
    updateProject = jest.fn();

    const module = await Test.createTestingModule({
      controllers: [ProjectsController],
      providers: [
        {
          provide: ProjectsService,
          useValue: { create: createProject, update: updateProject, getDataFromHistory: jest.fn().mockResolvedValue('somedata') },
        },
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({ whitelist: true, transform: true, transformOptions: { exposeUnsetFields: false } }),
        },
        { provide: APP_INTERCEPTOR, useValue: ClassSerializerInterceptor },
      ],
    })
      .overrideGuard(LoginGuard)
      .useValue({
        canActivate: (context: ExecutionContext) => {
          const req = context.switchToHttp().getRequest();
          req.user = { _id: userId, emailConfirmed: true };
          return true;
        },
      })
      .compile();

    app = await module.createNestApplication().init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('POST /projects', () => {
    const projectId = new Types.ObjectId();
    const projectRequest = { name: 'somename', data: '{ "version": "6.0.1" }' };
    const projectDto = { ...projectRequest, id: projectId.toString() };

    beforeAll(() => {
      createProject.mockResolvedValue(projectDto);
    });

    it('creates a project', async () => {
      await request(app.getHttpServer()).post('/projects').send(projectRequest).expect(201).expect(projectDto);

      expect(createProject.mock.calls[0][0]).toBe(userId);
      expect(createProject.mock.calls[0][1]).toEqual(projectRequest);
    });

    it('fails on missing data', () => {
      return request(app.getHttpServer()).post('/projects').send({ name: 'test' }).expect(400);
    });

    it('fails on missing name', () => {
      return request(app.getHttpServer()).post('/projects').send({ data: '{ "version": "6.0.1" }' }).expect(400);
    });

    it('fails on empty name', () => {
      return request(app.getHttpServer()).post('/projects').send({ name: '', data: '{ "version": "6.0.1" }' }).expect(400);
    });
  });

  describe('GET /projects/:id/history/:createdAt/data', () => {
    it('accepts on valid URL', async () => {
      const result = await request(app.getHttpServer())
        .get('/projects/618450d6e723d0242d8b69f8/history/618450d6e723d0242d8b69f8/data')
        .expect(200);
      expect(result.text).toEqual('somedata');
    });

    it('rejects on invalid date', async () => {
      await request(app.getHttpServer()).get('/projects/618450d6e723d0242d8b69f8/history/INVALID/data').expect(400);
    });
  });

  describe('PATCH /projects/:id', () => {
    const projectId = new Types.ObjectId();

    it('updates a project', async () => {
      await request(app.getHttpServer())
        .patch(`/projects/${projectId.toString()}`)
        .send({ name: 'new name', data: '{ "version": "6.0.1" }' })
        .expect(200);

      expect(updateProject.mock.calls[0][0]).toBe(userId);
      expect(projectId.equals(updateProject.mock.calls[0][1])).toBeTruthy();
      expect(updateProject.mock.calls[0][2]).toEqual({ name: 'new name', data: '{ "version": "6.0.1" }' });
    });

    it('works for empty update', async () => {
      await request(app.getHttpServer()).patch(`/projects/${projectId.toString()}`).send({}).expect(200);
    });

    it('fails on empty name', () => {
      return request(app.getHttpServer()).patch(`/projects/${projectId.toString()}`).send({ name: '' }).expect(400);
    });

    it('fails on invalid ID', () => {
      return request(app.getHttpServer()).patch('/projects/notamongoid').send({}).expect(400);
    });
  });
});
