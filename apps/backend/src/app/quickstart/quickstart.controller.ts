import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  ParseBoolPipe,
  ParseEnumPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { isEnum, isMongoId } from 'class-validator';
import { Throttle } from '@nestjs/throttler';
import { Request } from 'express';
import {
  GROUPED_QUICKSTART_OBJECTIVE_SORT_BY,
  GroupedQuickstartObjectiveSortBy,
  QUICKSTART_OBJECTIVE_SORT_BY,
  QuickstartObjectiveSortBy,
  SORT_DIRECTIONS,
  SortDirection,
} from '@entscheidungsnavi/api-types';
import { RolesGuard } from '../auth/roles.guard';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { UniqueConstraintViolationInterceptor } from '../common/unique-constraint-violation.interceptor';
import { PaginationParams } from '../common/pagination-params';
import { CreateQuickstartProjectDto } from './dto/create-quickstart-project.dto';
import { CreateQuickstartTagDto } from './dto/create-quickstart-tag.dto';
import { UpdateQuickstartProjectDto } from './dto/update-quickstart-project.dto';
import { UpdateQuickstartTagDto } from './dto/update-quickstart-tag.dto';
import { QuickstartProjectsService } from './quickstart-projects.service';
import { QuickstartTagsService } from './quickstart-tags.service';
import { QuickstartValuesService } from './quickstart-values.service';
import { CreateQuickstartValueDto, UpdateQuickstartValueDto } from './dto/quickstart-value.dto';
import { QuickstartValueTrackingDto } from './dto/quickstart-value-tracking.dto';
import { QuickstartHierarchiesService } from './quickstart-hierarchies.service';
import { CreateQuickstartHierarchyDto } from './dto/create-quickstart-hierarchy.dto';
import { QuickstartHierarchFilterDto } from './dto/quickstart-hierarchy-filter.dto';
import { UpdateQuickstartHierarchyDto } from './dto/update-quickstart-hierarchy.dto';
import { QuickstartHierarchySortDto } from './dto/quickstart-hierarchy.dto';
import { QuickstartObjectiveFilterDto } from './dto/quickstart-objective-filter.dto';
import { QuickstartObjectiveGroupByDto } from './dto/quickstart-objective.dto';
import { TrackQuickstartObjectivesDto } from './dto/track-quickstart-objectives.dto';

@Controller('quickstart')
export class QuickstartController {
  constructor(
    private quickstartProjectsService: QuickstartProjectsService,
    private quickstartTagsService: QuickstartTagsService,
    private quickstartValuesService: QuickstartValuesService,
    private quickstartHierarchiesService: QuickstartHierarchiesService
  ) {}

  @Get('projects')
  async listProjects(@Query('showInvisible', ParseBoolPipe) showInvisible: boolean) {
    return showInvisible ? await this.quickstartProjectsService.listAll() : await this.quickstartProjectsService.listVisible();
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @UseInterceptors(UniqueConstraintViolationInterceptor('A project with this link token already exists'))
  @Post('projects')
  async createProject(@Body() project: CreateQuickstartProjectDto) {
    return await this.quickstartProjectsService.create(project);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('projects/:handle')
  async findProject(@Param('handle') handle: string) {
    // We allow unauthorized users to find insivible quickstart projects on purpose. Visibility
    // is not a permission, just defines whether the project is shown in the list.
    if (isMongoId(handle)) {
      const id = new Types.ObjectId(handle);
      return await this.quickstartProjectsService.findById(id);
    } else {
      return await this.quickstartProjectsService.findByShareToken(handle);
    }
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @UseInterceptors(NotFoundInterceptor)
  @UseInterceptors(UniqueConstraintViolationInterceptor('A project with this link token already exists'))
  @Patch('projects/:id')
  async updateProject(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() project: UpdateQuickstartProjectDto) {
    return await this.quickstartProjectsService.update(id, project);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Delete('projects/:id')
  async deleteProject(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartProjectsService.remove(id);
  }

  @Get('tags')
  async listTags() {
    return await this.quickstartTagsService.list();
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Post('tags')
  async createTag(@Body() tag: CreateQuickstartTagDto) {
    return await this.quickstartTagsService.create(tag);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('tags/:id')
  async findTagById(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.quickstartTagsService.findById(id);
  }

  @UseInterceptors(NotFoundInterceptor)
  @UseGuards(RolesGuard('quickstart-manager'))
  @Patch('tags/:id')
  async updateTag(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() tag: UpdateQuickstartTagDto) {
    return await this.quickstartTagsService.update(id, tag);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Delete('tags/:id')
  async deleteTag(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartTagsService.remove(id);
  }

  @Get('values')
  async listValues(@Req() req: Request, @Query('includeStats', ParseBoolPipe) includeStats: boolean) {
    if (includeStats && (!req.isAuthenticated() || !req.user.roles.includes('quickstart-manager'))) {
      throw new ForbiddenException('quickstart-manager role required to read stats');
    }

    return await this.quickstartValuesService.list(includeStats);
  }

  @Throttle(1, 60) // Allow at most one request per minute
  @Post('values/metrics')
  async trackValueUsage(@Body() metrics: QuickstartValueTrackingDto) {
    await this.quickstartValuesService.collectMetrics(metrics);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('values/:id')
  async findValueById(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.quickstartValuesService.findById(id);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Post('values')
  async createValue(@Body() value: CreateQuickstartValueDto) {
    return await this.quickstartValuesService.create(value);
  }

  @UseInterceptors(NotFoundInterceptor)
  @UseGuards(RolesGuard('quickstart-manager'))
  @Patch('values/:id')
  async updateValue(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() value: UpdateQuickstartValueDto) {
    return await this.quickstartValuesService.update(id, value);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Delete('values/:id')
  async deleteValue(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartValuesService.remove(id);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Post('hierarchies')
  async createHierarchy(@Body() hierarchy: CreateQuickstartHierarchyDto) {
    return await this.quickstartHierarchiesService.create(hierarchy);
  }

  @Get('hierarchies')
  async listHierarchies(
    @Req() req: Request,
    @Query() filter: QuickstartHierarchFilterDto,
    @Query() { limit, offset }: PaginationParams,
    @Query() { sortBy, sortDirection }: QuickstartHierarchySortDto,
    @Query('includeStats', ParseBoolPipe) includeStats: boolean
  ) {
    if (includeStats && (!req.isAuthenticated() || !req.user.roles.includes('quickstart-manager'))) {
      throw new ForbiddenException('quickstart-manager role required to read stats');
    }

    return await this.quickstartHierarchiesService.listHierarchies(
      filter,
      { by: sortBy ?? 'totalAccumulatedScore', direction: sortDirection ?? 'desc' },
      { limit: limit ?? 100, offset: offset ?? 0 },
      includeStats
    );
  }

  @UseInterceptors(NotFoundInterceptor)
  @UseGuards(RolesGuard('quickstart-manager'))
  @Get('hierarchies/:id')
  async getHierarchy(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.quickstartHierarchiesService.getHierarchy(id);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Delete('hierarchies/:id')
  async deleteHierarchy(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartHierarchiesService.delete(id);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Patch('hierarchies/:id')
  async updateHierarchy(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() update: UpdateQuickstartHierarchyDto) {
    return await this.quickstartHierarchiesService.update(id, update);
  }

  @Get('objectives')
  async listObjectives(
    @Req() req: Request,
    @Query() filter: QuickstartObjectiveFilterDto,
    @Query() { limit, offset }: PaginationParams,
    @Query('sortBy') sortBy: string,
    @Query('sortDirection', new ParseEnumPipe(SORT_DIRECTIONS)) sortDirection: SortDirection,
    @Query('includeStats', ParseBoolPipe) includeStats: boolean,
    @Query() { groupBy }: QuickstartObjectiveGroupByDto
  ) {
    if (includeStats && (!req.isAuthenticated() || !req.user.roles.includes('quickstart-manager'))) {
      throw new ForbiddenException('quickstart-manager role required to read stats');
    }

    if (groupBy) {
      if (!isEnum(sortBy, GROUPED_QUICKSTART_OBJECTIVE_SORT_BY)) {
        throw new BadRequestException('invalid sort by value');
      }

      return await this.quickstartHierarchiesService.listGroupedObjectives(
        filter,
        { by: sortBy as GroupedQuickstartObjectiveSortBy, direction: sortDirection },
        { limit: limit ?? 100, offset: offset ?? 0 },
        includeStats,
        groupBy
      );
    } else {
      if (!isEnum(sortBy, QUICKSTART_OBJECTIVE_SORT_BY)) {
        throw new BadRequestException('invalid sort by value');
      }

      return await this.quickstartHierarchiesService.listObjectives(
        filter,
        { by: sortBy as QuickstartObjectiveSortBy, direction: sortDirection },
        { limit: limit ?? 100, offset: offset ?? 0 },
        includeStats
      );
    }
  }

  @Post('track-objectives')
  async trackHierarchyNode(@Body() body: TrackQuickstartObjectivesDto) {
    await this.quickstartHierarchiesService.incrementTrackingCounter(body.objectiveIds);
  }
}
