import { QuickstartHierarchyFilter } from '@entscheidungsnavi/api-types';
import { Transform } from 'class-transformer';
import { IsArray, IsMongoId, IsOptional, IsString } from 'class-validator';
import { escapeRegExp } from 'lodash';

export class QuickstartHierarchFilterDto implements QuickstartHierarchyFilter {
  @IsOptional()
  @IsString()
  @Transform(({ value }) => escapeRegExp(value))
  nameQuery?: string;

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  @Transform(({ value }) => (Array.isArray(value) ? value : [value]))
  tags?: string[];
}
