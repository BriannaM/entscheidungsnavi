import { Exclude, Expose, Type } from 'class-transformer';
import { QuickstartHierarchyListWithStats, QuickstartHierarchyWithStats } from '@entscheidungsnavi/api-types';
import {
  QuickstartHierarchyDto,
  QuickstartHierarchyListDto,
  QuickstartHierarchyTreeDto,
  QuickstartHierarchyTreeValueDto,
} from './quickstart-hierarchy.dto';

@Exclude()
export class QuickstartHierarchyTreeValueWithStatsDto extends QuickstartHierarchyTreeValueDto {
  @Expose()
  accumulatedScore: number;
}

@Exclude()
export class QuickstartHierarchyTreeWithStatsDto extends QuickstartHierarchyTreeDto {
  @Expose()
  @Type(() => QuickstartHierarchyTreeValueWithStatsDto)
  override value: QuickstartHierarchyTreeValueWithStatsDto;

  @Expose()
  @Type(() => QuickstartHierarchyTreeWithStatsDto)
  override children: QuickstartHierarchyTreeWithStatsDto[];
}

@Exclude()
export class QuickstartHierarchyWithStatsDto extends QuickstartHierarchyDto implements QuickstartHierarchyWithStats {
  @Expose()
  @Type(() => QuickstartHierarchyTreeWithStatsDto)
  override tree: QuickstartHierarchyTreeWithStatsDto;

  @Expose()
  totalAccumulatedScore: number;
}

@Exclude()
export class QuickstartHierarchyListWithStatsDto extends QuickstartHierarchyListDto implements QuickstartHierarchyListWithStats {
  @Expose()
  @Type(() => QuickstartHierarchyWithStatsDto)
  override items: QuickstartHierarchyWithStatsDto[];
}
