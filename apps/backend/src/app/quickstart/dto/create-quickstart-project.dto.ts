import { PickType } from '@nestjs/mapped-types';
import { QuickstartProjectDto } from './quickstart-project.dto';

export class CreateQuickstartProjectDto extends PickType(QuickstartProjectDto, ['name', 'data', 'visible', 'tags', 'shareToken']) {}
