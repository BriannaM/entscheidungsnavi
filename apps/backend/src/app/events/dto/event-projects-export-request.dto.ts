import { EventProjectsExportRequest } from '@entscheidungsnavi/api-types';
import { IsMongoId } from 'class-validator';

export class EventProjectsExportRequestDto implements EventProjectsExportRequest {
  @IsMongoId({ each: true })
  eventIds: string[];
}
