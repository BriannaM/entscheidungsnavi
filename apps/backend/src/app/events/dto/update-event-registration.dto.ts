import { PickType } from '@nestjs/mapped-types';
import { EventRegistrationDto } from './event-registration.dto';

export class UpdateEventRegistrationDto extends PickType(EventRegistrationDto, [
  'freeText',
  'questionnaireResponses',
  'projectData',
] as const) {}
