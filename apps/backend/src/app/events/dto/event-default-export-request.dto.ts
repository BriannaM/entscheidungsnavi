import { EventDefaultExportAttribute, EventDefaultExportRequest, EVENT_DEFAULT_EXPORT_ATTRIBUTES } from '@entscheidungsnavi/api-types';
import { IsEnum, IsMongoId, IsNumber, IsOptional } from 'class-validator';

export class EventDefaultExportRequestDto implements EventDefaultExportRequest {
  @IsMongoId({ each: true })
  eventIds: string[];

  @IsEnum(EVENT_DEFAULT_EXPORT_ATTRIBUTES, { each: true })
  attributes: EventDefaultExportAttribute[];

  @IsOptional()
  @IsNumber()
  alternativeLimit?: number;

  @IsOptional()
  @IsNumber()
  objectiveLimit?: number;

  @IsOptional()
  @IsNumber()
  influenceFactorLimit?: number;
}
