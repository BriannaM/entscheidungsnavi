import { IsString } from 'class-validator';

export class JoinEventDto {
  @IsString()
  code: string;
}
