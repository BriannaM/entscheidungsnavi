import { QuestionnaireEntry } from '@entscheidungsnavi/api-types';
import { validateQuestion } from './validate-question';

describe('validateQuestion()', () => {
  it('accepts only null responses for headings', () => {
    const entry: QuestionnaireEntry = { entryType: 'textBlock', text: '', type: 'body' };
    expect(validateQuestion(entry, null)).toBe(true);
    expect(validateQuestion(entry, 'some text')).toBe(false);
    expect(validateQuestion(entry, 20)).toBe(false);
  });

  it('handles number questions without bounds', () => {
    const entry: QuestionnaireEntry = { entryType: 'numberQuestion', label: '', question: '' };
    expect(validateQuestion(entry, 20)).toBe(true);
    expect(validateQuestion(entry, 0)).toBe(true);
    expect(validateQuestion(entry, null)).toBe(false);
    expect(validateQuestion(entry, 'some text')).toBe(false);
  });

  it('checks number bounds', () => {
    const baseEntry: QuestionnaireEntry = { entryType: 'numberQuestion', label: '', question: '' };
    expect(validateQuestion({ ...baseEntry, max: 10 }, 20)).toBe(false);
    expect(validateQuestion({ ...baseEntry, max: 10 }, 10)).toBe(true);
    expect(validateQuestion({ ...baseEntry, min: 10 }, 9)).toBe(false);
    expect(validateQuestion({ ...baseEntry, min: 10 }, 10)).toBe(true);
  });

  it('handles options questions', () => {
    const entry: QuestionnaireEntry = {
      entryType: 'optionsQuestion',
      label: '',
      question: '',
      options: ['1', '2', '3'],
      displayType: 'radio',
    };
    expect(validateQuestion(entry, 0)).toBe(true);
    expect(validateQuestion(entry, -1)).toBe(false);
    expect(validateQuestion(entry, 3)).toBe(false);
    expect(validateQuestion(entry, null)).toBe(false);
    expect(validateQuestion(entry, 'text')).toBe(false);
  });

  it('handles text questions without bounds', () => {
    const entry: QuestionnaireEntry = { entryType: 'textQuestion', label: '', question: '' };
    expect(validateQuestion(entry, 'text')).toBe(true);
    expect(validateQuestion(entry, '')).toBe(true);
    expect(validateQuestion(entry, null)).toBe(false);
    expect(validateQuestion(entry, 0)).toBe(false);
  });

  it('checks text question bounds', () => {
    const baseEntry: QuestionnaireEntry = { entryType: 'textQuestion', label: '', question: '' };

    expect(validateQuestion({ ...baseEntry, minLength: 5 }, 'text')).toBe(false);
    expect(validateQuestion({ ...baseEntry, minLength: 5 }, 'text1')).toBe(true);

    expect(validateQuestion({ ...baseEntry, maxLength: 5 }, 'text1')).toBe(true);
    expect(validateQuestion({ ...baseEntry, maxLength: 5 }, 'text12')).toBe(false);

    expect(validateQuestion({ ...baseEntry, pattern: '^\\d+$' }, 'text12')).toBe(false);
    expect(validateQuestion({ ...baseEntry, pattern: '^\\d+$' }, '12')).toBe(true);
  });

  it('handles table questions', () => {
    const entry: QuestionnaireEntry = { entryType: 'tableQuestion', baseQuestion: '', subQuestions: ['', ''], options: ['1', '2', '3'] };

    expect(validateQuestion(entry, null)).toBe(false);
    expect(validateQuestion(entry, 1)).toBe(false);
    expect(validateQuestion(entry, 'text')).toBe(false);

    expect(validateQuestion(entry, [])).toBe(false);
    expect(validateQuestion(entry, [1])).toBe(false);
    expect(validateQuestion(entry, [1, 3])).toBe(false);
    expect(validateQuestion(entry, ['text', 0] as any)).toBe(false);

    expect(validateQuestion(entry, [1, 2])).toBe(true);
  });
});
