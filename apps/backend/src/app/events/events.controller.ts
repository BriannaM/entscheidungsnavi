import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  Param,
  Patch,
  Post,
  Req,
  Res,
  StreamableFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { Response } from 'express';
import { RolesGuard } from '../auth/roles.guard';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { UniqueConstraintViolationInterceptor } from '../common/unique-constraint-violation.interceptor';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { CreateEventDto } from './dto/create-event.dto';
import { EventDefaultExportRequestDto } from './dto/event-default-export-request.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventsService } from './events.service';
import { EventProjectsExportRequestDto } from './dto/event-projects-export-request.dto';
import { EventAltAndObjDtoExportRequest } from './dto/event-alt-and-obj-export-request.dto';

@Controller('events')
@UseGuards(LoginGuard)
export class EventsController {
  constructor(private eventsService: EventsService) {}

  @Get()
  async list(@Req() request: AuthenticatedRequest) {
    return await this.eventsService.listEvents(request.user._id);
  }

  @UseInterceptors(UniqueConstraintViolationInterceptor('the registration code is not unique'))
  @Post()
  @UseGuards(RolesGuard('event-manager'))
  async create(@Req() request: AuthenticatedRequest, @Body() createEventDto: CreateEventDto) {
    return await this.eventsService.create(request.user._id, createEventDto);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get(':id')
  async getOne(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.eventsService.getOne(request.user._id, id);
  }

  @UseInterceptors(UniqueConstraintViolationInterceptor('the registration code is not unique'))
  @Patch(':id')
  async update(
    @Req() request: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) id: Types.ObjectId,
    @Body() updateEventDto: UpdateEventDto
  ) {
    return this.eventsService.update(request.user._id, id, updateEventDto);
  }

  @Delete(':id')
  async del(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.eventsService.deleteOne(request.user._id, id);
  }

  @Post('default-export')
  @HttpCode(200)
  async generateDefaultExport(@Req() request: AuthenticatedRequest, @Body() body: EventDefaultExportRequestDto, @Res() res: Response) {
    const wb = await this.eventsService.generateDefaultExport(request.user._id, body);
    const buffer = await wb.xlsx.writeBuffer();

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', 'attachment;filename=data.xlsx');

    res.write(buffer);
    res.end();
  }

  @Post('alternatives-and-objectives-export')
  @HttpCode(200)
  async generateAltAndObjExport(@Req() request: AuthenticatedRequest, @Body() body: EventAltAndObjDtoExportRequest, @Res() res: Response) {
    const wb = await this.eventsService.generateAltAndObjExport(request.user._id, body);
    const buffer = await wb.xlsx.writeBuffer();

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', 'attachment;filename=data.xlsx');

    res.write(buffer);
    res.end();
  }

  @Post('projects-export')
  @Header('Content-Type', 'application/zip')
  @Header('Content-Disposition', 'attachment;filename=data.zip')
  @HttpCode(200)
  async generateProjectsExport(@Req() request: AuthenticatedRequest, @Body() body: EventProjectsExportRequestDto) {
    const zip = await this.eventsService.generateProjectsExport(request.user._id, body);
    return new StreamableFile(zip);
  }

  @Get('submissions/:id/project')
  @Header('Content-Type', 'application/json')
  async getSubmittedProject(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.eventsService.getSubmittedProject(request.user._id, id);
  }
}
