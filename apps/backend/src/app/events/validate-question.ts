import { QuestionnaireEntry, QuestionnaireEntryResponse } from '@entscheidungsnavi/api-types';
import { NotImplementedException } from '@nestjs/common';

export function validateQuestion(question: QuestionnaireEntry, response: QuestionnaireEntryResponse) {
  switch (question.entryType) {
    case 'textBlock':
      return response == null;
    case 'numberQuestion':
      return (
        typeof response === 'number' &&
        (question.min == null || question.min <= response) &&
        (question.max == null || question.max >= response)
      );
    case 'optionsQuestion':
      return typeof response === 'number' && response >= 0 && response < question.options.length;
    case 'textQuestion':
      return (
        typeof response === 'string' &&
        (question.minLength == null || question.minLength <= response.length) &&
        (question.maxLength == null || question.maxLength >= response.length) &&
        (question.pattern == null || new RegExp(question.pattern).test(response))
      );
    case 'tableQuestion':
      return (
        Array.isArray(response) &&
        response.length === question.subQuestions.length &&
        response.every(r => typeof r === 'number' && r >= 0 && r < question.options.length)
      );
    default:
      throw new NotImplementedException();
  }
}
