import { Test } from '@nestjs/testing';
import { UsersService } from '../users/users.service';
import { LocalStrategy } from './local.strategy';

describe('LocalStrategy', () => {
  let localStrategy: LocalStrategy;
  let authenticate: jest.Mock;

  beforeEach(async () => {
    authenticate = jest.fn();

    const module = await Test.createTestingModule({
      providers: [
        {
          provide: UsersService,
          useValue: { authenticate },
        },
        LocalStrategy,
      ],
    }).compile();

    localStrategy = module.get<LocalStrategy>(LocalStrategy);
  });

  it('fails on incorrect password', async () => {
    authenticate.mockReturnValue(Promise.resolve({ error: 'some error' }));
    await expect(localStrategy.validate('username', 'password')).rejects.toThrow();
  });

  it('succeeds on correct password', async () => {
    const user = { _id: 'some bson id' };
    authenticate.mockReturnValueOnce(Promise.resolve({ user }));
    await expect(localStrategy.validate('user', '12345678')).resolves.toEqual(user);
    expect(authenticate.mock.calls[0]).toEqual(['user', '12345678']);
  });
});
