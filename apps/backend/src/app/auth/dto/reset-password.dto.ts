import { PickType } from '@nestjs/mapped-types';
import { CreateUserDto } from '../../users-api/dto/create-user.dto';

export class ResetPasswordDto extends PickType(CreateUserDto, ['password'] as const) {}
