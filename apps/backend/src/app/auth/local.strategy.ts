import { HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';

import { UsersService } from '../users/users.service';

/**
 * This is basically a wrapper aroung `passport-local-mongoose`.
 *
 * Enables authentication using username and password. The req.user field is set
 * to the UserDocument if authentication is successful.
 */
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private usersService: UsersService) {
    super({ usernameField: 'email' });
  }

  async validate(username: string, password: string) {
    const res = await this.usersService.authenticate(username, password);

    if (res.error == null) {
      return res.user;
    } else if (res.error instanceof Error && res.error.name === 'AttemptTooSoonError') {
      throw new HttpException('Account is locked temporarily', 429);
    } else {
      throw new UnauthorizedException();
    }
  }
}
