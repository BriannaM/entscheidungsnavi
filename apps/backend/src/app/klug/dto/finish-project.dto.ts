import { KlugFinishProject } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsEmail, IsOptional, ValidateNested } from 'class-validator';
import { KlugPdfExportImagesDto } from './klug-pdf-export-images.dto';

export class KlugFinishProjectDto implements KlugFinishProject {
  @IsOptional()
  @IsEmail()
  email?: string;

  @IsOptional()
  @Type(() => KlugPdfExportImagesDto)
  @ValidateNested()
  pdfExportImages?: KlugPdfExportImagesDto;
}
