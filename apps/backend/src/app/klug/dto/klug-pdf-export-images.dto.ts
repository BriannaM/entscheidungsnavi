import { KlugPdfExportImages } from '@entscheidungsnavi/api-types';
import { IsDataURI } from 'class-validator';

export class KlugPdfExportImagesDto implements KlugPdfExportImages {
  @IsDataURI()
  decisionStatementImage: string;

  @IsDataURI()
  objectiveListImage: string;

  @IsDataURI()
  alternativeListImage: string;

  @IsDataURI()
  assessmentGraphImage: string;

  @IsDataURI()
  comparisonGraphImage: string;

  @IsDataURI()
  evaluationGraphImage: string;
}
