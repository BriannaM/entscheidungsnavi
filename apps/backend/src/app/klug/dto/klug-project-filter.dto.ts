import { KlugProjectFilter } from '@entscheidungsnavi/api-types';
import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsDate, IsOptional } from 'class-validator';

export class KlugProjectFilterDto implements KlugProjectFilter {
  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value === 'true')
  isOfficial?: boolean;

  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value === 'true')
  finished?: boolean;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  startDate?: Date;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endDate?: Date;
}
