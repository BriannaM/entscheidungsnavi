import { IsNotEmpty, IsMongoId, IsString } from 'class-validator';

export class CreateTeamDto {
  @IsNotEmpty()
  @IsMongoId()
  projectId: string;

  @IsNotEmpty()
  @IsString()
  teamName: string;
}
