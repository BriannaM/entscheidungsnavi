import { PickType } from '@nestjs/mapped-types';
import { UserDto } from './user.dto';

export class UpdateUserRolesDto extends PickType(UserDto, ['roles']) {}
