import { SortDirection, SORT_DIRECTIONS, UserSort, UserSortBy, USER_SORT_BY } from '@entscheidungsnavi/api-types';
import { IsEnum, IsOptional } from 'class-validator';

export class UserSortDto implements UserSort {
  @IsOptional()
  @IsEnum(USER_SORT_BY)
  sortBy?: UserSortBy;

  @IsOptional()
  @IsEnum(SORT_DIRECTIONS)
  sortDirection?: SortDirection;
}
