import { Language } from '@entscheidungsnavi/api-types';
import { Body, Controller, Delete, Get, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { Lang } from '../common/language.decorator';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { UpdateUserDto } from './dto/update-user.dto';
import { DeleteUserDto } from './dto/delete-user.dto';
import { UsersApiService } from './users-api.service';
import { ConfirmEmailDto } from './dto/confirm-email.dto';

@UseGuards(LoginGuard)
@Controller('user')
export class UserController {
  constructor(private usersApiService: UsersApiService) {}

  @Get()
  async get(@Req() req: AuthenticatedRequest) {
    return await this.usersApiService.getOne(req.user._id);
  }

  @Patch()
  async update(@Req() req: AuthenticatedRequest, @Body() update: UpdateUserDto, @Lang() lang: Language) {
    await this.usersApiService.updateSelf(req.user._id, update, lang);
  }

  @Delete()
  async del(@Req() req: AuthenticatedRequest, @Body() { password }: DeleteUserDto) {
    await this.usersApiService.deleteOne(req.user._id, password);
  }

  @Post('request-email-confirmation')
  async requestConfirmationEmail(@Req() req: AuthenticatedRequest, @Lang() lang: Language) {
    await this.usersApiService.requestEmailConfirmation(req.user._id, lang);
  }

  @Post('confirm-email')
  async confirmMail(@Req() req: AuthenticatedRequest, @Body() body: ConfirmEmailDto) {
    await this.usersApiService.confirmEmail(req.user._id, body.token);
  }
}
