import { Component } from '@angular/core';

@Component({
  templateUrl: './quickstart-hierarchies-overview.component.html',
  styleUrls: ['./quickstart-hierarchies-overview.component.scss'],
})
export class QuickstartHierarchiesOverviewComponent {}
