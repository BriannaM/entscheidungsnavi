import { Component, Input } from '@angular/core';
import { QuickstartHierarchyNodeWithStats } from '@entscheidungsnavi/api-types';
import { Tree, emptyRichText } from '@entscheidungsnavi/tools';
import { HierarchyAction, TextWidthService } from '@entscheidungsnavi/widgets';

const NODE_HORIZONTAL_PADDING_PX = 8;

@Component({
  selector: 'dt-quickstart-hierarchy',
  templateUrl: './quickstart-hierarchy.component.html',
  styleUrls: ['./quickstart-hierarchy.component.scss'],
})
export class QuickstartHierarchyComponent {
  @Input() tree: Tree<QuickstartHierarchyNodeWithStats>;

  private inputFont: string;
  private minNodeTextWidth: number;

  constructor(private textWidthService: TextWidthService) {}

  private getEmptyTree() {
    return new Tree<QuickstartHierarchyNodeWithStats>({
      id: null,
      name: { de: '', en: '' },
      comment: { de: emptyRichText(), en: emptyRichText() },
      accumulatedScore: 0,
    });
  }

  calcInputWidth(value: QuickstartHierarchyNodeWithStats, elementDe: HTMLInputElement, elementEn: HTMLInputElement) {
    if (!this.inputFont) {
      this.inputFont = this.textWidthService.getComputedFont(window.getComputedStyle(elementDe));
      this.minNodeTextWidth = Math.max(
        this.textWidthService.getTextWidth(elementDe.placeholder, this.inputFont),
        this.textWidthService.getTextWidth(elementEn.placeholder, this.inputFont)
      );
    }

    const widthDe = this.textWidthService.getTextWidth(value.name.de, this.inputFont),
      widthEn = this.textWidthService.getTextWidth(value.name.en, this.inputFont);

    elementDe.style.width = elementEn.style.width = `${Math.max(widthDe, widthEn, this.minNodeTextWidth) + NODE_HORIZONTAL_PADDING_PX}px`;
  }

  executeAction(action: HierarchyAction) {
    switch (action.type) {
      case 'delete':
        this.tree.removeNode(action.location);
        break;
      case 'insert':
        this.tree.insertNode(action.location, this.getEmptyTree());
        break;
      case 'move': {
        const node = this.tree.removeNode(action.from);
        this.tree.insertNode(action.to, node);
        break;
      }
      case 'compound':
        action.actions.forEach(action => this.executeAction(action));
        break;
    }
  }
}
