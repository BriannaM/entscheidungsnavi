import { Component, Inject } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuickstartService } from '@entscheidungsnavi/api-client';
import { QuickstartHierarchyNodeWithStats, QuickstartHierarchyWithStats, QuickstartTag } from '@entscheidungsnavi/api-types';
import { emptyRichText, ITree, Tree } from '@entscheidungsnavi/tools';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { cloneDeep } from 'lodash';
import { filter, finalize, switchMap, tap } from 'rxjs';

const defaultQuickstartTree = new Tree<QuickstartHierarchyNodeWithStats>({
  id: null,
  name: { de: 'Ziele', en: 'Objectives' },
  comment: { de: emptyRichText(), en: emptyRichText() },
  accumulatedScore: 0,
});

@Component({
  templateUrl: './quickstart-hierarchy-details-modal.component.html',
  styleUrls: ['./quickstart-hierarchy-details-modal.component.scss'],
})
export class QuickstartHierarchyDetailsModalComponent {
  formGroup = this.fb.group({
    nameDe: [this.data.hierarchy?.name.de ?? '', Validators.required],
    nameEn: [this.data.hierarchy?.name.en ?? '', Validators.required],
    tags: [this.data.hierarchy?.tags.slice() ?? []],
  });

  tree = this.data.hierarchy ? Tree.from(cloneDeep(this.data.hierarchy.tree)) : cloneDeep(defaultQuickstartTree);

  constructor(
    private dialogRef: MatDialogRef<QuickstartHierarchyDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) protected data: { hierarchy?: QuickstartHierarchyWithStats; tags: QuickstartTag[] },
    private fb: NonNullableFormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private quickstartService: QuickstartService
  ) {}

  close(hasChanges = false) {
    this.dialogRef.close(hasChanges);
  }

  private isTreeValid() {
    const check = (tree: ITree<QuickstartHierarchyNodeWithStats>): boolean => {
      if (tree.value.name.de.length === 0 || tree.value.name.en.length === 0) {
        return false;
      }

      if (tree.children.some(child => !check(child))) {
        return false;
      }

      return true;
    };

    return check(this.tree);
  }

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    if (!this.isTreeValid()) {
      this.snackBar.open($localize`Der Zielbaum darf keine leeren Einträge enthalten`, 'Ok');
      return;
    }

    this.formGroup.disable({ emitEvent: false });
    const tree = this.tree;
    const name = { en: this.formGroup.value.nameEn, de: this.formGroup.value.nameDe };
    const tags = this.formGroup.value.tags;

    (this.data.hierarchy != null
      ? this.quickstartService.updateHierarchy(this.data.hierarchy.id, { name, tags, tree })
      : this.quickstartService.addHierarchy({ name, tags, tree })
    ).subscribe({
      next: value => this.dialogRef.close(value),
      error: () => {
        this.formGroup.enable({ emitEvent: false });
        this.snackBar.open($localize`Fehler beim Speichern`, 'Ok');
      },
    });
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Zielbaum löschen`,
          prompt: $localize`Bist Du sicher, dass Du den Zielbaum
          "${this.formGroup.value.nameDe}"/"${this.formGroup.value.nameEn}"
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Zielbaum löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
        }),
        switchMap(() => this.quickstartService.deleteHierarchy(this.data.hierarchy.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
        })
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Zielbaum gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }
}
