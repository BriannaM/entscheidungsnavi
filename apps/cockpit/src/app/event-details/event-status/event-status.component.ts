import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, NonNullableFormBuilder } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService, EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { cloneDeep, without } from 'lodash';
import { isEmail } from 'class-validator';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { EventDeleteModalComponent } from './delete-modal/event-delete-modal.component';
import { EventTransferOwnerModalComponent } from './transfer-owner-modal/event-transfer-owner-modal.component';

@Component({
  selector: 'dt-event-status',
  templateUrl: './event-status.component.html',
  styleUrls: ['./event-status.component.scss'],
})
export class EventStatusComponent implements OnInit, OnChanges {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  @Input() event: EventDto;
  @Output() isDeleting = new EventEmitter<boolean>();

  editorsForm = this.fb.group({
    editors: this.fb.control([] as string[], (control: FormControl<string[]>) =>
      control.value.every(entry => isEmail(entry)) ? null : { emailFormat: true }
    ),
  });

  userEmail: string;
  isOwner: boolean;

  constructor(
    private authService: AuthService,
    private eventManagementService: EventManagementService,
    private dialog: MatDialog,
    private fb: NonNullableFormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.user$.pipe(takeUntil(this.onDestroy$)).subscribe(user => {
      this.userEmail = user?.email;
      this.isOwner = this.userEmail === this.event.owner.email;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('event' in changes) {
      this.resetEditors();
    }
  }

  resetEditors() {
    this.editorsForm.setValue({ editors: this.event.editors.map(editor => editor.email) });
  }

  addEditor(event: MatChipInputEvent) {
    const value = (event.value ?? '').trim();

    if (value && !this.editorsForm.value.editors.includes(value)) {
      this.editorsForm.controls.editors.setValue([...this.editorsForm.value.editors, value]);
      this.editorsForm.controls.editors.markAsDirty();
    }

    event.chipInput.clear();
  }

  removeEditor(editor: string) {
    this.editorsForm.controls.editors.setValue(without(this.editorsForm.value.editors, editor));
    this.editorsForm.controls.editors.markAsDirty();
  }

  saveEditors() {
    this.editorsForm.disable({ emitEvent: false });
    this.eventManagementService
      .updateEvent(this.event.id, { editors: this.editorsForm.value.editors.map(email => ({ email })) })
      .subscribe({
        next: eventUpdate => {
          Object.assign(this.event, eventUpdate);
          this.editorsForm.enable();
          this.editorsForm.markAsPristine();
        },
        error: error => {
          this.editorsForm.enable();
          if (error instanceof HttpErrorResponse && error.status === 404) {
            this.editorsForm.controls.editors.setErrors({ emailNotFound: true });
          } else {
            this.editorsForm.controls.editors.setErrors({ unknownError: true });
          }
        },
      });
  }

  deleteEvent() {
    this.isDeleting.emit(true);
    this.dialog
      .open(EventDeleteModalComponent, { data: this.event })
      .afterClosed()
      .subscribe(() => this.isDeleting.emit(false));
  }

  transferOwnership() {
    this.dialog
      .open(EventTransferOwnerModalComponent, { data: this.event })
      .afterClosed()
      .subscribe(value => {
        if (value) this.resetEditors();
      });
  }

  duplicateEvent() {
    this.router.navigate(['/events/create'], { state: { event: cloneDeep(this.event) } });
  }
}
