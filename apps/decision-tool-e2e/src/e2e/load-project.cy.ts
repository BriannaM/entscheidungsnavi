describe('load-project', function () {
  it('should load alex project', function () {
    cy.visit('/');
    cy.loadAlex();
    cy.get('[data-cy=project_name]').contains('Alex');
  });
});
