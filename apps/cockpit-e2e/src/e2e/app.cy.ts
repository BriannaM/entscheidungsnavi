import { getLoginForm } from '../support/app.po';

describe('cockpit', () => {
  beforeEach(() => cy.visit('/'));

  it('should display the login form', () => {
    getLoginForm().should('be.visible');
  });

  // it('should allow login', () => {
  //   cy.login('admin@entscheidungsnavi.de', '12345678');
  // });
});
