export const getLoginForm = () => cy.get<HTMLFormElement>('form[data-cy=login_form]');
