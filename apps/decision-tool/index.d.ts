declare global {
  interface Window {
    dt: {
      lastError: unknown;
    };
    enableDTDebug?: () => void;
    dtEnv?: { [key: string]: string };
  }
}

export {};
