import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '@entscheidungsnavi/api-client';

@Injectable({
  providedIn: 'root',
})
export class NaviHelperService {
  constructor(private authService: AuthService, private snackBar: MatSnackBar) {}

  logout() {
    this.authService.logout().subscribe(() => this.snackBar.open($localize`Du wurdest erfolgreich abgemeldet!`, 'Ok', { duration: 5000 }));
  }
}
