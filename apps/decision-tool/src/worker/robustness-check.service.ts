import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { cloneDeep, constant, times } from 'lodash';
import { Subject } from 'rxjs';
import { InfluenceFactorStateMap } from '@entscheidungsnavi/decision-data/tools';
import { DecisionDataExportService } from '../app/data/decision-data-export.service';
import { ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT, StartMessage, UpdateMessage } from './robustness-check-messages';

export interface AlternativeResult {
  positionDistribution: number[];
  minUtility: number;
  maxUtility: number;
  avgUtility: number;
  utilityHistogram: number[];
}

export interface RobustnessWorkerResult {
  // Total number of iterations and speed summed across all workers
  iterationCount: number;
  // Results for every alternative
  alternatives: AlternativeResult[];
  // The frequencies of every influence factor state for every position [alternativeIdx][position]
  influenceFactors: InfluenceFactorStateMap<number[]>[][];
}

export type RobustnessWorkerSettings = Omit<StartMessage, 'decisionData'>;

@Injectable()
export class RobustnessCheckService implements OnDestroy {
  private workerPool: Worker[] = [];

  private results: {
    iterationCount: number;
    // How often each alternative (first index) is in each position (second index)
    positionCounts: number[][];
    // For every alternative/position combination: which influence factor states occurred
    stateCounts: InfluenceFactorStateMap<number[]>[][];
    // Statistics for each alternative
    utilitySum: number[];
    minUtility: number[];
    maxUtility: number[];
    // A histogram over the utilities for each alternative ([#alternativeIndex][#binIndex])
    utilityHistograms: number[][];
  };
  private settings: RobustnessWorkerSettings;

  private _noWorker = false;
  /**
   * True iff the browser does not support web workers.
   */
  get noWorker() {
    return this._noWorker;
  }

  get workerCount() {
    // Limit us half the systems logical cores
    return Math.max(Math.round(navigator.hardwareConcurrency / 2), 1);
  }

  /**
   * Emits the latest results from the robustness workers. Emits outside the Angular Zone.
   */
  readonly onUpdate$ = new Subject<void>();

  constructor(private zone: NgZone, private exportService: DecisionDataExportService) {
    if (!Worker) this._noWorker = true;
  }

  ngOnDestroy() {
    this.destroyWorkers();
  }

  private destroyWorkers() {
    this.workerPool.forEach(worker => {
      worker.removeAllListeners('message');
      worker.removeAllListeners('error');
      worker.terminate();
    });
    this.workerPool = [];
  }

  /**
   * Start a new worker calculation using the given data.
   *
   * @param settings - The settings for the calculation
   * @returns true if the calculation could be started, false otherwise
   */
  startWorker(settings: RobustnessWorkerSettings) {
    this.destroyWorkers();

    const alternativeCount = settings.selectedAlternatives.length;
    this.settings = settings;
    this.results = {
      iterationCount: 0,
      positionCounts: times(alternativeCount, () => times(alternativeCount, constant(0))),
      stateCounts: times(alternativeCount, () => times(alternativeCount, () => new InfluenceFactorStateMap<number[]>())),
      minUtility: times(alternativeCount, constant(Number.MAX_SAFE_INTEGER)),
      maxUtility: times(alternativeCount, constant(Number.MIN_SAFE_INTEGER)),
      utilitySum: times(alternativeCount, constant(0)),
      utilityHistograms: times(alternativeCount, () => times(ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT, constant(0))),
    };

    this.resumeWorker();
  }

  pauseWorker() {
    this.destroyWorkers();
  }

  resumeWorker() {
    for (let c = 0; c < this.workerCount; c++) {
      const newWorker = new Worker(new URL('./robustness-check.worker', import.meta.url), {
        type: 'module',
        name: this.constructor.name,
      });

      this.zone.runOutsideAngular(() => {
        newWorker.addEventListener('message', e => this.onWorkerMessage(e));
        newWorker.addEventListener('error', e => console.error(e));
      });

      newWorker.postMessage({
        ...this.settings,
        decisionData: this.exportService.dataToText(),
      } satisfies StartMessage);

      this.workerPool.push(newWorker);
    }
  }

  /**
   * This function runs outside the angular zone.
   */
  private onWorkerMessage(e: MessageEvent<UpdateMessage>) {
    const alternativeCount = this.settings.selectedAlternatives.length;

    // Aggregate the workers results into our current intermediate result
    for (let alternativeIndex = 0; alternativeIndex < alternativeCount; alternativeIndex++) {
      for (let position = 0; position < alternativeCount; position++) {
        this.results.positionCounts[alternativeIndex][position] += e.data.result.positions[alternativeIndex][position];

        // Update influence factor state distribution
        for (const [key, counts] of InfluenceFactorStateMap.fromInnerMap(e.data.result.stateCounts[alternativeIndex][position]).entries()) {
          let frequencies = this.results.stateCounts[alternativeIndex][position].get(key);

          if (frequencies == null) {
            frequencies = new Array(counts.length).fill(0);
            this.results.stateCounts[alternativeIndex][position].set(key, frequencies);
          }

          counts.forEach((count, stateIndex) => {
            frequencies[stateIndex] += count;
          });
        }
      }

      this.results.minUtility[alternativeIndex] = Math.min(this.results.minUtility[alternativeIndex], e.data.result.min[alternativeIndex]);
      this.results.maxUtility[alternativeIndex] = Math.max(this.results.maxUtility[alternativeIndex], e.data.result.max[alternativeIndex]);
      this.results.utilitySum[alternativeIndex] += e.data.result.sum[alternativeIndex];

      for (let binIndex = 0; binIndex < ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT; binIndex++) {
        this.results.utilityHistograms[alternativeIndex][binIndex] += e.data.result.histograms[alternativeIndex][binIndex];
      }
    }
    this.results.iterationCount += e.data.iterationCount;

    this.onUpdate$.next();
  }

  getCurrentResult(): RobustnessWorkerResult {
    const alternativeCount = this.settings.selectedAlternatives.length;

    // Aggregate worker results
    const result = cloneDeep(this.results);

    // Normalize state distribution by position counts, and normalize positions
    for (let alternativeIndex = 0; alternativeIndex < alternativeCount; alternativeIndex++) {
      for (let position = 0; position < alternativeCount; position++) {
        if (result.positionCounts[alternativeIndex][position] === 0) continue;

        for (const [, value] of result.stateCounts[alternativeIndex][position].entries()) {
          for (let stateIndex = 0; stateIndex < value.length; stateIndex++) {
            value[stateIndex] /= result.positionCounts[alternativeIndex][position];
          }
        }

        result.positionCounts[alternativeIndex][position] /= result.iterationCount;
      }
    }

    // Callback method
    return {
      alternatives: result.positionCounts.map((distribution, index) => ({
        positionDistribution: distribution,
        minUtility: result.minUtility[index],
        maxUtility: result.maxUtility[index],
        avgUtility: result.utilitySum[index] / result.iterationCount,
        utilityHistogram: result.utilityHistograms[index],
      })),
      influenceFactors: result.stateCounts,
      iterationCount: result.iterationCount,
    };
  }

  getCurrentIterationCount() {
    return this.results.iterationCount;
  }
}
