import { Component, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { getCurrentUrl } from '@entscheidungsnavi/widgets';

@Component({
  selector: 'dt-utility-function-navigation',
  template: `
    <dt-select-line align="start">
      <dt-select [selectedOptions]="selectedObjective" (selectedOptionsChange)="navigateTo($event)">
        <dt-select-label i18n>Nutzenfunktion</dt-select-label>
        <dt-select-option [value]="-1" i18n>Übersicht</dt-select-option>
        <dt-select-option *ngFor="let objective of decisionData.objectives; let index = index" [value]="index">
          {{ objective.name }}
        </dt-select-option>
      </dt-select>
    </dt-select-line>
  `,
  styles: [
    `
      dt-select-line {
        margin: 0 20px 16px 20px;
      }
    `,
  ],
})
export class UtilityFunctionNavigationComponent {
  private readonly detailUrlRegex = /detailed\/(\d+)$/;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  selectedObjective = -1;

  @Output() selectedObjectiveChange = new EventEmitter<number>(true);

  constructor(private router: Router, private route: ActivatedRoute, protected decisionData: DecisionData) {
    getCurrentUrl()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(url => {
        const match = this.detailUrlRegex.exec(url);
        this.selectedObjective = match != null ? +match[1] : -1;
        this.selectedObjectiveChange.emit(this.selectedObjective);
      });
  }

  navigateTo(position: number) {
    this.router.navigate(position === -1 ? ['.'] : ['detailed', position], { relativeTo: this.route });
  }
}
