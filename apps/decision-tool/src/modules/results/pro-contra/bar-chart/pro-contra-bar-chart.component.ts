import { Component, Input } from '@angular/core';
import { Alternative, Objective, Outcome } from '@entscheidungsnavi/decision-data/classes';

export type RankedAlternativeWithUtilityValues = {
  alternative: Alternative;
  originalIndex: number;
  rank: number;
  weightedUtilityValues: number[];
  outcomes: Outcome[];
};

@Component({
  selector: 'dt-pro-contra-bar-chart',
  styleUrls: ['./pro-contra-bar-chart.component.scss'],
  templateUrl: './pro-contra-bar-chart.component.html',
})
export class ProContraBarChartComponent {
  @Input() rankedAlternativesWithValues: RankedAlternativeWithUtilityValues[];
  @Input() objectives: Objective[];

  trackByAlternative(index: number, item: RankedAlternativeWithUtilityValues) {
    return item.alternative;
  }

  get maxValue() {
    return this.rankedAlternativesWithValues.reduce(
      (currentMax, rankedAlternativeWithValues) =>
        Math.max(
          currentMax,
          rankedAlternativeWithValues.weightedUtilityValues.reduce(
            (currentWeightedUtilityMax, weightedUtilityValue) => Math.max(currentWeightedUtilityMax, Math.abs(weightedUtilityValue)),
            0
          )
        ),
      0
    );
  }
}
