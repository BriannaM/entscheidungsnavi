import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { range, round, sortBy, unzipWith } from 'lodash';
import { applyWeightsToUtilityMatrix, getUtilityMatrixMeta } from '@entscheidungsnavi/decision-data/calculation';
import { PolarChartItem } from '@entscheidungsnavi/widgets/charts';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';
import { RankedAlternativeWithUtilityValues } from './bar-chart/pro-contra-bar-chart.component';

/*
    Note:
    - bar bar-chart is only available with at least two *alternatives*.
    - polar bar-chart is only available with at least three *objectives*.
 */

@Component({
  selector: 'dt-pro-and-contra',
  styleUrls: ['./pro-contra.component.scss'],
  templateUrl: './pro-contra.component.html',
})
@DisplayAtMaxWidth
export class ProContraComponent implements OnInit, Navigation, HelpMenuProvider {
  @ViewChild('helpContent') helpContent: TemplateRef<any>;

  @Input()
  showTitle = true;

  protected objectiveOrder: 'default' | 'weights' = 'default';
  private sortedObjectiveIndices: number[]; // order in which objectives should be shown
  protected objectiveNames: string[];
  protected sortedObjectives: Objective[];

  private alternativeRanking: number[]; // sorted alternative indices by utility value
  protected indicesOfCheckedAlternatives: number[];
  protected checkedAlternatives: PolarChartItem[];

  protected utilityMatrix: number[][];

  // for bar-chart
  private weightedUtilityMatrixForCheckedAlternatives: number[][];
  protected rankedAlternativesWithValues: RankedAlternativeWithUtilityValues[];

  protected display: 'bar' | 'polar' = 'polar';

  navLine = new NavLine({ left: [navLineElement().back('/results').build()] });

  helpMenu = getHelpMenu(this.languageService.steps, 'pro-contra');

  constructor(protected decisionData: DecisionData, private languageService: LanguageService) {}

  ngOnInit() {
    if (this.decisionData.objectives.length < 3) {
      this.display = 'bar';
    }

    this.objectiveNames = this.decisionData.objectives.map(objective => objective.name);

    this.utilityMatrix = getUtilityMatrixMeta(this.decisionData.outcomes, this.decisionData.objectives);

    this.alternativeRanking = this.getAlternativeRanking();

    this.indicesOfCheckedAlternatives = this.alternativeRanking.slice(0, 6).map(rank => rank - 1);

    this.handleObjectiveOrderUpdate();
    this.handleAlternativeToggledUpdate();
  }

  protected getAlternativeRanking() {
    const utilityValues = this.decisionData.getAlternativeUtilities().map(v => round(v, 2));

    const alternativeOrderingForRanking = range(this.decisionData.alternatives.length).sort(
      (alternativeIndexA, alternativeIndexB) => utilityValues[alternativeIndexB] - utilityValues[alternativeIndexA]
    );

    return range(this.decisionData.alternatives.length).map(
      alternativeIndex => alternativeOrderingForRanking.indexOf(alternativeIndex) + 1
    );
  }

  protected handleAlternativeToggledUpdate() {
    this.indicesOfCheckedAlternatives = sortBy(this.indicesOfCheckedAlternatives);
    this.checkedAlternatives = this.indicesOfCheckedAlternatives.map(alternativeIndex => ({
      id: alternativeIndex,
      label: this.decisionData.alternatives[alternativeIndex].name,
      values: this.utilityMatrix[alternativeIndex],
    }));

    this.weightedUtilityMatrixForCheckedAlternatives = this.getNormalizedUtilityMatrix();

    const utilityValues = this.decisionData.getAlternativeUtilities().map(v => round(v, 2));
    const weightedUtilityValues = this.weightedUtilityMatrixForCheckedAlternatives.map(weightedUtilityValuesForAlternative =>
      this.sortedObjectiveIndices.map(objectiveIndex => weightedUtilityValuesForAlternative[objectiveIndex])
    );

    this.rankedAlternativesWithValues = this.checkedAlternatives
      .map(({ id: originalIndex }, index) => {
        return {
          originalIndex,
          alternative: this.decisionData.alternatives[originalIndex],
          rank: this.alternativeRanking[originalIndex],
          weightedUtilityValues: weightedUtilityValues[index],
          outcomes: this.sortedObjectiveIndices.map(objectiveIndex => this.decisionData.outcomes[originalIndex][objectiveIndex]),
        };
      })
      .sort((a, b) => utilityValues[b.originalIndex] - utilityValues[a.originalIndex]);
  }

  protected handleObjectiveOrderUpdate() {
    this.sortedObjectiveIndices = range(this.decisionData.objectives.length);

    if (this.objectiveOrder === 'weights') {
      const objectiveWeightValues = this.decisionData.weights.getWeightValues();

      this.sortedObjectiveIndices.sort(
        (objectiveIndex1, objectiveIndex2) => objectiveWeightValues[objectiveIndex2] - objectiveWeightValues[objectiveIndex1]
      );
    }

    this.sortedObjectives = this.sortedObjectiveIndices.map(objectiveIndex => this.decisionData.objectives[objectiveIndex]);
  }

  /**
   * Returns a weighted utility matrix where the values for every objective average 0.
   */
  private getNormalizedUtilityMatrix() {
    // First get the weighted utilty matrix
    const utilityMatrix = getUtilityMatrixMeta(
      this.decisionData.outcomes.filter((_, alternativeIndex) => this.indicesOfCheckedAlternatives.includes(alternativeIndex)),
      this.decisionData.objectives
    );
    applyWeightsToUtilityMatrix(utilityMatrix, this.decisionData.weights.getWeightValues());

    const columnAverage = (matrix: number[][]): number[] => {
      const avgFunction = (...values: number[]) => values.reduce((a, b) => a + b) / values.length;
      return unzipWith(matrix, avgFunction);
    };

    // average utility value for each objective
    const objAvg: number[] = columnAverage(utilityMatrix);

    return utilityMatrix.map(row => row.map((v, objIdx) => (v - objAvg[objIdx]) * this.decisionData.objectives.length));
  }
}
