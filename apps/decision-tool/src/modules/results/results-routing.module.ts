import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EducationalModeGuard } from '../../app/guards';
import { ResultsOverviewComponent } from './main';
import {
  ObjectiveWeightingMainComponent,
  ObjectiveWeightingDetailedComponent,
  ObjectiveWeightingOverviewComponent,
  objectiveWeightingDetailedGuard,
} from './objective-weighting';
import { ProContraComponent } from './pro-contra';
import { RobustnessCheckComponent } from './robustness-check';
import { SensitivityAnalysisComponent } from './sensitivity-analysis';
import {
  UtilityFunctionMainComponent,
  UtilityFunctionDetailedComponent,
  UtilityFunctionOverviewComponent,
  utilityFunctionDetailedGuard,
} from './utility-functions';
import { ValidateObjectiveWeightingGuard } from './guards/validate-objective-weighting.guard';
import { ValidateUtilityFunctionsGuard } from './guards/validate-utility-functions.guard';
import { RiskComparisonComponent } from './risk-comparison/risk-comparison.component';

const routes: Routes = [
  {
    path: 'hints',
    redirectTo: 'steps',
  },
  {
    path: 'steps/1',
    canActivate: [EducationalModeGuard],
    data: {
      educationalModeRedirect: '/results/steps/2',
    },
    component: UtilityFunctionMainComponent,
    children: [
      {
        path: '',
        component: UtilityFunctionOverviewComponent,
      },
      {
        path: 'detailed/:objectiveId',
        canActivate: [utilityFunctionDetailedGuard],
        component: UtilityFunctionDetailedComponent,
      },
    ],
  },
  {
    path: 'steps/2',
    canActivate: [ValidateUtilityFunctionsGuard],
    component: ObjectiveWeightingMainComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ObjectiveWeightingOverviewComponent,
      },
      {
        path: 'detailed/:objectiveId',
        canActivate: [EducationalModeGuard, objectiveWeightingDetailedGuard],
        data: {
          educationalModeRedirect: '/results/steps/2',
        },
        component: ObjectiveWeightingDetailedComponent,
      },
    ],
  },
  {
    path: '',
    canActivate: [ValidateObjectiveWeightingGuard, ValidateUtilityFunctionsGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ResultsOverviewComponent,
      },
      {
        path: 'main',
        component: ResultsOverviewComponent,
      },
      { path: 'pro-contra', component: ProContraComponent },
      { path: 'sensitivity-analysis', component: SensitivityAnalysisComponent },
      {
        path: 'robustness-check',
        canActivate: [EducationalModeGuard],
        data: {
          educationalModeRedirect: '/results',
        },
        component: RobustnessCheckComponent,
      },
      {
        path: 'risk-comparison',
        canActivate: [EducationalModeGuard],
        data: {
          educationalModeRedirect: '/results',
        },
        component: RiskComparisonComponent,
      },
    ],
  },
  // ------------------------- REDIRECTS FOR LEGACY PATHS -------------------------
  {
    path: 'auswertung-uebersicht',
    redirectTo: 'main',
  },
  {
    path: 'auswertung-relativer-vergleich',
    redirectTo: 'pro-contra',
  },
  {
    path: 'auswertung-pro-contra',
    redirectTo: 'pro-contra',
  },
  {
    path: 'auswertung-sensitivitaetsanalyse',
    redirectTo: 'sensitivity-analysis',
  },
  {
    path: 'auswertung-robustheitstest',
    redirectTo: 'robustness-check',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultsRoutingModule {}
