import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, TrackByFunction } from '@angular/core';
import { range, sum } from 'lodash';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PersistentSetting } from '../../../app/data/persistent-setting';

@Component({
  selector: 'dt-chart-ranking',
  styleUrls: ['chart-ranking.component.scss'],
  templateUrl: 'chart-ranking.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartRankingComponent implements OnChanges {
  @Input()
  explainable = false;

  /*
    N = number of alternatives (decisionData.alternatives.length)
    M = number of objectives (decisionData.objectives.length)
    P = number of selected alternatives (selectedAlternatives.length)
  */

  // Array of size N with utility values of all alternatives in decisionData (not only the selected).
  @Input() utilityValues: number[];

  // Optional array of size N with utility values that should be considered as reference values for ranking changes.
  @Input() initialUtilityValues?: number[];

  // Array of size P that contains the utility values for all SELECTED ALTERNATIVES
  // that result from ignoring all objectives marked by ignoredObjectives.
  adjustedUtilityValues: number[];

  /*
    Optional matrix of size N x M that partitions the alternative's utility values into
    the contributing utility values from each objective in isolation.

    Its format is as follows:

    partitionedUtilityValues[selectedAlternativeIdx] = [
      utility value for alternative A from objective 0,
      ...,
      utility value for alternative A in objective M - 1
    ]

    A = selectedAlternatives[selectedAlternativeIdx]
  */
  @Input() weightedUtilityMatrix?: number[][];

  // Array of size M with names that are shown on hover (if displayMode = 'detailed').
  get objectiveNames() {
    return this.decisionData.objectives.map(objective => objective.name);
  }

  @Input() allowIgnoringObjectives = false;

  // Array of size N that determines if an objective should be ignored or not.
  // Ignored objectives are excluded in the calculation of adjustedUtilityValues and
  // are hidden in the partitioning of the utility value (if displayMode = 'detailed').
  ignoredObjectives: boolean[] = [];

  private ignoredObjectivesCount = 0;

  @Input() enableAlternativeSelection = false;

  // Array of size P of indices that are referencing alternatives in the decisionData.alternatives array.
  // Must not be smaller than minAlternatives.
  private _selectedAlternatives: number[] = range(this.decisionData.alternatives.length);

  get selectedAlternatives() {
    return this._selectedAlternatives;
  }
  @Input()
  set selectedAlternatives(newSelectedAlternatives: number[]) {
    this._selectedAlternatives = newSelectedAlternatives;
    if (this.utilityValues) this.updateData({ initialOrderOfAlternatives: true, orderOfAlternatives: true });
    this.selectedAlternativesChange.emit(newSelectedAlternatives);
  }

  @Output() selectedAlternativesChange = new EventEmitter<number[]>();

  // Minimum number of alternatives that have to stay selected.
  @Input() minAlternatives = 1;

  // Currently selected objective (if displayMode = 'detailed').
  hoverPartition = -1;

  // The winner index within selectedAlternative (NOT the global alternative index)
  winnerIdx: number;

  // Array of size P that indicates the rank change within the group of selected alternatives.
  // Difference is computed between initialOrderOfAlternatives and orderOfAlternatives.
  rankingChangeArray: number[] = [];

  // Defines the display order of the alternatives (descending).
  // Elements are indexing the selectedAlternatives array, thus orderOfAlternatives[i] is smaller than P.
  orderOfAlternatives: number[];

  // Snapshot of orderOfAlternatives updated on initialization and alternative selection change.
  // If initialValues or allowIgnoringColumns is set this contains the original order.
  initialOrderOfAlternatives: number[];

  @PersistentSetting('project') displayMode: 'simple' | 'detailed' = 'simple';
  @PersistentSetting('project') orderMode: 'utilityValues' | 'gutFeeling' = 'utilityValues';

  // Whether the Objective Names are still on the screen
  namesVisible = true;

  colors = [
    'hsl(208deg 35% 40%)',
    'hsl(0deg 50% 40%)',
    'hsl(28deg 75% 45%)',
    'hsl(448deg 35% 40%)',
    'hsl(268deg 35% 40%)',
    'hsl(174 50% 40%)',
  ];

  isMobile = false;

  private static sortWithRespectTo(valuesToSort: number[], ranking: number[]) {
    return valuesToSort.sort((i: number, j: number) => ranking[j] - ranking[i]);
  }

  // Track Functions
  trackByItem: TrackByFunction<any> = (_: number, item: any) => item;
  trackByIndex: TrackByFunction<any> = (index: number) => index;
  trackByRankingChange: TrackByFunction<any> = (index: number, _: any) => this.rankingChangeArray?.[index];

  get showRankingChange() {
    return !!this.initialUtilityValues || (this.canIgnoreObjectives && this.orderMode === 'utilityValues');
  }

  get selectedAlternativeIndicesOrdered() {
    if (this.orderMode === 'gutFeeling') {
      return this.orderOfAlternatives;
    } else {
      return this.initialOrderOfAlternatives ?? this.orderOfAlternatives;
    }
  }

  get shownValues() {
    return this.allowIgnoringObjectives && this.ignoredObjectivesCount > 0 ? this.adjustedUtilityValues : this.utilityValues;
  }

  get canIgnoreObjectives() {
    return this.allowIgnoringObjectives && this.displayMode === 'detailed';
  }

  get gridColumnTemplate() {
    const columns = [];

    // Ranking
    columns.push('min-content');

    // Ranking Change
    if (this.showRankingChange && !this.isMobile) {
      columns.push('min-content');
    }

    // Caption
    columns.push('fit-content(50%)');

    // Value
    columns.push('1fr');

    return columns.join(' ');
  }

  get gridRowTemplate() {
    return `repeat(${this.selectedAlternatives.length}, minmax(30px, 1fr))`;
  }

  constructor(public decisionData: DecisionData) {}

  setOrder(newOrderMode: 'utilityValues' | 'gutFeeling') {
    this.orderMode = newOrderMode;
    this.updateData({ initialOrderOfAlternatives: false, orderOfAlternatives: true });
  }

  setDisplay(newDisplayMode: 'simple' | 'detailed') {
    this.displayMode = newDisplayMode;

    if (this.allowIgnoringObjectives) {
      this.updateData();
    }
  }

  getRankingChange(selectedAlternativeIdx: number) {
    return this.showRankingChange ? this.rankingChangeArray?.[selectedAlternativeIdx] ?? 0 : 0;
  }

  toggleObjective(objectiveIdx: number) {
    if (!this.canIgnoreObjectives) return;

    this.ignoredObjectives[objectiveIdx] = !this.ignoredObjectives[objectiveIdx];
    this.ignoredObjectivesCount += this.ignoredObjectives[objectiveIdx] ? 1 : -1;

    this.updateData({ initialOrderOfAlternatives: false, orderOfAlternatives: true });
  }

  getUtilityValuePartitionContainerWidth(selectedAlternativeIdx: number) {
    return `calc(${this.shownValues[selectedAlternativeIdx] * 100}% + ${6 * this.ignoredObjectivesCount}px)`;
  }

  *nonZeroUtilityValuePartitions(selectedAlternativeIdx: number) {
    if (!this.weightedUtilityMatrix || !this.weightedUtilityMatrix[selectedAlternativeIdx]) return;

    for (let i = 0; i < this.weightedUtilityMatrix[selectedAlternativeIdx].length; i++) {
      const value = this.weightedUtilityMatrix[selectedAlternativeIdx][i];
      if (value > 0 || this.ignoredObjectives[i]) {
        yield [value, i, this.ignoredObjectives[i]] as const;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const toBeUpdatedWithForce = {
      initialOrderOfAlternatives: false,
      orderOfAlternatives: false,
    };

    toBeUpdatedWithForce.initialOrderOfAlternatives = !!changes['selectedAlternatives'];
    toBeUpdatedWithForce.orderOfAlternatives = !!changes['utilityValues'] || !!changes['partitionedValues'];

    this.updateData(toBeUpdatedWithForce);
  }

  updateData(withForce = { orderOfAlternatives: false, initialOrderOfAlternatives: false }) {
    this.updateAdjustedUtilityValues();
    this.updateInitialOrderOfAlternatives(withForce.initialOrderOfAlternatives);
    this.updateWinner();
    this.updateOrderOfAlternatives(withForce.orderOfAlternatives);
    this.updateRankingChangeArray();
  }

  private updateAdjustedUtilityValues() {
    this.adjustedUtilityValues = this.selectedAlternatives.map(alternativeIdx => {
      if (this.canIgnoreObjectives) {
        return Math.max(
          0,
          this.utilityValues[alternativeIdx] -
            sum(this.weightedUtilityMatrix[alternativeIdx].filter((_, objectiveIdx) => this.ignoredObjectives[objectiveIdx]))
        );
      } else {
        return this.utilityValues[alternativeIdx];
      }
    });
  }

  private updateInitialOrderOfAlternatives(force = false) {
    if (!this.initialOrderOfAlternatives || force) {
      this.initialOrderOfAlternatives = ChartRankingComponent.sortWithRespectTo(
        range(this.selectedAlternatives.length),
        this.selectedAlternatives.map(alternativeIdx => this.initialUtilityValues?.[alternativeIdx] ?? this.utilityValues[alternativeIdx])
      );
    }
  }

  private updateWinner() {
    let winnerValue = -1;

    this.winnerIdx = -1;
    for (let i = 0; i < this.adjustedUtilityValues.length; i++) {
      if (this.adjustedUtilityValues[i] > winnerValue) {
        winnerValue = this.adjustedUtilityValues[i];
        this.winnerIdx = i;
      }
    }
  }

  private updateOrderOfAlternatives(force = false) {
    if (!this.orderOfAlternatives || force) {
      this.orderOfAlternatives = ChartRankingComponent.sortWithRespectTo(
        range(this.selectedAlternatives.length),
        this.orderMode === 'utilityValues' ? this.adjustedUtilityValues : this.selectedAlternatives
      );

      if (this.orderMode === 'gutFeeling') this.orderOfAlternatives.reverse();
    }
  }

  private updateRankingChangeArray() {
    if (this.initialOrderOfAlternatives && (this.initialUtilityValues || this.allowIgnoringObjectives)) {
      this.rankingChangeArray = range(this.selectedAlternatives.length).map(
        selectedAlternativeIdx =>
          this.initialOrderOfAlternatives.indexOf(selectedAlternativeIdx) - this.orderOfAlternatives.indexOf(selectedAlternativeIdx)
      );
    }
  }
}
