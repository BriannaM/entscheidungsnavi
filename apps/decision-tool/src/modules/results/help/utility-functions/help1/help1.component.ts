import { Component } from '@angular/core';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { getCurrentUrl } from '@entscheidungsnavi/widgets';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  templateUrl: './help1.component.html',
  styleUrls: ['../../../../hints.scss'],
})
export class Help1Component {
  onDetailedPage: boolean;
  isVerbal: boolean;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(decisionData: DecisionData) {
    getCurrentUrl()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(url => {
        this.onDetailedPage = url.startsWith('/results/steps/1/detailed');
        if (this.onDetailedPage) {
          const currentObjective = +url.split('/').pop();
          this.isVerbal = decisionData.objectives[currentObjective].isVerbal;
        }
      });
  }
}
