import { Component } from '@angular/core';

@Component({
  selector: 'dt-help-main-starter',
  templateUrl: './help-main-starter.component.html',
  styleUrls: ['../../../hints.scss'],
})
export class HelpMainStarterComponent {}
