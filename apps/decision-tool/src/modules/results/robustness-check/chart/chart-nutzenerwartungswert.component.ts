import { Component, Input } from '@angular/core';

@Component({
  selector: 'dt-chart-nutzenerwartungswert',
  styleUrls: ['./chart-nutzenerwartungswert.component.scss'],
  templateUrl: './chart-nutzenerwartungswert.component.html',
})
export class ChartNutzenerwartungswertComponent {
  @Input() values: number[][];

  verticalLines = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
}
