import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { IfKey, InfluenceFactorStateMap, PredefinedIfKey } from '@entscheidungsnavi/decision-data/tools';
import { PREDEFINED_INFLUENCE_FACTORS } from '@entscheidungsnavi/decision-data/classes';
import { PREDEFINED_INFLUENCE_FACTOR_NAMES } from '@entscheidungsnavi/widgets/pipes/predefined-influence-factor-names';
import { sum } from 'lodash';

@Component({
  selector: 'dt-robustness-influence-factor-deviation',
  templateUrl: './robustness-influence-factor-deviation.component.html',
  styleUrls: ['./robustness-influence-factor-deviation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RobustnessInfluenceFactorDeviationComponent implements OnChanges {
  @Input() stateDistribution: InfluenceFactorStateMap<number[]>;

  relevantInfluenceFactors: Array<{
    key: IfKey;
    stateIndices: number[];
    frequency: number;
    expected: number;
  }>;

  constructor(private decisionData: DecisionData) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('stateDistribution' in changes) this.updateMostDeviatingInfluenceFactors();
  }

  isPredefinedKey(key: IfKey): key is PredefinedIfKey {
    return typeof key !== 'number';
  }

  getName(value: (typeof this.relevantInfluenceFactors)[number]) {
    if (this.isPredefinedKey(value.key)) {
      const objective = this.decisionData.objectives[value.key.objectiveIndex];
      return objective.name + (objective.isIndicator ? `/${objective.indicatorData.indicators[value.key.indicatorIndex].name}` : '');
    } else {
      return this.decisionData.influenceFactors[value.key].name;
    }
  }

  getPredefinedDescription(key: PredefinedIfKey) {
    return (
      '(' +
      PREDEFINED_INFLUENCE_FACTOR_NAMES[key.id].name +
      ' ' +
      $localize`in Alternative` +
      ' ' +
      this.decisionData.alternatives[key.alternativeIndex].name +
      ')'
    );
  }

  getStateName(value: (typeof this.relevantInfluenceFactors)[number]) {
    const key = value.key;
    return this.isPredefinedKey(key)
      ? value.stateIndices.map(stateIndex => PREDEFINED_INFLUENCE_FACTOR_NAMES[key.id].stateNames[stateIndex]).join(', ')
      : value.stateIndices.map(stateIndex => this.decisionData.influenceFactors[key].states[stateIndex].name).join(', ');
  }

  updateMostDeviatingInfluenceFactors() {
    this.relevantInfluenceFactors = [];

    for (const [key, stateFrequencies] of this.stateDistribution.entries()) {
      const relevantStateIndices = stateFrequencies
        .map((frequency, stateIndex) => (frequency > 0 ? stateIndex : -1))
        .filter(index => index !== -1);

      if (relevantStateIndices.length === stateFrequencies.length) {
        // All states have frequency > 0, so this influence factor is not relevant
        continue;
      }

      const ifStates = this.isPredefinedKey(key)
        ? PREDEFINED_INFLUENCE_FACTORS[key.id].states
        : this.decisionData.influenceFactors[key].states;

      const frequency = sum(relevantStateIndices.map(index => stateFrequencies[index])),
        expected = sum(relevantStateIndices.map(index => ifStates[index].probability / 100));

      this.relevantInfluenceFactors.push({
        key,
        stateIndices: relevantStateIndices,
        frequency,
        expected,
      });
    }

    // Sort by expected probability (asc)
    this.relevantInfluenceFactors.sort((a, b) => a.expected - b.expected);
  }
}
