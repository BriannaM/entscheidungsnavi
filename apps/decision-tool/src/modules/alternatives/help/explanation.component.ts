import { Component } from '@angular/core';

@Component({
  templateUrl: 'explanation.component.html',
  styleUrls: ['./explanation.component.scss', '../../hints.scss'],
})
export class AlternativenExplanationComponent {
  download() {
    window.open('https://pubsonline.informs.org/doi/10.1287/opre.2015.1411', '_blank');
  }
}
