import { Component, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Alternative } from '@entscheidungsnavi/decision-data/classes';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { AlternativeListComponent } from '../../alternative-list/alternative-list.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  selector: 'dt-alternative-hint-2',
  templateUrl: './hint2.component.html',
  styleUrls: ['./hint2.component.scss'],
})
export class AlternativeHint2Component extends AbstractAlternativeHintComponent implements OnInit {
  @Input()
  showTitle = true;
  @ViewChild('notify', { static: true }) private notifyRef: TemplateRef<any>;
  @ViewChild(AlternativeListComponent) alternativeList: AlternativeListComponent;

  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 3`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(injector: Injector, private snackBar: MatSnackBar) {
    super(injector);
    this.pageKey = 2;
  }

  addAlternative(alternative: Alternative) {
    this.decisionData.addAlternative({ alternative });
    this.alternativeList.updateAlternatives();

    this.snackBar.openFromTemplate(this.notifyRef, { duration: 3000 });
  }
}
