import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Alternative } from '@entscheidungsnavi/decision-data/classes';
import { ALTERNATIVES_STEPS, NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { HelpMenuProvider, helpPage } from '../../../app/help/help';
import { getExplanationPage } from '../help';
import { TransferService } from '../../../app/transfer';
import { AppSettingsService } from '../../../app/data/app-settings.service';
import { ExplanationService } from '../../shared/decision-quality';
import { HelpBackgroundComponent } from '../hints/help-background/help-background.component';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  selector: 'dt-alternatives',
  templateUrl: './alternatives.component.html',
  styleUrls: ['./alternatives.component.scss'],
})
export class AlternativesComponent implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/alternatives/steps/7').build()],
    middle: [this.explanationService.generateAssessmentButton('CREATIVE_DOABLE_ALTERNATIVES')],
    right: [navLineElement().continue('/impactmodel').build()],
  });

  @Input()
  showTitle = true;

  helpMenu = {
    educational: [
      helpPage()
        .name($localize`Ergebnisseite`)
        .template(() => this.helpTemplate)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 3`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
    starter: [
      helpPage()
        .name($localize`Schritt ${NAVI_STEP_ORDER.indexOf('alternatives') + 1}\: ${this.languageService.steps.alternatives.name}`)
        .template(() => this.starterModeHelp)
        .build(),
      getExplanationPage('starter'),
    ],
  };

  @ViewChild('starterModeHelp') starterModeHelp: TemplateRef<any>;
  @ViewChild('helpTemplate') helpTemplate: TemplateRef<any>;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
    private currentProgressService: CurrentProgressService,
    private explanationService: ExplanationService,
    protected appSettings: AppSettingsService,
    transferService: TransferService
  ) {
    transferService.onReceived$.pipe(takeUntil(this.onDestroy$)).subscribe(receivedObject => {
      if (receivedObject instanceof Alternative) {
        transferService.openNotification(receivedObject);
      }
    });
  }

  move(from: number, to: number) {
    this.decisionData.moveAlternative(from, to);
  }

  delete(position: number) {
    this.decisionData.removeAlternative(position);
  }

  ngOnInit() {
    this.decisionData.hintAlternatives.subStepProgression = ALTERNATIVES_STEPS[ALTERNATIVES_STEPS.length - 1];
    this.currentProgressService.update();

    if (this.decisionData.alternatives.length === 0) {
      // Make sure at least two alternatives are present
      while (this.decisionData.alternatives.length < 2) {
        this.decisionData.addAlternative();
      }
    }
  }
}
