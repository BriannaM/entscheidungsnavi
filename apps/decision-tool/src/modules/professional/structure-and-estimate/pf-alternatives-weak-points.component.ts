import { Component } from '@angular/core';

@Component({
  template: `<dt-alternative-hint-2
    [showTitle]="false"
    data-cy="structure-and-estimate-tool-weak-points-alternative-list"
  ></dt-alternative-hint-2>`,
  styles: [
    `
      dt-alternative-hint-2 {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
export class PfAlternativesWeakPointsComponent {}
