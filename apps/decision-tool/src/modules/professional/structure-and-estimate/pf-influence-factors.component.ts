import { Component } from '@angular/core';

@Component({
  template:
    '<dt-influence-factors [showTitle]="false" data-cy="structure-and-estimate-tool-influence-factors-list"></dt-influence-factors>',
  styles: [
    `
      dt-influence-factors {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
export class PfInfluenceFactorsComponent {}
