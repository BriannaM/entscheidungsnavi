import { Component } from '@angular/core';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';

@Component({
  template: `<dt-alternative-hint-5
    [showTitle]="false"
    data-cy="structure-and-estimate-tool-lever-method-alternative-list"
  ></dt-alternative-hint-5>`,
  styles: [
    `
      dt-alternative-hint-5 {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfAlternativesLeverMethodComponent {}
