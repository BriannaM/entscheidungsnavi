/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { MatMenuTrigger } from '@angular/material/menu';
import { DisplayAtMaxWidth, wantsToBeDisplayedAtMaximumWidth } from '../../../app/interfaces/display-at-max-width';
import { NavLine, Navigation } from '../../shared/navline';

export const STRUCTURE_AND_ESTIMATE_TOOLS = [
  'sae-overview',
  'sae-assumptions',
  'sae-objective-list',
  'sae-hierarchy',
  'sae-checklists',
  'sae-checklists',
  'sae-alternative-list',
  'sae-weak-points',
  'sae-objective-focused-search',
  'sae-lever-method',
  'sae-influence-factors',
] as const;

export type StructureAndEstimateTool = (typeof STRUCTURE_AND_ESTIMATE_TOOLS)[number];

type EndpointForToolInStructureAndEstimate =
  | ''
  | 'assumptions'
  | 'objective-list'
  | 'hierarchy'
  | 'checklists'
  | 'weak-points'
  | 'objective-focused-search'
  | 'lever-method'
  | 'alternative-list'
  | 'influence-factors';

export const EVALUATE_AND_DECIDE_TOOLS = [
  'ead-overview',
  'ead-utility-functions',
  'ead-objective-weighting',
  'ead-sensitivity-analysis',
  'ead-pros-and-cons',
  'ead-robustness-check',
  'ead-risk-comparison',
] as const;

export type EvaluateAndDecideTool = (typeof EVALUATE_AND_DECIDE_TOOLS)[number];

type EndpointForToolInEvaluateAndDecide =
  | ''
  | 'utility-functions'
  | 'objective-weighting'
  | 'sensitivity-analysis'
  | 'pro-contra'
  | 'robustness-check'
  | 'risk-comparison';

export type Tool = StructureAndEstimateTool | EvaluateAndDecideTool;
export type ToolEndpoint = EndpointForToolInStructureAndEstimate | EndpointForToolInEvaluateAndDecide;

@Component({
  templateUrl: './pf-main.component.html',
  styleUrls: ['./pf-main.component.scss'],
})
@DisplayAtMaxWidth
export class PfMainComponent implements Navigation {
  private static readonly toolToEndpoint: {
    [K in Tool]: ToolEndpoint;
  } = {
    'sae-overview': '',
    'sae-assumptions': 'assumptions',
    'sae-objective-list': 'objective-list',
    'sae-hierarchy': 'hierarchy',
    'sae-checklists': 'checklists',
    'sae-weak-points': 'weak-points',
    'sae-objective-focused-search': 'objective-focused-search',
    'sae-lever-method': 'lever-method',
    'sae-alternative-list': 'alternative-list',
    'sae-influence-factors': 'influence-factors',

    'ead-overview': '',
    'ead-utility-functions': 'utility-functions',
    'ead-objective-weighting': 'objective-weighting',
    'ead-sensitivity-analysis': 'sensitivity-analysis',
    'ead-pros-and-cons': 'pro-contra',
    'ead-robustness-check': 'robustness-check',
    'ead-risk-comparison': 'risk-comparison',
  };

  readonly SAE_OBJECTIVES_TOOL_CATEGORY: Tool[] = ['sae-objective-list', 'sae-hierarchy', 'sae-checklists'];
  readonly SAE_ALTERNATIVES_TOOL_CATEGORY: Tool[] = [
    'sae-alternative-list',
    'sae-weak-points',
    'sae-objective-focused-search',
    'sae-lever-method',
  ];

  readonly EAD_EVALUATION_TOOL_CATEGORY: Tool[] = [
    'ead-sensitivity-analysis',
    'ead-pros-and-cons',
    'ead-robustness-check',
    'ead-risk-comparison',
  ];

  navLine: NavLine = null;

  private _activeTool: Tool | null;

  get activeTool() {
    return this._activeTool;
  }

  set activeTool(toolToActivate: Tool) {
    if (toolToActivate !== this._activeTool) {
      this._activeTool = toolToActivate;
      this.openTool(toolToActivate);
    }
  }

  @ViewChild('menuTrigger') menuTrigger: MatMenuTrigger;

  limitWidth = true;

  activePhase: 'sae' | 'ead' = 'sae';

  hoverTimer: any;

  private static getPathOfTool(tool: Tool) {
    const isStructureAndEstimateTool = tool.startsWith('sae');
    const phaseUrl = '/professional/' + (isStructureAndEstimateTool ? 'structure-and-estimate' : 'evaluate-and-decide');

    if (tool.endsWith('overview')) {
      return phaseUrl;
    } else {
      return phaseUrl + '/' + this.toolToEndpoint[tool];
    }
  }

  constructor(private router: Router, private cdr: ChangeDetectorRef) {}

  openModeSelectionMenu() {
    // I know that it is ugly, but mat menu does not support for example showDelay like with tooltips
    this.hoverTimer = setTimeout(() => {
      this.menuTrigger.openMenu();
    }, 1300);
  }

  resetHoverTimeout() {
    // reset the time for hovering over the switch mode button
    clearTimeout(this.hoverTimer);
  }

  async setPhaseAndTool(component: unknown, route: ActivatedRoute) {
    const data = await firstValueFrom(route.data);

    if ('tool' in data) {
      const tool = data.tool;

      if (STRUCTURE_AND_ESTIMATE_TOOLS.includes(tool)) {
        this.activePhase = 'sae';
      } else if (EVALUATE_AND_DECIDE_TOOLS.includes(tool)) {
        this.activePhase = 'ead';
      } else {
        throw new Error('Unknown tool provided.');
      }

      this._activeTool = tool;
    }

    if (this.limitWidth !== !wantsToBeDisplayedAtMaximumWidth(component)) {
      this.limitWidth = !this.limitWidth;
    }

    this.cdr.detectChanges();
  }

  openTool(tool: Tool) {
    return this.router.navigateByUrl(PfMainComponent.getPathOfTool(tool));
  }

  switchToStructureAndEstimate() {
    if (this.activePhase === 'sae') {
      this.openTool('sae-overview');
    } else {
      this.router.navigateByUrl('/professional/structure-and-estimate');
    }
    this.resetHoverTimeout();
  }

  switchToEvaluateAndDecide() {
    if (this.activePhase === 'ead') {
      this.openTool('ead-overview');
    } else {
      this.router.navigateByUrl('/professional/evaluate-and-decide');
    }
    this.resetHoverTimeout();
  }
}
