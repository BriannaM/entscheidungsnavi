import { Component } from '@angular/core';

@Component({
  template: '<dt-risk-comparison [showTitle]="false"></dt-risk-comparison>',
  styles: [
    `
      dt-risk-comparison {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
export class PfRiskComparisonComponent {}
