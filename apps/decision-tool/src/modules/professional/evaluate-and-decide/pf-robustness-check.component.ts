import { Component } from '@angular/core';

@Component({
  template: '<dt-robustness-check [showTitle]="false"> </dt-robustness-check>',
  styles: [
    `
      dt-robustness-check {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
export class PfRobustnessCheckComponent {}
