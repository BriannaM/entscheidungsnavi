import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EducationalModeGuard } from '../../app/guards';
import { ImpactModelComponent } from './main';
import { InfluenceFactorsComponent } from './influence-factors';

const routes: Routes = [
  {
    path: '',
    component: ImpactModelComponent,
  },
  {
    path: 'uncertaintyfactors',
    canActivate: [EducationalModeGuard],
    data: {
      educationalModeRedirect: '/impactmodel',
    },
    children: [
      {
        path: '',
        component: InfluenceFactorsComponent,
      },
      {
        path: ':id',
        redirectTo: '',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImpactModelRoutingModule {}
