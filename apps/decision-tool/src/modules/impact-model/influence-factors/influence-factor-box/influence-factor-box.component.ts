import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { checkProbabilities, UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { checkType } from '@entscheidungsnavi/tools';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { clamp, debounce } from 'lodash';
import {
  InfluenceFactorModalComponent,
  InfluenceFactorModalData,
  InfluenceFactorModalResult,
} from '../influence-factor-modal/influence-factor-modal.component';
import { InfluenceFactorHelperService } from '../influence-factors-helper.service';
import { ProjectService } from '../../../../app/data/project';

@Component({
  selector: 'dt-influence-factor-box',
  templateUrl: './influence-factor-box.component.html',
  styleUrls: ['./influence-factor-box.component.scss'],
})
export class InfluenceFactorBoxComponent implements OnInit {
  @Input() influenceFactor: UserdefinedInfluenceFactor;
  @Input() usageCount: number;

  @Output() deleteClick = new EventEmitter<void>();
  @Output() isNormalizedChange = new EventEmitter<boolean>();

  readonly pMax = UserdefinedInfluenceFactor.P_MAX;

  // We show unnormalized probabilities, but save normalized ones in the background
  shownProbabilities: number[];
  // Wether the shownProbabilities are normalized. The saved probabilites are always normalized.
  isNormalized: boolean;

  private writeBackProbabilities = debounce(() => {
    // Write the probs back to the influence factor and normalize
    this.shownProbabilities.forEach((prob, index) => {
      this.influenceFactor.states[index].probability = clamp(prob, 0, 100);
    });
    this.influenceFactor.normalizeProbabilities();
  }, 500);

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  constructor(
    private dialog: MatDialog,
    private influenceFactorHelper: InfluenceFactorHelperService,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
    private popOverService: PopOverService
  ) {}

  transferInfluenceFactorToMe(buttonElement: ElementRef<HTMLElement>) {
    this.teamTrait
      .transferObject(this.influenceFactor.uuid)
      .pipe(loadingIndicator(this.snackBar, $localize`Einflussfaktor wird übernommen…`))
      .subscribe({
        next: () => this.popOverService.whistle(buttonElement, $localize`Einflussfaktor übernommen!`, 'check'),
        error: () =>
          this.popOverService.whistle(buttonElement, $localize`Beim Übernehmen des Einflussfaktors ist ein Fehler aufgetreten.`, 'error'),
      });
  }

  ngOnInit() {
    this.initProbabilities();
  }

  changePrecision(newPrecision: number) {
    this.influenceFactor.precision = clamp(newPrecision, 0, UserdefinedInfluenceFactor.P_MAX);
  }

  adjustPrecision(change: number) {
    this.changePrecision(this.influenceFactor.precision + change);
  }

  changeProbability(index: number, value: number) {
    this.shownProbabilities[index] = value;
    this.checkNormalized();

    this.writeBackProbabilities();
  }

  adjustProbability(index: number, change: number) {
    this.changeProbability(index, this.shownProbabilities[index] + change);
  }

  private checkNormalized() {
    const isNormalized = checkProbabilities(this.shownProbabilities);
    if (isNormalized !== this.isNormalized) {
      this.isNormalized = isNormalized;
      this.isNormalizedChange.next(isNormalized);
    }
  }

  normalize() {
    this.writeBackProbabilities.flush();
    this.initProbabilities();
  }

  initProbabilities() {
    this.shownProbabilities = this.influenceFactor.states.map(state => state.probability ?? 0);
    this.checkNormalized();
  }

  edit() {
    this.dialog
      .open(InfluenceFactorModalComponent, {
        data: checkType<InfluenceFactorModalData>({
          influenceFactor: this.influenceFactor,
          usageCount: this.usageCount,
          probabilities: this.shownProbabilities,
          onSaveCallback: () => this.initProbabilities(),
        }),
      })
      .afterClosed()
      .subscribe((result: InfluenceFactorModalResult) => {
        if (result.showConnections) this.showConnections();
      });
  }

  showConnections() {
    this.influenceFactorHelper.showInfluenceFactorConnections(this.influenceFactor);
  }

  relativeDeviation(probability: number) {
    return probability > 0 ? this.influenceFactor.precision * Math.min(1 - probability / 100, probability / 100) : 0;
  }
}
