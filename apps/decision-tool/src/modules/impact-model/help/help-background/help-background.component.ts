import { Component } from '@angular/core';

@Component({
  templateUrl: './help-background.component.html',
  styleUrls: ['../../../hints.scss'],
})
export class HelpBackgroundComponent {}
