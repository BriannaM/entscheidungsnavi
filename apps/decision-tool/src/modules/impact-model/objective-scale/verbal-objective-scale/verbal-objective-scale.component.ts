import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { VerbalObjectiveData } from '@entscheidungsnavi/decision-data/classes';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { PopOverRef, PopOverService } from '@entscheidungsnavi/widgets';
import { cloneDeep, isEqual, zip } from 'lodash';
import { Observable, map, distinctUntilChanged, startWith, takeUntil } from 'rxjs';

@Component({
  selector: 'dt-verbal-objective-scale',
  templateUrl: './verbal-objective-scale.component.html',
  styleUrls: ['./verbal-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerbalObjectiveScaleComponent implements OnInit {
  @Input() verbalData: VerbalObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  initParamsForm: ReturnType<typeof this.getInitParamsForm>;
  statesForm: ReturnType<typeof this.getStatesForm>;

  private initialValue: typeof this.statesForm.value;

  @ViewChild('initParamsPopover') initParamsPopover: TemplateRef<any>;
  @ViewChild('initParamsOpener', { read: ElementRef }) initParamsOpener: ElementRef;
  openPopover: PopOverRef;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(private fb: NonNullableFormBuilder, private popover: PopOverService) {}

  ngOnInit() {
    this.initParamsForm = this.getInitParamsForm();
    this.statesForm = this.getStatesForm();
    this.initialValue = cloneDeep(this.statesForm.value);

    // We do not care about the validity of the init params
    this.statesForm.statusChanges.pipe(takeUntil(this.onDestroy$)).subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.statesForm.valid);

    this.statesForm.valueChanges
      .pipe(
        startWith(null),
        map(() => this.checkHasChanges()),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe(hasChanges => this.isDirty.emit(hasChanges));

    if (this.statesForm.value.states.length === 0) {
      this.applyInitParams();
    }
  }

  private checkHasChanges() {
    return !isEqual(this.initialValue, this.statesForm.value);
  }

  save() {
    this.verbalData.initParams.from = this.initParamsForm.value.from;
    this.verbalData.initParams.to = this.initParamsForm.value.to;
    this.verbalData.initParams.stepNumber = this.initParamsForm.value.stepNumber;

    const statesFormArray = this.statesForm.controls.states;
    while (this.verbalData.options.length > statesFormArray.length) {
      this.verbalData.removeOption(this.verbalData.options.length - 1);
    }
    statesFormArray.controls.forEach((stateGroup, index) => {
      if (this.verbalData.options.length > index) {
        this.verbalData.options[index] = stateGroup.get('name').value;
      } else {
        this.verbalData.addOption(index, stateGroup.get('name').value);
      }
      this.verbalData.comments[index] = stateGroup.get('comment').value;
    });
  }

  openInitParams() {
    this.openPopover = this.popover.open(this.initParamsPopover, this.initParamsOpener, {
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });
  }

  applyInitParams() {
    if (!this.initParamsForm.valid) {
      return;
    }

    const from: string = this.initParamsForm.get('from').value,
      to: string = this.initParamsForm.get('to').value;
    const optionNameTemplates = [
      $localize`sehr ${from}`,
      from,
      $localize`eher ${from}`,
      $localize`mittel`,
      $localize`eher ${to}`,
      to,
      $localize`sehr ${to}`,
    ];

    let optionIndices: number[] = [];
    switch (this.initParamsForm.get('stepNumber').value) {
      case 2:
        optionIndices = [1, 5];
        break;
      case 3:
        optionIndices = [1, 3, 5];
        break;
      case 4:
        optionIndices = [1, 2, 4, 5];
        break;
      case 5:
        optionIndices = [1, 2, 3, 4, 5];
        break;
      case 6:
        optionIndices = [0, 1, 2, 4, 5, 6];
        break;
      case 7:
        optionIndices = [0, 1, 2, 3, 4, 5, 6];
        break;
    }
    const newOptionNames = optionIndices.map(index => optionNameTemplates[index]);

    const statesFormArray = this.statesForm.controls.states;
    statesFormArray.clear();
    newOptionNames.forEach(option => statesFormArray.push(this.getStateForm(option, '')));
  }

  addState() {
    this.statesForm.controls.states.push(this.getStateForm('', ''));
  }

  deleteState(index: number) {
    this.statesForm.controls.states.removeAt(index);
  }

  private getInitParamsForm() {
    return this.fb.group({
      from: [this.verbalData.initParams.from ?? $localize`gering`, Validators.required],
      to: [this.verbalData.initParams.to ?? $localize`hoch`, Validators.required],
      stepNumber: [this.verbalData.initParams.stepNumber, [Validators.required, Validators.min(2), Validators.max(7)]],
    });
  }

  private getStatesForm() {
    const statesFormArray = this.fb.array(
      zip(this.verbalData.options, this.verbalData.comments).map(([name, comment]) => this.getStateForm(name, comment))
    );
    return this.fb.group({ states: statesFormArray });
  }

  private getStateForm(name: string, comment: string) {
    return this.fb.group({
      name: [name, Validators.required],
      comment: [comment],
    });
  }

  drop(event: CdkDragDrop<unknown>) {
    moveItemInArray(this.statesForm.controls.states.controls, event.previousIndex, event.currentIndex);
  }

  getColorForRank(index: number) {
    return `rgb(${200 * (1 - index / this.statesForm.controls.states.length)}, ${
      200 * (index / this.statesForm.controls.states.length)
    }, 0)`;
  }
}
