import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  Alternative,
  Indicator,
  InfluenceFactor,
  Objective,
  ObjectiveInput,
  PredefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data/classes';
import { VerbalIndicatorForecastModalComponent, VerbalIndicatorForecastModalData } from '../../../forecast-of-outcomes-modal';

@Component({
  selector: 'dt-indicator-inner-table',
  templateUrl: './indicator-inner-table.component.html',
  styleUrls: ['./indicator-inner-table.component.scss'],
})
export class IndicatorInnerTableComponent implements OnInit, OnChanges {
  @Input() alternative: Alternative;
  @Input() indicator: Indicator;
  @Input() indicatorGrouping: 'indicators' | 'states';
  @Input() indicatorIdx: number;
  @Input() influenceFactor: InfluenceFactor;
  @Input() numberOfCategoryColumns: number;
  @Input() objective: Objective;
  @Input() outcomeValues: ObjectiveInput[];
  @Input() stateIdx: number;

  // is state linked with median
  // e.g. [[true, false, false], [false, false, false]] means that
  // worst is implicit and changes together with median (ind1)
  // none of the values is implicit (ind2)
  isValueImplicit: boolean[][][]; // [stateIdx][indicatorIdx][categoryIdx]
  // is block collapsed (only for PredefinedInfluenceFactors)
  areImplicitValuesCollapsed: boolean[][]; // [indicatorIdx][categoryIdx]
  medianIdx: number;

  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    if (this.influenceFactor != null) {
      this.resetImplicitValues();
      this.initializeCollapseValues();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('influenceFactor' in changes) {
      // influence factor has been changed, implicit values don't match anymore
      this.resetImplicitValues();
      this.initializeCollapseValues();
    }
  }

  protected openVerbalIndicatorForecastModal(indicatorIdx: number, stateIdx = 0) {
    this.dialog
      .open<VerbalIndicatorForecastModalComponent, VerbalIndicatorForecastModalData>(VerbalIndicatorForecastModalComponent, {
        data: {
          indicatorIdx,
          alternative: this.alternative,
          objective: this.objective,
          influenceFactor: this.influenceFactor,
          outcomeValues: this.outcomeValues,
          isValueImplicit: this.isValueImplicit,
          stateIdx,
        },
      })
      .afterClosed()
      .subscribe((data: { values: ObjectiveInput[]; isValueImplicit: boolean[][][] }) => {
        if (data != null) {
          this.outcomeValues = data.values;
          this.isValueImplicit = data.isValueImplicit;
          if (this.influenceFactor instanceof PredefinedInfluenceFactor) {
            this.initializeCollapseValues();
          }
        }
      });
  }

  protected isInfluenceFactorPredefined(influenceFactor: InfluenceFactor): influenceFactor is PredefinedInfluenceFactor {
    return influenceFactor instanceof PredefinedInfluenceFactor;
  }

  protected isValueGrayedOut(stateIdx: number, indicatorIdx: number, categoryIdx = 0) {
    if (this.influenceFactor instanceof PredefinedInfluenceFactor) {
      return this.isValueImplicit[stateIdx][indicatorIdx][categoryIdx];
    }
    return false;
  }

  protected isBlockCollapsed(indicatorIdx: number, categoryIdx = 0) {
    return (
      this.objective.includesVerbalIndicators &&
      this.influenceFactor instanceof PredefinedInfluenceFactor &&
      this.areImplicitValuesCollapsed[indicatorIdx][categoryIdx]
    );
  }

  // computes implicit values on page load and influence factor change
  private resetImplicitValues() {
    this.medianIdx = Math.floor(this.outcomeValues.length / 2);
    this.isValueImplicit = this.outcomeValues.map((valuesPerState, stateIdx) =>
      valuesPerState.map((valuesPerIndicator, indicatorIdx) =>
        valuesPerIndicator.map(
          (valuePerCategory, categoryIdx) =>
            valuePerCategory === this.outcomeValues[this.medianIdx][indicatorIdx][categoryIdx] && this.medianIdx !== stateIdx
        )
      )
    );
  }

  private initializeCollapseValues() {
    if (!this.objective.includesVerbalIndicators || this.influenceFactor == null) return;
    if (this.areImplicitValuesCollapsed == null) this.areImplicitValuesCollapsed = [];
    this.isValueImplicit[0].forEach((valuesPerIndicator, indicatorIdx) => {
      if (this.areImplicitValuesCollapsed[indicatorIdx] == null) {
        this.areImplicitValuesCollapsed[indicatorIdx] = [];
      }
      valuesPerIndicator.forEach((_, categoryIdx) => {
        const newCollapseValue = this.isValueImplicit
          .map(x => x[indicatorIdx][categoryIdx])
          .every((value, stageIdx) => value || stageIdx === this.medianIdx);
        if (this.areImplicitValuesCollapsed[indicatorIdx][categoryIdx] != null) {
          this.areImplicitValuesCollapsed[indicatorIdx][categoryIdx] &&= newCollapseValue;
        } else {
          this.areImplicitValuesCollapsed[indicatorIdx].push(newCollapseValue);
        }
      });
    });
  }

  // change Worst/Best values based on Median
  protected handleValueChange(newValue: number, stateIdx: number, indicatorIdx: number) {
    this.outcomeValues[stateIdx][indicatorIdx] = [newValue]; // set value
    if (this.influenceFactor instanceof PredefinedInfluenceFactor) {
      this.isValueImplicit[stateIdx][indicatorIdx][0] = false; // no longer implicit
      if (stateIdx === this.medianIdx) {
        // it's median
        this.outcomeValues.forEach((_, currentStateIdx) => {
          if (this.isValueImplicit[currentStateIdx][indicatorIdx][0]) {
            // set other implicit values to median value
            this.outcomeValues[currentStateIdx][indicatorIdx] = [newValue];
          }
        });
      }
    }
  }

  // change Worst/Best verbal indicator values based on Median
  protected handleVerbalIndicatorValueChange(newValue: number, stateIdx: number, indicatorIdx: number, categoryIdx: number) {
    this.outcomeValues[stateIdx][indicatorIdx][categoryIdx] = newValue; // set value
    if (this.influenceFactor instanceof PredefinedInfluenceFactor) {
      this.isValueImplicit[stateIdx][indicatorIdx][categoryIdx] = false; // no longer implicit
      if (stateIdx === this.medianIdx) {
        // it's median
        this.outcomeValues.forEach((_, currentStateIdx) => {
          if (this.isValueImplicit[currentStateIdx][indicatorIdx][categoryIdx]) {
            // set other implicit values to median value
            this.outcomeValues[currentStateIdx][indicatorIdx][categoryIdx] = newValue;
          }
        });
      }
    }
  }
}
