import { ChangeDetectorRef, Component, ElementRef, Input, OnInit } from '@angular/core';
import { Alternative, InfluenceFactor, Objective, ObjectiveInput } from '@entscheidungsnavi/decision-data/classes';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'dt-indicator-values',
  templateUrl: './indicator-values.component.html',
  styleUrls: ['./indicator-values.component.scss'],
})
export class IndicatorValuesComponent implements OnInit {
  @Input() alternative: Alternative;
  @Input() objective: Objective;
  @Input() outcomeValues: ObjectiveInput[];
  @Input() influenceFactor: InfluenceFactor;

  /* Template values. */
  numberOfCategoryColumns: number; // needed for colspan for indicatorGrouping = 'states'
  indicatorGrouping: 'indicators' | 'states' = 'indicators';

  /* Constants. */
  readonly nameWidthPercentage = 25; // in %
  readonly valueWidthPercentage = 8; // in %

  constructor(private dialog: MatDialog, public elementRef: ElementRef, private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    const numberOfCategoriesPerIndicator = this.objective.indicatorData.indicators.map(indicator => {
      return Math.max(indicator.verbalIndicatorCategories.length, 1); // colspan = 1 is minimum
    });
    this.numberOfCategoryColumns = this.lcmOfArray(numberOfCategoriesPerIndicator); // minimal number of columns determined using LCM
  }

  protected max(a: number, b: number) {
    return Math.max(a, b);
  }

  // calculates the greatest common divisor
  private gcd(a: number, b: number): number {
    return !b ? a : this.gcd(b, a % b);
  }

  // calculates the least common multiple of two numbers
  private lcm(a: number, b: number) {
    return (a * b) / this.gcd(a, b);
  }

  // calculates the least common multiple of an array of numbers
  private lcmOfArray(arr: number[]) {
    return arr.reduce((prev, curr) => this.lcm(prev, curr), 1);
  }
}
