import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import {
  Alternative,
  InfluenceFactor,
  Objective,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactor,
  UserdefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { isEqual } from 'lodash';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import { lastValueFrom } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { checkType } from '@entscheidungsnavi/tools';
import { calculateIndicatorValue } from '@entscheidungsnavi/decision-data/calculation';
import {
  InfluenceFactorHelperService,
  InfluenceFactorModalComponent,
  InfluenceFactorModalData,
  InfluenceFactorModalResult,
} from '../../influence-factors';

export interface ForecastOfOutcomesModalData {
  selectedTabIndex: number;
  alternative: Alternative;
  objective: Objective;
  outcome: Outcome;
}

@Component({
  templateUrl: './forecast-of-outcomes-modal.component.html',
  styleUrls: ['./forecast-of-outcomes-modal.component.scss'],
})
export class ForecastOfOutcomesModalComponent implements OnInit {
  @Input() objective: Objective;
  @Input() alternative: Alternative;
  @Input() outcome: Outcome;

  @Output() modalSave = new EventEmitter<void>();
  @Output() influenceFactorModified = new EventEmitter<number>(); // Emits the ID of the modified influence factor

  copy: Outcome;
  medianIdx: number;

  isValueImplicit: boolean[] = []; // e.g. [true, false, false] means that worst is implicit and changes together with median
  areImplicitValuesCollapsed = false; // values of PredefinedInfluenceFactors are collapsed by default

  selectedTabIndex = 0;
  readonly predefinedInfluenceFactors = Object.values(PREDEFINED_INFLUENCE_FACTORS);

  readonly wrongOrderMessage = $localize`Unzulässige Reihenfolge der angegebenen Werte.`;
  readonly noTornadoAvailableMessage = $localize`Die Darstellung eines Tornadodiagramms setzt voraus,
  dass eine Indikatorskala mit systemseitigen Einflussfaktoren verknüpft wurde.`;

  get hasChanges() {
    return !(
      isEqual(this.outcome.values, this.copy.values) &&
      this.outcome.influenceFactor === this.copy.influenceFactor &&
      this.outcome.comment === this.copy.comment &&
      this.outcome.processed === this.copy.processed
    );
  }

  get isValid() {
    return this.copy.values.every(value => validateValue(value, this.objective)[0]) && this.isCorrectSystemSidedIF();
  }

  get isTornadoAvailable() {
    return this.objective.isIndicator && this.isInfluenceFactorPredefined(this.copy.influenceFactor) && this.isCorrectSystemSidedIF();
  }

  get viewType() {
    return this.objective.isIndicator ? 'indicatorView' : this.copy.influenceFactor == null ? 'noIndicatorWithoutIF' : 'noIndicatorWithIF';
  }

  constructor(
    protected decisionData: DecisionData,
    @Inject(MAT_DIALOG_DATA) data: ForecastOfOutcomesModalData,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ForecastOfOutcomesModalComponent>,
    private influenceFactorHelper: InfluenceFactorHelperService
  ) {
    this.selectedTabIndex = data.selectedTabIndex;
    this.alternative = data.alternative;
    this.objective = data.objective;
    this.outcome = data.outcome;
  }

  ngOnInit() {
    this.copy = this.outcome.clone();
    this.resetImplicitValues();
    if (!this.isValid) {
      this.selectedTabIndex = 0;
    } else if (this.selectedTabIndex === 2 && !this.isTornadoAvailable) {
      this.selectedTabIndex = 1;
    }
    this.medianIdx = Math.floor(this.copy.values.length / 2);
  }

  close() {
    this.dialogRef.close(true);
  }

  async tryClose() {
    if (!this.hasChanges || (await this.confirmDiscard())) {
      this.close();
      return true;
    }

    return false;
  }

  private async confirmDiscard() {
    const result = await lastValueFrom(
      this.dialog
        .open(ConfirmModalComponent, {
          data: checkType<ConfirmModalData>({
            title: $localize`Ungespeicherte Änderungen`,
            prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen an der Wirkungsprognose verwerfen willst?`,
            template: 'discard',
          }),
        })
        .afterClosed()
    );
    return !!result;
  }

  apply() {
    if (this.isValid) {
      this.save();
      this.close();
    }
  }

  save() {
    this.outcome.copyBack(this.copy);
  }

  changeUncertaintyFactor(newUf: UserdefinedInfluenceFactor) {
    this.copy.setInfluenceFactor(newUf);
    this.resetImplicitValues();
  }

  /* Checks if row of values follows the order the corresponding (indicator/total) scale. */
  isCorrectSystemSidedIF() {
    if (!this.isInfluenceFactorPredefined(this.copy.influenceFactor) || this.copy.influenceFactor == null) {
      return true;
    }
    if (this.objective.isIndicator) {
      for (let i = 0; i < this.copy.values[0].length; i++) {
        const direction = this.objective.indicatorData.indicators[i].max - this.objective.indicatorData.indicators[i].min;
        const indicator = this.objective.indicatorData.indicators[i];
        if (
          (Math.sign(
            calculateIndicatorValue(this.copy.values[2][i], indicator) - calculateIndicatorValue(this.copy.values[1][i], indicator)
          ) !== Math.sign(direction) &&
            calculateIndicatorValue(this.copy.values[2][i], indicator) - calculateIndicatorValue(this.copy.values[1][i], indicator) !==
              0) ||
          (Math.sign(
            calculateIndicatorValue(this.copy.values[1][i], indicator) - calculateIndicatorValue(this.copy.values[0][i], indicator)
          ) !== Math.sign(direction) &&
            calculateIndicatorValue(this.copy.values[1][i], indicator) - calculateIndicatorValue(this.copy.values[0][i], indicator) !== 0)
        ) {
          return false;
        }
      }
    } else {
      let direction = 1; // always positive for verbal scale
      if (this.objective.isNumerical) {
        direction = this.objective.numericalData.to - this.objective.numericalData.from;
      }
      const copyValues = this.copy.values;
      if (
        (Math.sign(copyValues[2][0][0] - copyValues[1][0][0]) !== Math.sign(direction) &&
          copyValues[2][0][0] - copyValues[1][0][0] !== 0) ||
        (Math.sign(copyValues[1][0][0] - copyValues[0][0][0]) !== Math.sign(direction) && copyValues[1][0][0] - copyValues[0][0][0] !== 0)
      ) {
        return false;
      }
    }
    return true;
  }

  /**
   * The precision in decision-data is relative to the probability. This is calculated here.
   */
  relativeDeviation(probability: number, praezision: number) {
    return probability > 0 ? praezision * Math.min(1 - probability / 100, probability / 100) : 0;
  }

  isInfluenceFactorPredefined(influenceFactor: InfluenceFactor): influenceFactor is PredefinedInfluenceFactor {
    return influenceFactor instanceof PredefinedInfluenceFactor;
  }

  editInfluenceFactor() {
    if (this.copy.influenceFactor != null) {
      const influenceFactor = this.copy.influenceFactor as UserdefinedInfluenceFactor;
      this.dialog
        .open(InfluenceFactorModalComponent, {
          data: checkType<InfluenceFactorModalData>({
            influenceFactor: influenceFactor,
            onSaveCallback: influenceFactorId => {
              this.changeUncertaintyFactor(this.decisionData.influenceFactors[influenceFactorId]);
              this.influenceFactorModified.emit(influenceFactorId);
            },
          }),
        })
        .afterClosed()
        .subscribe(async (result: InfluenceFactorModalResult) => {
          if (result.showConnections && (await this.tryClose())) {
            this.influenceFactorHelper.showInfluenceFactorConnections(influenceFactor);
          }
        });
    }
  }

  addInfluenceFactor() {
    this.dialog.open(InfluenceFactorModalComponent, {
      data: checkType<InfluenceFactorModalData>({
        onSaveCallback: influenceFactorId => this.changeUncertaintyFactor(this.decisionData.influenceFactors[influenceFactorId]),
      }),
    });
  }

  // computes implicit values for numerical and verbal values on page load and influence factor change
  resetImplicitValues() {
    if (this.copy.influenceFactor instanceof PredefinedInfluenceFactor) {
      this.medianIdx = Math.floor(this.copy.values.length / 2);
      if (!this.objective.isIndicator) {
        // create numerical/verbal implicit values
        this.isValueImplicit = this.copy.values.map((_, stateIdx) => {
          return this.copy.values[stateIdx][0][0] === this.copy.values[this.medianIdx][0][0] && stateIdx !== this.medianIdx;
        });
        this.areImplicitValuesCollapsed = this.isValueImplicit.every((value, stageIdx) => value || stageIdx === this.medianIdx);
      }
    }
  }

  // change implicit values if median is changed, if implicit value is changed - make it non-implicit
  handleValueChangePredefinedUF(newValue: number, stateIdx: number) {
    const medianIdx = Math.floor(this.copy.values.length / 2);
    this.copy.values[stateIdx][0][0] = newValue;
    if (this.copy.influenceFactor instanceof PredefinedInfluenceFactor && stateIdx === medianIdx) {
      // change implicit values to median value
      this.copy.values.forEach((_, currentStateIdx) => {
        this.copy.values[currentStateIdx][0][0] = newValue;
      });
    }
  }

  isValueGrayedOut(stateIdx: number, indicatorIdx?: number) {
    if (this.copy.influenceFactor instanceof PredefinedInfluenceFactor) {
      if (this.objective.isIndicator) {
        return this.isValueImplicit[stateIdx][indicatorIdx];
      }
      return this.isValueImplicit[stateIdx];
    }
    return false;
  }
}
