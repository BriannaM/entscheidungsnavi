import { ChangeDetectorRef, Component, Host, HostBinding, Input, NgZone, OnInit, Optional, TrackByFunction } from '@angular/core';
import { TeamComment } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { NoteBtnComponent } from '@entscheidungsnavi/widgets';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, exhaustMap, filter, interval, map, Observable, of, takeUntil } from 'rxjs';
import { ProjectService } from '../../../../app/data/project';

@Component({
  selector: 'dt-team-comments',
  templateUrl: './team-comments.component.html',
  styleUrls: ['./team-comments.component.scss'],
})
export class TeamCommentsComponent implements OnInit {
  @Input()
  object: { uuid: string };

  @Input()
  memberId?: string;

  @Input()
  firstUnreadCommentId: string;

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  trackCommentById: TrackByFunction<TeamComment> = (_, comment) => comment.id;

  constructor(
    @Host() @Optional() private noteBtn: NoteBtnComponent,
    private projectService: ProjectService,
    private cdRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private zone: NgZone
  ) {}

  ngOnInit() {
    if (this.noteBtn) {
      this.noteBtn.hasTeamComments = this.comments.length > 0;
    }

    this.zone.runOutsideAngular(() => {
      interval(5000)
        .pipe(
          map(() => this.projectService.getTeamTrait()),
          filter(teamTrait => (this.noteBtn == null || this.noteBtn.isOpen) && teamTrait != null),
          exhaustMap(teamTrait => teamTrait.updateCommentsFor(this.objectId, this.memberId)),
          catchError(_ => of()),
          takeUntil(this.onDestroy$)
        )
        .subscribe(_ => {
          this.zone.run(() => {
            this.commentsChanged();
          });
        });
    });
  }

  commentsChanged() {
    if (this.noteBtn) {
      this.noteBtn.hasTeamComments = this.comments.length > 0;
    }

    this.cdRef.markForCheck();
  }

  @HostBinding('class.active')
  get isTeamProject() {
    return this.projectService.getTeamTrait() != null;
  }

  get objectId() {
    return this.object.uuid;
  }

  get comments() {
    const team = this.projectService.getTeamTrait();
    if (!team) {
      return [];
    }

    const member = this.memberId ? team.getTeamMemberFromMemberId(this.memberId) : team.activeMember;
    return member.comments.filter((c: { objectId: string }) => c.objectId === this.objectId).reverse();
  }

  getAuthorDisplayNameForComment(comment: TeamComment) {
    const member = this.projectService.getTeamTrait().getTeamMemberFromMemberId(comment.author);
    return member.user.name ?? member.user.email;
  }

  canDeleteComment(comment: TeamComment) {
    const team = this.projectService.getTeamTrait();
    return comment.author === team.userMember.id || (this.memberId ?? team.activeMember.id) === team.userMember.id;
  }

  addComment(input: HTMLInputElement) {
    const content = input.value;

    if (!content) {
      return;
    }

    this.projectService
      .getTeamTrait()
      .addComment(this.objectId, content, this.memberId)
      .subscribe({
        complete: () => this.commentsChanged(),
        error: () => this.snackBar.open($localize`Beim Hinzufügen des Kommentars ist ein Fehler aufgetreten`, $localize`Ok`),
      });

    input.value = '';
  }

  deleteComment(commentId: string) {
    this.projectService
      .getTeamTrait()
      .deleteComment(commentId, this.memberId)
      .subscribe({
        error: () => this.snackBar.open($localize`Beim Löschen des Kommentars ist ein Fehler aufgetreten`, $localize`Ok`),
        complete: () => this.commentsChanged(),
      });
  }
}
