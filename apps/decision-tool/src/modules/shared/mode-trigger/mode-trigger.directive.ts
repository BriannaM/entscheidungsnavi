import { Directive, HostListener, Input } from '@angular/core';
import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { ModeTransitionService } from '../mode-transition/mode-transition.service';

@Directive({
  selector: '[dtModeTrigger]',
})
export class ModeTriggerDirective {
  @Input('dtModeTrigger')
  mode: ProjectMode;

  constructor(private modeTransitionService: ModeTransitionService) {}

  @HostListener('click')
  async onClick() {
    const to = this.mode;

    if (to === 'professional') {
      await this.modeTransitionService.transitionIntoProfessional();
    } else if (to === 'educational') {
      await this.modeTransitionService.transitionIntoEducational();
    }
  }
}
