import { InjectionToken } from '@angular/core';

export const HELP_PAGE_CONTEXT = new InjectionToken<string>('HELP_PAGE_CONTEXT');
