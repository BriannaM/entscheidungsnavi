import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { helpPage } from '../../../app/help/help';
import { ObjectiveAspectHierarchyShortcutsComponent } from './aspect-hierarchy-shortcuts.component';
import { ZieleExplanationComponent } from './explanation.component';

export function getTutorialPage() {
  return helpPage()
    .youtube('H3d0XlZuaPk')
    .name($localize`YouTube-Tutorial zu Schritt 2`)
    .build();
}

export function getExplanationPage(mode: ProjectMode) {
  return helpPage()
    .explanation(ZieleExplanationComponent)
    .name(
      mode === 'educational'
        ? $localize`Wozu dient Schritt ${NAVI_STEP_ORDER.indexOf('objectives') + 1}?`
        : $localize`Mehr zur Strukturierung der Fundamentalziele`
    )
    .build();
}

export function getShortcutsPage() {
  return helpPage()
    .name($localize`Shortcuts in der Zielhierarchie`)
    .component(ObjectiveAspectHierarchyShortcutsComponent)
    .build();
}
