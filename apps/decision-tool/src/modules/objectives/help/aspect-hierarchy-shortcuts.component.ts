import { Component } from '@angular/core';
import { PlatformDetectService } from '@entscheidungsnavi/widgets';

@Component({
  selector: 'dt-aspect-hierarchy-shortcuts',
  templateUrl: './aspect-hierarchy-shortcuts.component.html',
  styleUrls: ['./aspect-hierarchy-shortcuts.component.scss', '../../hints.scss'],
})
export class ObjectiveAspectHierarchyShortcutsComponent {
  protected isMacOS: boolean;

  constructor(platformDetectService: PlatformDetectService) {
    this.isMacOS = platformDetectService.macOS;
  }
}
