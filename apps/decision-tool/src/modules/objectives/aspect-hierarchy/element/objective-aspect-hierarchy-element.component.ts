import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Tree } from '@entscheidungsnavi/tools';
import { TextWidthService } from '@entscheidungsnavi/widgets';
import { areLocationSequencesEqual, InsertDirection } from '../objective-aspect-hierarchy.component';
import { HierarchyElement } from '../hierarchy-element';

enum StateOfAddButtons {
  'CLOSED',
  'SUGGESTED',
  'OPEN',
}

@Component({
  selector: 'dt-objective-aspect-hierarchy-element',
  templateUrl: './objective-aspect-hierarchy-element.component.html',
  styleUrls: ['./objective-aspect-hierarchy-element.component.scss'],
})
export class ObjectiveAspectHierarchyElementComponent implements AfterViewInit, OnChanges {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public static FOCUS: number[] = undefined;

  @Input() depth: number;
  @Input() tree: Tree<HierarchyElement>;
  @Input() parentTree: Tree<HierarchyElement>;

  @Input() editable = true;

  @Input() focused = false;

  @Input() focusable = false;

  @Input() forceAspect = false;

  // Flags that determine whether we should offer certain add buttons.
  @Input() enableAddAbove = true;
  @Input() enableAddRight = true;
  @Input() enableAddBelow = true;
  @Input() enableAddLeft = true;

  stateOfAddButtons = StateOfAddButtons.CLOSED;

  @Input() locationSequence: number[];

  @Input() forceHideNoteBtn = false;

  @Input() useObjectivesFromTree = false;

  @Input()
  allowInputSelection = true;

  @Output() valueChange = new EventEmitter<string>();

  @Output() focusMe = new EventEmitter();

  @Output() addNextToMe = new EventEmitter<InsertDirection>();

  @Output() dragged = new EventEmitter<DragEvent>();

  @ViewChild('input', { static: true })
  inputElement: ElementRef<HTMLInputElement>;

  @ViewChild('placeholder', { static: true })
  inputPlaceholderElement: ElementRef<HTMLDivElement>;

  @ViewChild('delete', { static: true })
  deleteButtonElement: ElementRef<HTMLDivElement>;

  @ViewChild('node', { static: true })
  nodeElement: ElementRef<HTMLDivElement>;

  document = document;

  inputFont: string;

  dragging = false;
  draggable = true;

  constructor(protected decisionData: DecisionData, private textWidthService: TextWidthService) {}

  get comment(): string {
    return this.locationSequence.length === 2 ? this.decisionData.objectives[this.locationSequence[1]].comment : '';
  }

  ngAfterViewInit() {
    this.inputFont = this.textWidthService.getComputedFont(window.getComputedStyle(this.inputElement.nativeElement));

    this.calcInputWidth();

    if (
      ObjectiveAspectHierarchyElementComponent.FOCUS !== undefined &&
      areLocationSequencesEqual(ObjectiveAspectHierarchyElementComponent.FOCUS, this.locationSequence)
    ) {
      ObjectiveAspectHierarchyElementComponent.FOCUS = undefined;

      this.focusMe.emit();

      requestAnimationFrame(() => {
        if (this.inputElement) {
          this.inputElement.nativeElement.focus();
        }
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.focused) {
      if (changes.focused.currentValue) {
        this.stateOfAddButtons = StateOfAddButtons.SUGGESTED;
      } else {
        this.stateOfAddButtons = StateOfAddButtons.CLOSED;
      }
    }
  }

  get displayBackgroundColor() {
    return this.tree.value.backgroundColor
      ? this.tree.value.backgroundColor
      : this.depth === 0
      ? '#D3D3D3'
      : this.depth === 1
      ? '#5D666F'
      : undefined;
  }

  get displayTextColor() {
    return this.tree.value.textColor ? this.tree.value.textColor : this.depth === 0 ? '#000000' : this.depth === 1 ? '#FFFFFF' : '#000000';
  }

  stopEdit() {
    requestAnimationFrame(() => {
      this.valueChange.emit(this.inputElement.nativeElement.value);
    });
  }

  dragStart(event: DragEvent) {
    if ((event.target as HTMLElement).tagName === 'INPUT') {
      event.preventDefault();
      event.stopPropagation();
      return;
    }

    this.dragging = true;

    event.dataTransfer.dropEffect = 'move';

    if (this.tree.value.name) {
      event.dataTransfer.setData('text', this.tree.value.name);
    } else {
      event.dataTransfer.setData('text', 'empty aspect');
    }

    this.dragged.emit(event);
  }

  drop(_event: DragEvent) {
    this.nodeElement.nativeElement.removeAttribute('drag');
  }

  dragLeave(_event: DragEvent) {
    this.nodeElement.nativeElement.removeAttribute('drag');
  }

  calcInputWidth(newName?: string) {
    let newValue = newName ?? this.tree.value.name;

    if (newValue === '') {
      newValue = this.tree.value.placeholder ?? newValue;
    }

    this.inputElement.nativeElement.style.width = this.inputPlaceholderElement.nativeElement.style.width = `${
      this.textWidthService.getTextWidth(newValue, this.inputFont) + 10
    }px`;

    this.inputPlaceholderElement.nativeElement.style.width = `calc(${
      this.textWidthService.getTextWidth(newValue, this.inputFont) + 10
    }px + 0.6rem)`;
  }

  addAbove(event: Event) {
    return this.handleAdd(InsertDirection.ABOVE, event);
  }

  addRight(event: Event) {
    return this.handleAdd(InsertDirection.RIGHT, event);
  }

  addBelow(event: Event) {
    return this.handleAdd(InsertDirection.BELOW, event);
  }

  addLeft(event: Event) {
    return this.handleAdd(InsertDirection.LEFT, event);
  }

  private handleAdd(direction: InsertDirection, event: Event) {
    event.stopPropagation();
    this.addNextToMe.emit(direction);
  }

  areAddButtonsOpen() {
    return this.stateOfAddButtons === StateOfAddButtons.OPEN;
  }

  areAddButtonsSuggested() {
    return this.stateOfAddButtons === StateOfAddButtons.SUGGESTED;
  }

  areAddButtonsClosed() {
    return this.stateOfAddButtons === StateOfAddButtons.CLOSED;
  }

  openAddButtons() {
    this.stateOfAddButtons = StateOfAddButtons.OPEN;
  }
}
