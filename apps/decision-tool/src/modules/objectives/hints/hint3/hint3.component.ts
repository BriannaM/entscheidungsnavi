import { Component, Injector } from '@angular/core';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { Tree } from '@entscheidungsnavi/tools';
import { HelpService } from '../../../../app/help/help.service';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { AbstractObjectiveHintComponent } from '../objective-hint.component';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  templateUrl: './hint3.component.html',
  styleUrls: ['../hint3-5.component.scss'],
})
@DisplayAtMaxWidth
export class ZieleHint3Component extends AbstractObjectiveHintComponent {
  exampleTree: Tree<ObjectiveElement> = Tree.from({
    value: { name: $localize`Lebensqualität` },
    children: [
      {
        value: { name: $localize`Gesamtentlohnung (nächste drei Jahre)` },
        children: [
          { value: { name: $localize`Anfangseinkommen` }, children: [] },
          {
            value: { name: $localize`Geldwerte Vorteile` },
            children: [
              { value: { name: $localize`Handy` }, children: [] },
              { value: { name: $localize`Dienstwagen` }, children: [] },
            ],
          },
        ],
      },
      {
        value: { name: $localize`Freude an der Arbeit` },
        children: [
          { value: { name: $localize`Interessante Themen` }, children: [] },
          { value: { name: $localize`Nette Kollegen` }, children: [] },
          { value: { name: $localize`Intellektuelle Herausforderung` }, children: [] },
          { value: { name: $localize`Eigenverantwortung` }, children: [] },
        ],
      },
      {
        value: { name: $localize`Weiterentwicklungsmöglichkeiten` },
        children: [
          {
            value: { name: $localize`Aufstiegsmöglichkeiten` },
            children: [
              { value: { name: $localize`Fähigkeiten` }, children: [] },
              { value: { name: $localize`Erfahrungen` }, children: [] },
            ],
          },
          {
            value: { name: $localize`Jobwechsel` },
            children: [
              { value: { name: $localize`Vernetzung` }, children: [] },
              { value: { name: $localize`Reputation` }, children: [] },
            ],
          },
        ],
      },
      {
        value: { name: $localize`Freizeitnutzen` },
        children: [
          {
            value: { name: $localize`Freizeitmöglichkeiten` },
            children: [
              { value: { name: $localize`Attraktives örtliches Umfeld` }, children: [] },
              { value: { name: $localize`Nähe zu Eltern und Freunden` }, children: [] },
            ],
          },
          {
            value: { name: $localize`Menge an nutzbarer Freizeit` },
            children: [
              { value: { name: $localize`Geringe Arbeitszeiten` }, children: [] },
              { value: { name: $localize`Geringe Fahrzeiten` }, children: [] },
              { value: { name: $localize`Geringe Flexibilität` }, children: [] },
            ],
          },
        ],
      },
      {
        value: { name: $localize`Attraktive Wohnsituation` },
        children: [
          { value: { name: $localize`Kostenniveau in der Region` }, children: [] },
          { value: { name: $localize`Wohnkomfort` }, children: [] },
          { value: { name: $localize`Umzugskosten` }, children: [] },
        ],
      },
    ],
  });

  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 2`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(injector: Injector, public helpService: HelpService) {
    super(injector);
    this.pageKey = 3;
  }
}
