import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  templateUrl: './delete-objective-modal.component.html',
  styleUrls: ['./delete-objective-modal.component.scss'],
})
export class DeleteObjectiveModalComponent {
  get name() {
    return this.decisionData.objectives[this.objectiveIndex].name;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public objectiveIndex: number,
    private dialogRef: MatDialogRef<DeleteObjectiveModalComponent>,
    private decisionData: DecisionData
  ) {}

  close(confirmDelete = false) {
    this.dialogRef.close(confirmDelete);
  }
}
