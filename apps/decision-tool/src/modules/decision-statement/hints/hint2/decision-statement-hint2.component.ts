import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Value } from '@entscheidungsnavi/decision-data/classes';
import { ModalComponent, TabsComponent } from '@entscheidungsnavi/widgets';
import { QuickstartService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, EMPTY, of } from 'rxjs';
import { sum } from 'lodash';
import { AbstractDecisionStatementHint } from '../decision-statement-hint';
import { helpPage } from '../../../../app/help/help';
import { Help1Component } from '../hint2/help1/help1.component';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { LanguageService } from '../../../../app/data/language.service';

@Component({
  templateUrl: './decision-statement-hint2.component.html',
  styleUrls: ['./decision-statement-hint2.component.scss'],
})
export class DecisionStatementHint2Component extends AbstractDecisionStatementHint implements OnInit, OnDestroy {
  isLoading = false;
  groupSize = 0;
  private hasReset = false;

  get state(): 'loading' | 'empty' | 'ready' {
    if (this.isLoading) return 'loading';
    else if (this.decisionData.decisionStatement.values.length === 0) return 'empty';
    else return 'ready';
  }

  contextChangeResolve: () => void;
  @ViewChild('changeContextModal') changeContextModal: ModalComponent;
  @ViewChild(TabsComponent) tabs: TabsComponent;

  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 1`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(
    injector: Injector,
    private quickstartService: QuickstartService,
    private snackBar: MatSnackBar,
    private languageService: LanguageService
  ) {
    super(injector);
    this.page = 2;
  }

  override ngOnInit() {
    super.ngOnInit();

    if (this.decisionData.decisionStatement.values.length === 0) {
      this.resetValues();
    } else {
      this.sortValues();
    }
  }

  ngOnDestroy() {
    this.performTracking();
  }

  private performTracking() {
    // Only track if we loaded a set of new values this time
    if (!this.hasReset || this.groupSize === 0) return;

    // The average weight over ALL set values, not only the ones from the backend
    const sumOfWeights = sum(this.decisionData.decisionStatement.values.map(value => value.val).filter(value => value != null));

    // All values that stem from our backend list
    const valuesFromBackend = this.decisionData.decisionStatement.values.filter(value => value.valueId != null);

    if (valuesFromBackend.length > 0 && sumOfWeights > 0) {
      this.quickstartService
        .trackValueUsage({
          values: valuesFromBackend.map(value => ({
            id: value.valueId,
            // Divide the values by the average to account for different weight scaling of users
            // (some might only select values between 50 and 60, some between 10 and 90).
            score: (value.val || 0) / sumOfWeights,
          })),
        })
        .pipe(catchError(() => EMPTY))
        .subscribe();
    }
  }

  resetValues() {
    this.isLoading = true;
    this.quickstartService
      .getValues()
      .pipe(
        catchError(() => {
          this.snackBar.open($localize`Fehler beim Laden der Standard-Werteliste. Die Werteliste wurde stattdessen geleert.`, 'Ok');
          return of([]);
        })
      )
      .subscribe(values => {
        // clone the default values to decisionData
        this.decisionData.decisionStatement.values = values.map(
          value => new Value(this.languageService.isEnglish ? value.name.en : value.name.de, null, value.id)
        );

        this.hasReset = true;
        this.isLoading = false;
        this.groupSize = 0;
      });
  }

  addValue(index?: number) {
    this.decisionData.decisionStatement.addValue(index);
  }

  removeValue(index: number) {
    this.decisionData.decisionStatement.removeValue(index);
    if (index < this.groupSize) {
      // if the value is in the top group, lower the group size, so that no other value moves to the group
      this.groupSize--;
    }
  }

  updateGroupSize() {
    this.groupSize = Math.min(this.decisionData.decisionStatement.values.filter(val => val.val != null).length, 5);
  }

  sortValues() {
    this.decisionData.decisionStatement.sortValues();
    this.updateGroupSize();
  }
}
