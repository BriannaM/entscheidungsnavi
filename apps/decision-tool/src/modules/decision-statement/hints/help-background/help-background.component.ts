import { Component } from '@angular/core';
import { LanguageService } from '../../../../app/data/language.service';

@Component({
  templateUrl: './help-background.component.html',
  styleUrls: ['./help-background.component.scss', '../../../hints.scss'],
})
export class HelpBackgroundComponent {
  constructor(protected languageService: LanguageService) {}
}
