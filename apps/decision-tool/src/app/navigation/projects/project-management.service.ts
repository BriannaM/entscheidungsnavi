import { EnvironmentInjector, Injectable } from '@angular/core';
import { OnlineProjectsService, TeamsService } from '@entscheidungsnavi/api-client';
import { map, mergeMap, of, tap } from 'rxjs';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DecisionDataExportService } from '../../data/decision-data-export.service';
import { ProjectService, OnlineProject, LocalProject } from '../../data/project';
import { checkOnlineProjectSize, saveTimeout } from '../../data/project/save-notification';
import { TrackingService } from '../../data/tracking.service';

/**
 * A helper service to manages the users online projects.
 * It handles the case where the currently opened project is modified gracefully.
 */
@Injectable({ providedIn: 'root' })
export class OnlineProjectManagementService {
  constructor(
    private projectService: ProjectService,
    private exportService: DecisionDataExportService,
    private teamsService: TeamsService,
    private onlineProjectsService: OnlineProjectsService,
    private trackingService: TrackingService,
    private injector: EnvironmentInjector,
    private decisionData: DecisionData
  ) {}

  /**
   * Returns the currently opened online project with this ID, or null, if this project is not opened.
   *
   * @param projectId - The project id
   * @returns The project or null
   */
  private getCurrentProject(projectId: string) {
    const activeProject = this.projectService.getProject();

    if (activeProject instanceof OnlineProject && activeProject.info.id === projectId) {
      return activeProject;
    } else {
      return null;
    }
  }

  createTeam(projectId: string, teamName: string) {
    return this.teamsService.createTeam(projectId, teamName).pipe(
      tap(createdTeam => {
        const activeProject = this.getCurrentProject(projectId);

        if (activeProject) {
          this.trackingService.trackEvent('convert to team', { category: 'project' });
          const newProject = this.injector.runInContext(() => new OnlineProject(activeProject.info, createdTeam));
          this.projectService.project$.next(newProject);
        }
      })
    );
  }

  rename(projectId: string, newName: string) {
    this.trackingService.trackEvent('rename project', { category: 'project' });

    return this.onlineProjectsService.renameProject(projectId, newName).pipe(
      tap(() => {
        const activeProject = this.getCurrentProject(projectId);
        if (activeProject) {
          this.decisionData.decisionProblem = newName;
        }
      })
    );
  }

  delete(projectId: string) {
    this.trackingService.trackEvent('delete project', { category: 'project' });

    return this.onlineProjectsService.deleteProject(projectId).pipe(
      tap(() => {
        const activeProject = this.getCurrentProject(projectId);

        if (activeProject) {
          const newProject = this.injector.runInContext(() => new LocalProject('local'));
          this.projectService.project$.next(newProject);
        }
      })
    );
  }

  saveAs(newName: string, existingProjectId?: string) {
    this.decisionData.decisionProblem = newName;
    const data = this.exportService.dataToText();

    const activeProject = this.projectService.getProject(OnlineProject);

    if (existingProjectId && activeProject?.info.id === existingProjectId) {
      return activeProject.save().pipe(map(() => activeProject));
    } else {
      this.trackingService.trackEvent(existingProjectId ? 'save as new project' : 'save as existing project', { category: 'project' });

      const saveObservable = existingProjectId
        ? this.onlineProjectsService.updateProject(existingProjectId, { name: newName, data })
        : this.onlineProjectsService.addProject(newName, data);

      return of(data).pipe(
        checkOnlineProjectSize(),
        mergeMap(() => saveObservable.pipe(saveTimeout())),
        map(project => {
          const newProject = this.injector.runInContext(() => new OnlineProject({ ...project, data }));
          this.projectService.project$.next(newProject);
          return newProject;
        })
      );
    }
  }
}
