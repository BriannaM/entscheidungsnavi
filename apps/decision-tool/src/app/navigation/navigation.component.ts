import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { NAVI_STEP_ORDER, NaviStep, NaviStepNames, NaviSubStep, subStepToNumber } from '@entscheidungsnavi/decision-data/steps';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Observable, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { TeamUUIDs } from '@entscheidungsnavi/decision-data/classes';
import { CurrentProgressService } from '../data/current-progress.service';
import { NavigationStepMetaData } from '../../modules/shared/navigation/navigation-step';
import { ENVIRONMENT } from '../../environments/environment';
import { AboutUsModalComponent } from '../about-us-modal/about-us-modal.component';
import { ServiceWorkerService } from '../../services/service-worker.service';
import { ProjectService } from '../data/project';
import { LanguageService } from '../data/language.service';

@Component({
  selector: 'dt-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  steps = NAVI_STEP_ORDER.map(stepId => ({
    id: stepId,
    ...this.languageService.steps[stepId],
  }));

  @Input() isUnfolded = true;

  @Input() isSidenavOver: boolean;

  @Output() unfoldButtonClick = new EventEmitter<void>();

  @ViewChild('decisionStatementContainer') decisionStatementContainer: ElementRef<HTMLElement>;

  year: string;

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  TeamUUIDs = TeamUUIDs;

  static navigateToStep(
    stepIndex: number,
    currentProgress: NaviSubStep,
    router: Router,
    decisionData: DecisionData,
    steps: { [key in NaviStep]: NavigationStepMetaData & NaviStepNames }
  ): Promise<boolean> {
    const currentStepName = currentProgress.step;
    const currentStepIndex = NAVI_STEP_ORDER.indexOf(currentStepName);

    if (stepIndex === 4 && currentProgress.subStepIndex === undefined && !decisionData.validateWeights()[0]) {
      // Results
      decisionData.resultSubstepProgress = 1;
      currentProgress.subStepIndex = 1;
    }

    if (
      stepIndex === 3 ||
      stepIndex < currentStepIndex ||
      (stepIndex === currentStepIndex && currentProgress.subStepIndex === undefined) ||
      stepIndex === 5
    ) {
      return router.navigate([steps[NAVI_STEP_ORDER[stepIndex]].routerLink]);
    } else if (stepIndex === currentStepIndex) {
      return router.navigate([steps[NAVI_STEP_ORDER[stepIndex]].routerLink, 'steps', currentProgress.subStepIndex + 1]);
    } else {
      return router.navigate([steps[NAVI_STEP_ORDER[stepIndex]].routerLink, 'steps', 1]);
    }
  }

  constructor(
    protected decisionData: DecisionData,
    protected languageService: LanguageService,
    private router: Router,
    private currentProgressService: CurrentProgressService,
    private dialog: MatDialog,
    private cdRef: ChangeDetectorRef,
    protected serviceWorkerService: ServiceWorkerService,
    protected projectService: ProjectService
  ) {
    this.year = new Date().getFullYear() + '';

    this.currentProgressService.currentProgressUpdate$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.cdRef.detectChanges();
    });
  }

  isSubStepDone(step: NaviSubStep) {
    return subStepToNumber(step) < subStepToNumber(this.currentProgressService.currentProgress);
  }

  isSubStepCurrent(step: NaviSubStep) {
    return subStepToNumber(step) === subStepToNumber(this.currentProgressService.currentProgress);
  }

  isStepDone(stepIndex: number) {
    const currentProgress = this.currentProgressService.currentProgress;

    const currentStepName = currentProgress.step;
    const currentStepIndex = NAVI_STEP_ORDER.indexOf(currentStepName);

    return stepIndex < currentStepIndex || (stepIndex === currentStepIndex && currentProgress.subStepIndex === undefined);
  }

  toStep(stepIndex: number) {
    NavigationComponent.navigateToStep(
      stepIndex,
      this.currentProgressService.currentProgress,
      this.router,
      this.decisionData,
      this.languageService.steps
    );
  }

  isProjectLoaded(): boolean {
    return this.projectService.isProjectLoaded();
  }

  get version() {
    return ENVIRONMENT.version;
  }

  get environmentType() {
    return ENVIRONMENT.type;
  }

  get buildTimestamp() {
    return ENVIRONMENT.buildTimestamp;
  }

  openAboutUs() {
    this.dialog.open(AboutUsModalComponent);
  }

  toggleUnfold() {
    this.unfoldButtonClick.emit();
  }
}
