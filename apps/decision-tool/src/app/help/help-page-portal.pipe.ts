import { ComponentPortal, Portal, TemplatePortal } from '@angular/cdk/portal';
import { Injector, Pipe, PipeTransform, ViewContainerRef } from '@angular/core';
import { HELP_PAGE_CONTEXT } from '../../modules/shared/help-page-context.token';
import { HelpPage } from './help';
import { HelpPageYoutubeComponent, HELP_PAGE_YOUTUBE_VIDEO_ID } from './help-page-youtube.component';

/**
 * Turns a help page into a ComponentPortal to be displayed
 */
@Pipe({
  name: 'helpPagePortal',
})
export class HelpPagePortalPipe implements PipeTransform {
  constructor(private injector: Injector, private viewContainerRef: ViewContainerRef) {}

  transform(value?: HelpPage): Portal<any> {
    switch (value?.content.type) {
      case 'youtube': {
        const injector = Injector.create({
          parent: this.injector,
          providers: [{ provide: HELP_PAGE_YOUTUBE_VIDEO_ID, useValue: value.content.youtubeVideoId }],
        });
        return new ComponentPortal(HelpPageYoutubeComponent, undefined, injector);
      }
      case 'component':
        return new ComponentPortal(
          value.content.component,
          this.viewContainerRef,
          Injector.create({
            parent: this.injector,
            providers: [{ provide: HELP_PAGE_CONTEXT, useValue: value.context }],
          })
        );
      case 'template':
        return new TemplatePortal(value.content.template(), this.viewContainerRef);
      default:
        return null;
    }
  }
}
