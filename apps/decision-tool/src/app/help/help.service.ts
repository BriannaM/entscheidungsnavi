import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HelpMenu } from './help';
import { HelpComponentConfig } from './help.component';

@Injectable({
  providedIn: 'root',
})
export class HelpService {
  private currentMenu$: BehaviorSubject<HelpMenu>;
  private currentPage$: BehaviorSubject<number>;

  // The config passed to every HelpComponent
  private config: HelpComponentConfig;

  constructor() {
    this.currentMenu$ = new BehaviorSubject(null);
    this.currentPage$ = new BehaviorSubject(0);

    this.config = {
      currentMenu$: this.currentMenu$,
      currentPage$: this.currentPage$,
    };
  }

  setMenu(menu: HelpMenu) {
    if (this.currentMenu$.value !== menu) {
      this.currentMenu$.next(menu);
      this.currentPage$.next(0);
    }
  }

  getConfig() {
    return this.config;
  }
}
