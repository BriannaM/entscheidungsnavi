import { Component, Inject, InjectionToken } from '@angular/core';

export const HELP_PAGE_YOUTUBE_VIDEO_ID = new InjectionToken<string>('DT_HELP_PAGE_YOUTUBE_VIDEO_ID');

@Component({
  template: `<dt-youtube-container [videoId]="videoId"></dt-youtube-container>`,
  styles: [
    `
      :host {
        flex: 1;
      }
      dt-youtube-container {
        height: 100%;
        width: 100%;
      }
    `,
  ],
})
export class HelpPageYoutubeComponent {
  constructor(@Inject(HELP_PAGE_YOUTUBE_VIDEO_ID) public videoId: string) {}
}
