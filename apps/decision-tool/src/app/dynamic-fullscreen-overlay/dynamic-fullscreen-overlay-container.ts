import { OverlayContainer } from '@angular/cdk/overlay';
import { Platform } from '@angular/cdk/platform';
import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, OnDestroy } from '@angular/core';

/**
 * This class is nearly an exact copy of the Angular CDK FullscreenOverlayContainer.
 * https://github.com/angular/components/blob/master/src/cdk/overlay/fullscreen-overlay-container.ts
 *
 * The only difference being that it checks whether the fullscreen element is a child of the overlay container before
 * adding the overlay container as a child of the fullscreen element (see _adjustParentForFullscreenChange() and issue #440).
 *
 * This is needed for Youtube Modals where the modal content goes fullscreen.
 */
@Injectable({ providedIn: 'root' })
export class DynamicFullscreenOverlayContainer extends OverlayContainer implements OnDestroy {
  private _fullScreenEventName: string | undefined;
  private _fullScreenListener: () => void;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  constructor(@Inject(DOCUMENT) _document: any, platform: Platform) {
    super(_document, platform);
  }

  override ngOnDestroy() {
    super.ngOnDestroy();

    if (this._fullScreenEventName && this._fullScreenListener) {
      this._document.removeEventListener(this._fullScreenEventName, this._fullScreenListener);
    }
  }

  protected override _createContainer(): void {
    super._createContainer();
    this._adjustParentForFullscreenChange();
    this._addFullscreenChangeListener(() => this._adjustParentForFullscreenChange());
  }

  private _adjustParentForFullscreenChange(): void {
    if (!this._containerElement) {
      return;
    }

    const fullscreenElement = this.getFullscreenElement();
    const parent = fullscreenElement || this._document.body;
    // Second condition: only append if necessary. This prevents youtube modals from resetting when leaving fullscreen mode.
    if (!this._containerElement.contains(parent) && this._containerElement.parentNode !== parent) {
      parent.appendChild(this._containerElement);
    }
  }

  private _addFullscreenChangeListener(fn: () => void) {
    const eventName = this._getEventName();

    if (eventName) {
      if (this._fullScreenListener) {
        this._document.removeEventListener(eventName, this._fullScreenListener);
      }

      this._document.addEventListener(eventName, fn);
      this._fullScreenListener = fn;
    }
  }

  private _getEventName(): string | undefined {
    if (!this._fullScreenEventName) {
      const document = this._document as any;

      if (document.fullscreenEnabled) {
        this._fullScreenEventName = 'fullscreenchange';
      } else if (document.webkitFullscreenEnabled) {
        this._fullScreenEventName = 'webkitfullscreenchange';
      } else if (document.mozFullScreenEnabled) {
        this._fullScreenEventName = 'mozfullscreenchange';
      } else if (document.msFullscreenEnabled) {
        this._fullScreenEventName = 'MSFullscreenChange';
      }
    }

    return this._fullScreenEventName;
  }

  /**
   * When the page is put into fullscreen mode, a specific element is specified.
   * Only that element and its children are visible when in fullscreen mode.
   */
  getFullscreenElement(): Element {
    const document = this._document as any;

    return (
      document.fullscreenElement ||
      document.webkitFullscreenElement ||
      document.mozFullScreenElement ||
      document.msFullscreenElement ||
      null
    );
  }
}
