import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ModeTransitionService } from '../../../modules/shared/mode-transition/mode-transition.service';

@Injectable({
  providedIn: 'root',
})
export class ProfessionalModeGuard {
  constructor(private decisionData: DecisionData, private modeTransitionService: ModeTransitionService) {}

  canActivate(): boolean {
    if (this.decisionData.isProfessional()) {
      return true;
    } else {
      this.modeTransitionService.transitionIntoProfessional();
      // Block the current navigation. The transition service will handle it.
      return false;
    }
  }
}
