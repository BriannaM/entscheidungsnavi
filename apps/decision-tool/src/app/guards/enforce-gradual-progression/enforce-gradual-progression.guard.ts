import { inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CanActivateChildFn, NavigationCancel, Router } from '@angular/router';
import { subStepToNumber } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { firstValueFrom, takeUntil } from 'rxjs';
import { naviSubStepToUrl, urlToNaviSubStep } from '../../../modules/shared/navigation/navigation-step';
import { AppSettingsService } from '../../data/app-settings.service';
import { CurrentProgressService } from '../../data/current-progress.service';
import {
  GradualProgressionWarningModalComponent,
  ModalResult,
} from './gradual-progression-warning-modal/gradual-progression-warning-modal.component';

const IMPACT_MODEL_STEP_NUMBER = subStepToNumber({ step: 'impactModel' });

export const enforceGradualProgressionGuard: CanActivateChildFn = async (childRoute, state) => {
  // Only check for the "leaf"-routes of the url tree, that actually show components (not redirects, not intermediates)
  if (childRoute.routeConfig.component == null) {
    return true;
  }

  // Only required in educational mode
  if (inject(DecisionData).projectMode !== 'educational') {
    return true;
  }

  const nextStep = urlToNaviSubStep(state.url);
  if (nextStep == null) {
    return true;
  }

  const currentProgressService = inject(CurrentProgressService);
  const router = inject(Router);

  currentProgressService.update();
  const currentStepNumber = subStepToNumber(currentProgressService.currentProgress);

  // We only track steps up to the impact model. After that has been opened, all steps are accessible.
  if (currentStepNumber < IMPACT_MODEL_STEP_NUMBER && currentStepNumber + 1 < subStepToNumber(nextStep)) {
    if (inject(AppSettingsService).skipSubstepWarning) {
      const dialogRef = inject(MatDialog).open<GradualProgressionWarningModalComponent, void, ModalResult>(
        GradualProgressionWarningModalComponent
      );

      router.events.pipe(takeUntil(dialogRef.afterClosed())).subscribe(event => {
        if (event instanceof NavigationCancel) {
          dialogRef.componentInstance.close();
        }
      });

      const result = await firstValueFrom(dialogRef.afterClosed());

      switch (result) {
        case 'abort':
          return false;
        case 'confirm':
          currentProgressService.confirmSkip(nextStep);
          return true;
        case 'navigate-to-latest':
          return router.parseUrl(naviSubStepToUrl(currentProgressService.currentProgress));
      }
    } else {
      currentProgressService.confirmSkip(nextStep);
      inject(MatSnackBar).openFromComponent(SnackbarComponent, {
        data: { message: $localize`Teilschritt(e) übersprungen`, icon: 'fast_forward' } as SnackbarData,
        duration: 5000,
      });
    }
  }

  return true;
};
