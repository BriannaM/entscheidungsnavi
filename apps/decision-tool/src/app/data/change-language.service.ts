import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { firstValueFrom } from 'rxjs';
import { LanguageService } from './language.service';
import { ProjectService } from './project';

@Injectable({ providedIn: 'root' })
export class ChangeLanguageService {
  private _isChangingLanguage = false;
  get isChangingLanguage() {
    return this._isChangingLanguage;
  }

  constructor(private languageService: LanguageService, private location: Location, private projectService: ProjectService) {}

  async changeLanguage() {
    if (
      !(await firstValueFrom(
        this.projectService.confirmProjectUnload({
          title: $localize`Ungespeicherte Änderungen`,
          prompt: $localize`Durch das Ändern der Sprache wird Dein Projekt geschlossen und ungespeicherte Änderungen gehen verloren.
          Wir empfehlen, Dein Projekt vorher online zu speichern. Möchtest Du Dein Projekt schließen und die Sprache wechseln?`,
          buttonConfirm: $localize`Ja, Projekt schließen`,
        })
      ))
    ) {
      return;
    }

    this._isChangingLanguage = true;
    let url: string;
    if (this.languageService.isEnglish) {
      url = '../de';
    } else {
      url = '../en';
    }
    document.location.href = this.location.prepareExternalUrl(url + this.location.path());
  }
}
