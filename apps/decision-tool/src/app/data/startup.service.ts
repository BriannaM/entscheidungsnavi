import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, isDevMode } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService, QuickstartService, TeamsService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { isMongoId } from 'class-validator';
import { catchError, concatMap, EMPTY, filter, first, firstValueFrom, fromEvent, of, switchMap } from 'rxjs';
import { Router } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ENVIRONMENT } from '../../environments/environment';
import { NewProjectModalComponent } from '../main/start';
import { ProjectListModalComponent } from '../navigation';
import { NightlyWarningModalComponent } from '../nightly/nightly-warning-modal/nightly-warning-modal.component';
import { LoginModalComponent } from '../main/userarea';
import { AppSettingsService } from './app-settings.service';
import { LocalProject, OnlineProject, ProjectService } from './project';
import { DecisionDataExportService } from './decision-data-export.service';

type StartParams = Partial<{
  quickaction: 'newStarterProject' | 'newEducationalProject' | 'openQuickstartProject';
  quickstart: string;
  embedded: unknown;
  linkedproject: string;
  eventSubmission: string;
  klugToken: string;
  joinTeam: string;
}>;

const lastProjectInfoKey = 'dt-last-project-info';
const devModeTempStorageKey = 'dev-mode-project-store';

@Injectable({
  providedIn: 'root',
})
export class StartupService {
  private alreadyRan = false;

  constructor(
    private projectService: ProjectService,
    private exportService: DecisionDataExportService,
    private dialog: MatDialog,
    private appSettings: AppSettingsService,
    private quickstartService: QuickstartService,
    private snackbarService: MatSnackBar,
    private authService: AuthService,
    private teamAPIService: TeamsService,
    private router: Router,
    private decisionData: DecisionData
  ) {
    this.projectService.project$.pipe(filter(() => this.alreadyRan)).subscribe(project => {
      if (project instanceof OnlineProject) {
        sessionStorage.setItem(
          lastProjectInfoKey,
          project.isOwnProject
            ? JSON.stringify({ onlineProjectId: project.info.id })
            : JSON.stringify({ teamId: project.team.info.id, memberId: project.team.activeMember.id })
        );
      } else sessionStorage.removeItem(lastProjectInfoKey);
    });

    if (isDevMode()) {
      fromEvent(window, 'beforeunload').subscribe(() => {
        if (this.projectService.isProjectLoaded()) {
          sessionStorage.setItem(devModeTempStorageKey, this.exportService.dataToText());
        } else {
          sessionStorage.removeItem(devModeTempStorageKey);
        }
      });
    }
  }

  async autorun(params: StartParams) {
    if (this.alreadyRan) {
      return;
    }
    this.alreadyRan = true;

    this.showNightlyWarning();

    if ('embedded' in params) {
      return await firstValueFrom(this.projectService.loadProjectFromCockpit());
    }

    if (params.quickaction) {
      const action = params.quickaction;
      if (action === 'newStarterProject') {
        this.dialog.open(NewProjectModalComponent, { data: 'starter' });
      } else if (action === 'newEducationalProject') {
        this.dialog.open(NewProjectModalComponent, { data: 'educational' });
      } else if (action === 'openQuickstartProject') {
        this.quickstartService
          .getProjects()
          .pipe(loadingIndicator(this.snackbarService, $localize`Themenprojekte werden geladen`))
          .subscribe({
            next: list => this.dialog.open(ProjectListModalComponent, { data: { quickstart: true, projectList: list } }),
            error: () => {
              this.dialog.open(ConfirmModalComponent, {
                data: {
                  title: $localize`Fehler`,
                  prompt: $localize`Themenprojekte sind aktuell nicht verfügbar. Versuche es später erneut.`,
                  buttonDeny: 'Okay',
                },
              });
            },
          });
      }
      return;
    }

    if (params.quickstart) {
      this.projectService.loadQuickstartProject(params.quickstart).subscribe({
        error: () => {
          this.dialog.open(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler`,
              prompt: $localize`Das verlinkte Themenprojekt existiert nicht mehr.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });

      return;
    }

    if (params.linkedproject) {
      this.projectService.loadLinkedProject(params.linkedproject).subscribe({
        error: () => {
          this.dialog.open(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler`,
              prompt: $localize`Das verlinkte Projekt existiert nicht mehr oder der Link wurde deaktiviert.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });

      return;
    }

    if (isMongoId(params.eventSubmission)) {
      this.projectService.loadEventSubmission(params.eventSubmission).subscribe({
        error: (error: HttpErrorResponse) => {
          this.dialog.open(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler beim Laden der Abgabe zur Veranstaltung`,
              prompt:
                error.status === 401 || error.status === 403
                  ? $localize`Du besitzt nicht die nötige Berechtigung, um Abgaben aus dieser Veranstaltung zu laden.
                  Stelle sicher, dass Du mit dem richtigen Account eingeloggt bist.`
                  : error.status === 404
                  ? $localize`Die angegebene Abgabe zur Veranstaltung wurde nicht gefunden. Möglicherweise ist der Link fehlerhaft.`
                  : $localize`Ein unbekannter Fehler ist beim Laden der Abgabe aufgetreten.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });
    }

    if (params.klugToken) {
      this.projectService.loadKlugProject(params.klugToken).subscribe({
        error: (error: Error) => {
          const isMissing = error instanceof HttpErrorResponse && error.status === 404;

          this.dialog.open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler beim Laden des KLUGentscheiden Projekts`,
              prompt: isMissing
                ? $localize`Unter dem Token „${params.klugToken}“ konnte kein Projekt gefunden werden.`
                : $localize`Bei der Abfrage des Projekts ist ein Fehler aufgetreten. Versuche es eventuell später erneut.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });
      return;
    }

    if (params.joinTeam) {
      await this.joinTeam(params.joinTeam);
      return;
    }

    let lastProjectInfo: any;
    try {
      const lastProjectText = sessionStorage.getItem(lastProjectInfoKey);
      if (lastProjectText) {
        lastProjectInfo = JSON.parse(lastProjectText);
      }
    } catch {}

    if (lastProjectInfo) {
      this.authService.loggedIn$
        .pipe(
          first(),
          filter(Boolean),
          switchMap(() => {
            if ('onlineProjectId' in lastProjectInfo) {
              return this.projectService.loadOnlineProject(lastProjectInfo.onlineProjectId);
            } else if ('teamId' in lastProjectInfo && 'memberId' in lastProjectInfo) {
              return this.projectService.loadTeamProject(lastProjectInfo.teamId, lastProjectInfo.memberId);
            } else {
              return EMPTY;
            }
          })
        )
        .subscribe({
          error: () => this.snackbarService.open($localize`Das zuletzt geöffnete Projekt konnte nicht automatisch geladen werden`, 'Ok'),
        });
      return;
    }

    if (isDevMode()) {
      const project = sessionStorage.getItem(devModeTempStorageKey);
      if (project) {
        try {
          this.exportService.importText(project);
          this.projectService.project$.next(new LocalProject('local'));
          this.router.navigateByUrl(this.decisionData.lastUrl || '/start', { onSameUrlNavigation: 'reload' });
          return;
        } catch {}
      }
    }
  }

  private async joinTeam(token: string) {
    if (!(await firstValueFrom(this.authService.loggedIn$))) {
      await firstValueFrom(this.dialog.open(LoginModalComponent).afterClosed());
    }

    if (await firstValueFrom(this.authService.loggedIn$)) {
      const info = await firstValueFrom(
        this.teamAPIService.getInviteInfo(token).pipe(
          catchError(error => {
            if (error instanceof HttpErrorResponse && error.status == 404) {
              this.snackbarService.open(
                $localize`Die Einladung wurde bereits verwendet oder das Team existiert nicht mehr.`,
                $localize`Ok`
              );
            } else {
              this.snackbarService.open(
                $localize`Beim Abrufen der Team-Informationen ist ein unbekannter Fehler aufgetreten.`,
                $localize`Ok`
              );
            }

            return of(null);
          })
        )
      );

      if (!info) {
        return;
      }

      const confirmed = await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Team beitreten`,
              prompt: $localize`${info.inviterName} hat Dich in das Team „${info.teamName}“ eingeladen.
              Möchtest Du dem Team beitreten?`,
              buttonConfirm: $localize`Ja`,
              buttonDeny: $localize`Nein`,
              buttonConfirmColor: 'accent',
            },
          })
          .afterClosed()
      );

      if (!confirmed) {
        return;
      }

      this.teamAPIService
        .join(token)
        .pipe(concatMap(project => this.projectService.loadOnlineProject(project.id)))
        .subscribe({
          error: error => {
            if (error instanceof HttpErrorResponse && error.status === 409) {
              this.snackbarService.open($localize`Du bist bereits Teil dieses Teams.`, $localize`Ok`);
              return;
            }

            this.snackbarService.open($localize`Beim Teambeitritt ist ein unbekannter Fehler aufgetreten.`, $localize`Ok`);
          },
        });
    }
  }

  private showNightlyWarning() {
    if (this.appSettings.showNightlyWarning && ENVIRONMENT.nightly) {
      this.dialog.open(NightlyWarningModalComponent);
    }
  }
}
