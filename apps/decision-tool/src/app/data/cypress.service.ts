import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { isCypressRun } from '@entscheidungsnavi/widgets';

@Injectable({
  providedIn: 'root',
})
export class CypressService {
  constructor(decisionData: DecisionData) {
    if (isCypressRun()) {
      window['DecisionData'] = decisionData;
    }
  }
}
