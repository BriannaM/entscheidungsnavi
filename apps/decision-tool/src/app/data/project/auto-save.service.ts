import { Injectable, NgZone } from '@angular/core';
import {
  BehaviorSubject,
  EMPTY,
  Subject,
  audit,
  combineLatest,
  debounceTime,
  filter,
  fromEvent,
  map,
  merge,
  of,
  retry,
  startWith,
  switchAll,
  switchMap,
  tap,
  timer,
} from 'rxjs';
import { TimeTrackingService } from '../time-tracking.service';
import { OnlineProject } from './online-project';

export type SaveState = 'off' | 'idle' | 'saving' | 'error';

@Injectable({ providedIn: 'root' })
export class AutoSaveService {
  readonly saveState$ = new BehaviorSubject<SaveState>('off');

  private _saveError: any;
  get saveError() {
    return this._saveError;
  }

  private triggerSave$ = new Subject<void>();
  // We cannot depend on the project service to avoid a circular dependency
  private project$ = new Subject<OnlineProject | null>();

  constructor(private timeTrackingService: TimeTrackingService, private zone: NgZone) {
    this.zone.runOutsideAngular(() => {
      this.project$
        .pipe(
          // Only continue with the project if it can be saved
          switchMap(project => this.shouldAutoSave(project)),
          map(project => {
            if (project == null) {
              this.updateSaveState('off');
              return EMPTY;
            } else {
              this.updateSaveState('idle');
            }

            // We force a save when...
            const forceSave$ = merge(
              // ...we minimize the window
              fromEvent(window, 'blur'),
              // ...the window is about to be unloaded
              fromEvent(window, 'beforeunload'),
              // ...or we were manually triggered
              this.triggerSave$
            );

            // Trigger a save whenever...
            return merge(
              // ...we detect a change in in decision data
              this.timeTrackingService.decisionDataChanged(),
              // ...or a safe was forced by user action
              forceSave$
            ).pipe(
              // Suppress change events until we are no longer saving
              audit(() => this.saveState$.pipe(filter(state => state === 'idle'))),
              // Suppress events when the project is actually not modified
              filter(() => !project.isProjectSaved()),
              // Here, the actual update process starts
              tap(() => this.updateSaveState('saving')),
              debounceTime(3_000),
              // Save and retry
              switchMap(() =>
                this.zone
                  .run(() => project.save())
                  .pipe(
                    retry({
                      delay: error => {
                        this._saveError = error;
                        this.updateSaveState('error');
                        return timer(10_000);
                      },
                    })
                  )
              ),
              tap(() => this.updateSaveState('idle'))
            );
          }),
          switchAll()
        )
        .subscribe();
    });
  }

  private shouldAutoSave(project: OnlineProject) {
    if (project != null && project.info.loadedHistoryEntry == null) {
      return combineLatest([project.canSave$, project.onSaved$.pipe(startWith(null))]).pipe(
        map(([canSave]) => (canSave && project.info.loadedHistoryEntry == null ? project : null))
      );
    } else {
      return of(null);
    }
  }

  private updateSaveState(newState: SaveState) {
    this.zone.run(() => this.saveState$.next(newState));
  }

  updateProject(project: OnlineProject | null) {
    this.project$.next(project);
  }

  triggerSave() {
    this.triggerSave$.next();
  }
}
