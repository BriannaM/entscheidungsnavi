import { AuthService, OnlineProjectsService } from '@entscheidungsnavi/api-client';
import { hashCode } from '@entscheidungsnavi/tools';
import { inject } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { FileExport } from '@entscheidungsnavi/widgets';
import sanitize from 'sanitize-filename';
import { DecisionDataExportService } from '../decision-data-export.service';
import { TrackingService } from '../tracking.service';

export abstract class AbstractProject {
  protected exportService = inject(DecisionDataExportService);
  protected trackingService = inject(TrackingService);
  protected onlineProjectsService = inject(OnlineProjectsService);
  protected authService = inject(AuthService);
  protected decisionData = inject(DecisionData);

  private lastExportHash: number;

  protected updateLastExportHash(exportText?: string) {
    exportText = exportText ?? this.exportService.changeDetectionRelevantDataToText();
    this.lastExportHash = hashCode(exportText);
  }

  isProjectSaved(): boolean {
    const data = this.exportService.changeDetectionRelevantDataToText();
    return hashCode(data) === this.lastExportHash;
  }

  exportFile() {
    this.trackingService.trackEvent('export project', { category: 'project' });

    const name = (sanitize(this.decisionData.decisionProblem) || 'export') + '.json';
    const data = this.exportService.dataToText();
    FileExport.download(name, data);
  }
}
