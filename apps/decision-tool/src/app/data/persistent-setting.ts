import { PersistentSetting as globalPersistentSetting } from '@entscheidungsnavi/widgets';
import { skip } from 'rxjs';
import { DecisionToolModule } from '../decision-tool.module';
import { ProjectService } from './project';
export { PersistentSettingParent } from '@entscheidungsnavi/widgets';

/**
 * This decorator can be used on properties of objects which should be persisted.
 * It also syncs values across multiple instances of the same class.
 *
 * It extends {@link @entscheidungsnavi/widgets#PersistentSetting}, allowing for
 * PersistentSettings with the lifetime 'project'.
 *
 * @remarks
 * Properties with this decorator must be initialized with a default value. Otherwise,
 * the first write to the property will be ignored.
 *
 * Only for the default scope 'indefinite', the
 * {@link @entscheidungsnavi/widgets#PersistentSettingParent} decorator must be used
 * on the containing class.
 *
 * @param lifetime - How long the value should be persisted
 *
 * @example
 * A setting which should be persisted indefinitely in local storage:
 * ```
 *   @PersistentSetting()
 *   colorOutcomes = false;
 * ```
 *
 * @example
 * A setting which should be persisted for the currently opened project:
 * ```
 *   @PersistentSetting('project')
 *   view: 'objective-list' | 'hierarchy' = 'objective-list';
 * ```
 */
export function PersistentSetting(lifetime: 'indefinite' | 'project' = 'indefinite') {
  if (lifetime === 'indefinite') {
    return globalPersistentSetting();
  } else {
    return persistSettingProject();
  }
}

function persistSettingProject() {
  let defaultValue: unknown;
  let currentValue: unknown;

  const projectService = DecisionToolModule.injector.get(ProjectService);
  projectService.project$.pipe(skip(1)).subscribe(() => (currentValue = defaultValue));

  const firstSetOnInstanceProperty = Symbol();

  return function (target: unknown, propertyKey: string) {
    let firstSetOnClass = true;
    Object.defineProperty(target, firstSetOnInstanceProperty, { value: true, writable: true });

    Object.defineProperty(target, propertyKey, {
      get: () => {
        return currentValue;
      },
      set: function (newValue: unknown) {
        if (this[firstSetOnInstanceProperty]) {
          this[firstSetOnInstanceProperty] = false;

          if (firstSetOnClass) {
            // The property is set for the very first time for this _class_ (not only instance)
            firstSetOnClass = false;

            // The value assigned here is the default property value. We store it to reset
            // it on project change.
            defaultValue = newValue;
            currentValue = newValue;
          }
        } else {
          currentValue = newValue;
        }
      },
    });
  };
}
