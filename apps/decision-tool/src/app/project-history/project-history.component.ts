import { Component } from '@angular/core';
import { OnlineProjectsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { catchError, EMPTY, filter, finalize, map, startWith, switchMap, tap } from 'rxjs';
import { Router } from '@angular/router';
import { ProjectHistoryEntry } from '@entscheidungsnavi/api-types';
import { OnlineProject, ProjectService } from '../data/project';

@Component({
  selector: 'dt-project-history',
  templateUrl: './project-history.component.html',
  styleUrls: ['./project-history.component.scss'],
})
export class ProjectHistoryComponent {
  isRestoringBackup = false;
  hasError = false;

  projectData$ = this.projectService.project$.pipe(
    filter((project): project is OnlineProject => project instanceof OnlineProject),
    switchMap(project =>
      project.onSaved$.pipe(
        startWith(null),
        map(() => project)
      )
    ),
    switchMap(project =>
      this.onlineProjectsService.getHistory(project.info.id).pipe(
        map(history => ({ project, history: this.groupHistoryEntries(history.list) })),
        tap(data => this.resetExpandedGroups(data.project, data.history))
      )
    ),
    catchError(() => {
      this.hasError = true;
      return EMPTY;
    })
  );

  expandedGroupIds = new Set<string>();

  constructor(
    private projectService: ProjectService,
    private onlineProjectsService: OnlineProjectsService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  onClick(projectId: string, historyEntryId?: string) {
    this.projectService.loadOnlineProject(projectId, { historyEntryId, overrideLastUrl: this.router.url }).subscribe({
      error: () =>
        this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
          data: { icon: 'error', message: $localize`Beim Laden der Version ist ein Fehler aufgetreten` },
          duration: 5000,
        }),
    });
  }

  private groupHistoryEntries(entries: ProjectHistoryEntry[]): ProjectHistoryEntry[][] {
    const groups: ProjectHistoryEntry[][] = [];

    while (entries.length > 0) {
      const group = [entries.pop()];
      while (entries.length > 0 && group.at(-1).versionTimestamp.getTime() - entries.at(-1).versionTimestamp.getTime() < 5 * 60 * 1000) {
        group.push(entries.pop());
      }
      groups.push(group);
    }

    return groups;
  }

  private resetExpandedGroups(project: OnlineProject, groups: ProjectHistoryEntry[][]) {
    this.expandedGroupIds.clear();

    for (const group of groups) {
      for (const entry of group) {
        if (entry.id === project.info.loadedHistoryEntry) {
          this.expandedGroupIds.add(group.at(-1).id);
        }
      }
    }
  }

  toggleGroup(groupId: string) {
    if (this.expandedGroupIds.has(groupId)) {
      this.expandedGroupIds.delete(groupId);
    } else {
      this.expandedGroupIds.add(groupId);
    }
  }

  restoreBackup() {
    this.isRestoringBackup = true;
    this.projectService
      .getProject(OnlineProject)
      .save()
      .pipe(finalize(() => (this.isRestoringBackup = false)))
      .subscribe({
        error: () => this.snackBar.open($localize`Fehler beim Wiederherstellen der Sicherung`, 'Ok'),
      });
  }
}
