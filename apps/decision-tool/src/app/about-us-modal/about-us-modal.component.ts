import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  templateUrl: './about-us-modal.component.html',
  styleUrls: ['./about-us-modal.component.scss'],
})
export class AboutUsModalComponent {
  constructor(public dialogRef: MatDialogRef<AboutUsModalComponent>) {}
}
