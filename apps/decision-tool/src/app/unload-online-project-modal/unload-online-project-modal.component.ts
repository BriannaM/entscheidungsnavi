import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { filter, Observable, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { AutoSaveService } from '../data/project/auto-save.service';

@Component({
  templateUrl: './unload-online-project-modal.component.html',
  styleUrls: ['./unload-online-project-modal.component.scss'],
})
export class UnloadOnlineProjectModalComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(protected autoSaveService: AutoSaveService, private dialogRef: MatDialogRef<UnloadOnlineProjectModalComponent>) {
    this.autoSaveService.triggerSave();
  }

  ngOnInit() {
    this.autoSaveService.saveState$
      .pipe(
        filter(state => state === 'idle'),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => this.close(true));
  }

  close(accept = false) {
    this.dialogRef.close(accept);
  }
}
