/**
 * The margin between paragraphs (in pt).
 *
 * Used between subsequent paragraphs of text, or between a header and the content below.
 */
export const PARAGRAPH_MARGIN = 6;

/**
 * The margin between content sections (in pt).
 *
 * Used between different sections of content, e.g., different chapters.
 */
export const SECTION_MARGIN = 11;
