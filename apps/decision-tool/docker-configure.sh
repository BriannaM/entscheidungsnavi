#!/usr/bin/env sh

cat >/usr/share/nginx/html/de/assets/env.js <<EOL
(function (window) {
  window.dtEnv = {
    environmentType: '${ENVIRONMENT_TYPE}',
    timestamp: '${TIMESTAMP}',
    sentryDsn: '${SENTRY_DSN}',
    cockpitOrigin: '${COCKPIT_ORIGIN}',
  };
  window.dtMatomoSiteId = '${MATOMO_SITE_ID}';
})(this);
EOL

sed "s|<COCKPIT_ORIGIN>|$COCKPIT_ORIGIN|" /etc/nginx/conf.d/template.default.conf >/etc/nginx/conf.d/default.conf

cp /usr/share/nginx/html/de/assets/env.js /usr/share/nginx/html/en/assets/env.js
