#!/usr/bin/env sh

cat >/usr/share/nginx/html/de/assets/env.js <<EOL
(function (window) {
  window.klugEnv = {
    environmentType: '${ENVIRONMENT_TYPE}',
    sentryDsn: '${SENTRY_DSN}',
  };
  window.dtMatomoSiteId = '${MATOMO_SITE_ID}';
})(this);
EOL
