import { createEnvironment, ENVIRONMENT_TYPES, EnvironmentType } from './environment-types';

function getType(): EnvironmentType {
  if (ENVIRONMENT_TYPES.includes(window.klugEnv?.environmentType as EnvironmentType)) {
    return window.klugEnv.environmentType as EnvironmentType;
  }

  return 'production';
}

export const ENVIRONMENT = createEnvironment({
  type: getType(),
  sentryDsn: window.klugEnv?.sentryDsn || null,
});
