import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { KLUG_EXTRA_DATA_FIELD, KlugData } from '@entscheidungsnavi/decision-data/klug';

@Injectable({ providedIn: 'root' })
export class PairComparisonService {
  constructor(private decisionData: DecisionData) {
    decisionData.objectiveAdded$.subscribe(objectiveIndex => {
      const comparisonTable = this.getComparisonTable();
      for (const comparisons of comparisonTable) {
        comparisons.splice(objectiveIndex, 0, 'missing');
      }

      comparisonTable.splice(objectiveIndex, 0, new Array(this.decisionData.objectives.length).fill('missing'));
    });

    decisionData.objectiveRemoved$.subscribe(objectiveIndex => {
      const comparisonTable = this.getComparisonTable();
      for (const comparisons of comparisonTable) {
        comparisons.splice(objectiveIndex, 1);
      }

      this.getComparisonTable().splice(objectiveIndex, 1);
    });
  }

  getComparisonTable() {
    let field = this.decisionData.extraData[KLUG_EXTRA_DATA_FIELD] as KlugData;

    if (field == null) {
      field = this.decisionData.extraData[KLUG_EXTRA_DATA_FIELD] = { pairComparisons: [] };
    }

    return field.pairComparisons;
  }
}
