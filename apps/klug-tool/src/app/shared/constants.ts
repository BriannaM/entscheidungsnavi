export class Constants {
  public readonly maxObjectiveCount = 10;
  public readonly minObjectiveCount = 3;

  public readonly maxAlternativeCount = 10;
  public readonly minAlternativeCount = 2;
}
