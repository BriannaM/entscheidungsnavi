import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { AbstractStepComponent } from '../step-container.component';

@Component({
  selector: 'klug-decision-statement',
  templateUrl: './decision-statement.component.html',
  styleUrls: ['./decision-statement.component.scss'],
})
export class DecisionStatementComponent extends AbstractStepComponent {
  constructor(protected decisionData: DecisionData) {
    super();
  }
}
