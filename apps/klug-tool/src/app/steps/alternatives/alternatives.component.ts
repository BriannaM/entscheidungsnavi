import { Component } from '@angular/core';
import { AbstractStepComponent } from '../step-container.component';

@Component({
  selector: 'klug-alternatives',
  templateUrl: './alternatives.component.html',
})
export class AlternativesComponent extends AbstractStepComponent {}
